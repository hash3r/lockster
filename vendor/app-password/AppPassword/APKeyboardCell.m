//
//  APKeyboardCell.m
//  AppPassword
//
//  Created by ct on 4/2/13.
//  Copyright (c) 2013 Mitre. All rights reserved.
//

#import "APKeyboardCell.h"
#import <QuartzCore/QuartzCore.h>
#import "UIColor+RGB.h"
#import "SlowFadeButton.h"
#import "UIImage+Generate.h"

@implementation APKeyboardCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self) {
        
        UIImage *img1 = [UIImage imageWithColor:[UIColor redColor]];
        img1 = [UIImage roundedImageWithImage:img1];
        
        UIImage *img2 = [UIImage imageWithColor:[UIColorFromRGB(0xf5f5f5) colorWithAlphaComponent:1.0]];
        img2 = [UIImage roundedImageWithImage:img2];
        self.btn                      = [[SlowFadeButton alloc]
                                        initWithFrame:CGRectMake(0, 0, self.frame.size.height, self.frame.size.height) mainImg:img1 highlightImg:img2];
//        self.btn.backgroundColor      = [UIColor clearColor];
        
        UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Light" size:36];
        self.btn.titleLabel.font = font;
        self.btn.layer.cornerRadius   = self.frame.size.height/2;
        self.btn.layer.borderWidth = 1.0;
        self.btn.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 5, 0);
        
        self.btn.layer.borderColor = [UIColorFromRGB(0xf5f5f5) CGColor];
        [self.btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [self addSubview:self.btn];
        
        CGRect lRect =  CGRectMake(0.0, 53.0, frame.size.width, 10.0f);
        
        self.label                    = [[UILabel alloc] initWithFrame:lRect];
        
        UIFont *lblFont = [UIFont fontWithName:@"HelveticaNeue-Light" size:9];
        self.label.font               = lblFont;
        self.label.textAlignment      = NSTextAlignmentCenter;
        self.label.text               = @"ABC";
        self.label.textColor          = [UIColor whiteColor];
        self.label.backgroundColor    = [UIColor clearColor];
        self.label.layer.cornerRadius = 10;
        
        [self.btn addSubview:self.label];
        self.backgroundColor = [UIColor clearColor];
    }
    
    return self;
}

//-(void) addGradient {
//    
//    // Add Border
//    CALayer *layer      = _btn.layer;
//    layer.cornerRadius  = 10.0f;
//    layer.masksToBounds = YES;
//    layer.borderWidth   = 1.0f;
//    layer.borderColor   = [UIColor colorWithWhite:0.1f alpha:0.8f].CGColor;
//    
//    // Add Shine
//    CAGradientLayer *shineLayer = [CAGradientLayer layer];
//    shineLayer.frame    = layer.bounds;
//    shineLayer.colors   = [NSArray arrayWithObjects:
//                          (id)[UIColor colorWithWhite:1.0f  alpha:0.4f].CGColor,
//                          (id)[UIColor colorWithWhite:1.0f  alpha:0.2f].CGColor,
//                          (id)[UIColor colorWithWhite:0.75f alpha:0.2f].CGColor,
//                          (id)[UIColor colorWithWhite:0.4f  alpha:0.2f].CGColor,
//                          (id)[UIColor colorWithWhite:0.2f  alpha:0.2f].CGColor,
//                          nil];
//    
//    shineLayer.locations = [NSArray arrayWithObjects:
//                            [NSNumber numberWithFloat:0.0f],
//                            [NSNumber numberWithFloat:0.25f],
//                            [NSNumber numberWithFloat:0.5f],
//                            [NSNumber numberWithFloat:0.8f],
//                            [NSNumber numberWithFloat:1.0f],
//                            nil];
//    
//    [layer addSublayer:shineLayer];
//}

@end

