//
//  SlowFadeButton.m
//  AppPassword
//
//  Created by Chebulaev Oleg on 24.04.14.
//  Copyright (c) 2014 MITRE. All rights reserved.
//

#import "SlowFadeButton.h"

@interface SlowFadeButton ()

@property(strong, nonatomic)UIImageView *glowOverlayImgView; // Used to overlay glowing animal image and fade out

@end

@implementation SlowFadeButton



-(id)initWithFrame:(CGRect)theFrame mainImg:(UIImage*)theMainImg highlightImg:(UIImage*)theHighlightImg
{
    if((self = [SlowFadeButton buttonWithType:UIButtonTypeCustom])) {
        
        self.frame = theFrame;
        
        if(!theMainImg) {
            NSLog(@"Problem loading the main image\n");
        }
        else if(!theHighlightImg) {
            NSLog(@"Problem loading the highlight image\n");
        }
        
        [self setImage:theMainImg forState:UIControlStateNormal];
        self.glowOverlayImgView = [[UIImageView alloc] initWithImage:theHighlightImg];
        self.glowOverlayImgView.frame = self.imageView.frame;
        self.glowOverlayImgView.bounds = self.imageView.bounds;
        
        self.adjustsImageWhenHighlighted = NO;
    }
    
    return self;
}


-(void)setHighlighted:(BOOL)highlighted
{
    // Check if button is going from not highlighted to highlighted
    if(![self isHighlighted] && highlighted) {
        if (self.needHighlight) {
            self.glowOverlayImgView.alpha = 1;
            [self addSubview:self.glowOverlayImgView];
            [self sendSubviewToBack:self.glowOverlayImgView];
        }
    }
    // Check if button is going from highlighted to not highlighted
    else if([self isHighlighted] && !highlighted) {
        if (self.needHighlight) {
            [UIView animateWithDuration:0.4f
                             animations:^{
                                 self.glowOverlayImgView.alpha = 0;
                             }
                             completion:NULL];
        }
    }
    
    [super setHighlighted:highlighted];
}

-(void)setGlowOverlayImgView:(UIImageView *)glowOverlayImgView
{
    if(glowOverlayImgView != _glowOverlayImgView) {
        _glowOverlayImgView = glowOverlayImgView;
    }
    
    self.glowOverlayImgView.alpha = 0;
}

@end
