//
//  APSimplePassportCell.h
//  AppPassword
//
//  Created by ct on 4/2/13.
//  Copyright (c) 2013 Mitre. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface APSimplePassCell : UICollectionViewCell {
    UIView *_indicator;
}

//@property (nonatomic, strong) UILabel  *label;
- (void)setFilled:(BOOL)filled;
@end
