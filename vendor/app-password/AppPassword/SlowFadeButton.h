//
//  SlowFadeButton.h
//  AppPassword
//
//  Created by Chebulaev Oleg on 24.04.14.
//  Copyright (c) 2014 MITRE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SlowFadeButton : UIButton

-(id)initWithFrame:(CGRect)theFrame mainImg:(UIImage*)theMainImg highlightImg:(UIImage*)theHighlightImg;

@property (nonatomic, assign) BOOL needHighlight;
@end
