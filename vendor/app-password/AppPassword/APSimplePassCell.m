//
//  APSimplePassportCell.m
//  AppPassword
//
//  Created by ct on 4/2/13.
//  Copyright (c) 2013 Mitre. All rights reserved.
//

#import "APSimplePassCell.h"
#import <QuartzCore/QuartzCore.h>
#import <UIColor+RGB.h>

@implementation APSimplePassCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self) {
        _indicator = [[UIView alloc] initWithFrame:CGRectMake(8, 0, 14, 14)];
        _indicator.layer.borderWidth = 1.0;
        _indicator.layer.cornerRadius = 7;
        _indicator.layer.borderColor = [UIColorFromRGB(0xf5f5f5) CGColor];
        [self addSubview:_indicator];

    }
    
    return self;
}

- (void)setFilled:(BOOL)filled {
    if (filled) {
        _indicator.backgroundColor = UIColorFromRGB(0xf5f5f5);
    } else {
        _indicator.backgroundColor = [UIColor clearColor];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
