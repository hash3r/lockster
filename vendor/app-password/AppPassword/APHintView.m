//
//  APHintView.m
//  AppPassword
//
//  Created by Chebulaev Oleg on 24.03.14.
//  Copyright (c) 2014 MITRE. All rights reserved.
//

#import "APHintView.h"

@implementation APHintView

- (id)initWithFrame:(CGRect)frame hintText:(NSString *)text {
    if (self = [super initWithFrame:frame]) {
        UIView *hintView = [[UIView alloc] initWithFrame:CGRectMake(18, 135, 284, 192)];
        hintView.layer.shadowOffset = CGSizeMake(2.5, 2.5);
        hintView.layer.shadowRadius = 25.0;
        hintView.layer.shadowColor = [[UIColor blackColor] CGColor];
        hintView.layer.shadowOpacity = 1.0;
        
        UILabel *l = [[UILabel alloc] initWithFrame:CGRectMake(15, 58, 254, 50)];
        l.backgroundColor = [UIColor clearColor];
        UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Light" size:15];
        l.font = font;
        l.textColor = [UIColor whiteColor];
        l.backgroundColor = [UIColor clearColor];
        l.textAlignment = NSTextAlignmentCenter;
        l.numberOfLines = 2;
        l.lineBreakMode = NSLineBreakByWordWrapping;
        l.text = @"You have described your passcode\nas";
        [hintView addSubview:l];
        
        UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(15, CGRectGetMaxY(l.frame) - 15, 254, 40)];
        lbl.tag = 101;
        lbl.backgroundColor = [UIColor clearColor];
        UIFont *font1 = [UIFont fontWithName:@"HelveticaNeue-Light" size:18];
        lbl.font = font1;
        lbl.textAlignment = NSTextAlignmentCenter;
        lbl.tintColor = [UIColor yellowColor];
        lbl.textColor = [UIColor yellowColor];
        lbl.text = text;
        [hintView addSubview:lbl];
        
        hintView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.9];
        [self addSubview:hintView];
        
        UITapGestureRecognizer *tapR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
        [self addGestureRecognizer:tapR];
        self.backgroundColor = [UIColor clearColor];
        
        _hintView = hintView;
        _hintView.alpha = 0.0;
    }
    return self;
}

- (void)show {
    [UIView animateWithDuration:0.15 animations:^{
        _hintView.alpha = 1.0;
    }];
}

- (void)hideAndRemove {
    [UIView animateWithDuration:0.15 animations:^{
        _hintView.alpha = 0.0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

-(void) handleTapGesture:(UIGestureRecognizer *) sender {
    if (sender.state != UIGestureRecognizerStateEnded)  // <---
        return;                                         // <---
    [self hideAndRemove];
}

@end
