//
//  APKeyboardCell.h
//  AppPassword
//
//  Created by ct on 4/2/13.
//  Copyright (c) 2013 Mitre. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SlowFadeButton;

@interface APKeyboardCell : UICollectionViewCell

@property (nonatomic, strong) SlowFadeButton *btn;
@property (nonatomic, strong) UILabel  *label;

@end
