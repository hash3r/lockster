//
//  APHintView.h
//  AppPassword
//
//  Created by Chebulaev Oleg on 24.03.14.
//  Copyright (c) 2014 MITRE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface APHintView : UIView {
    UIView *_hintView;
}
- (id)initWithFrame:(CGRect)frame hintText:(NSString *)text;
- (void)show;
@end
