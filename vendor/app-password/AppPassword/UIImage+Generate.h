//
//  UIImage+Generate.h
//  AppPassword
//
//  Created by Chebulaev Oleg on 24.03.14.
//  Copyright (c) 2014 MITRE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Generate)
+ (UIImage *)roundedImageWithImage:(UIImage *)image;
+ (UIImage *)imageWithColor:(UIColor *)color;
@end
