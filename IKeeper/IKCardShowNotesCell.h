//
//  IKCardShowNotesCell.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 26.01.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SZTextView/SZTextView.h>

@interface IKCardShowNotesCell : UITableViewCell

@property (nonatomic, weak) IBOutlet SZTextView *noteTextView;

@end
