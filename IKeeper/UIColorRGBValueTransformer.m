//
//  UIColorRGBValueTransformer.m
//  IKeeper
//
//  Created by Chebulaev Oleg on 13.05.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import "UIColorRGBValueTransformer.h"

@implementation UIColorRGBValueTransformer

+ (Class)transformedValueClass
{
    return [NSData class];
}

+ (BOOL)allowsReverseTransformation
{
    return YES;
}

- (id)transformedValue:(id)value
{
    UIColor* color = value;
    if (!color) {
        return nil;
    }
    const CGFloat* components = CGColorGetComponents(color.CGColor);
    NSString* colorAsString = [NSString stringWithFormat:@"%f,%f,%f,%f", components[0], components[1], components[2], components[3]];
    return [colorAsString dataUsingEncoding:NSUTF8StringEncoding];
}

- (id)reverseTransformedValue:(id)value
{
    if (!value) {
        return nil;
    }
    NSString* colorAsString = [[NSString alloc] initWithData:value encoding:NSUTF8StringEncoding];
    NSArray* components = [colorAsString componentsSeparatedByString:@","];
    CGFloat r = [[components objectAtIndex:0] floatValue];
    CGFloat g = [[components objectAtIndex:1] floatValue];
    CGFloat b = [[components objectAtIndex:2] floatValue];
    CGFloat a = [[components objectAtIndex:3] floatValue];
    
    return [UIColor colorWithRed:r green:g blue:b alpha:a];
}

@end
