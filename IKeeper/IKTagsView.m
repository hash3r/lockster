//
//  IKTagsView.m
//  IKeeper
//
//  Created by Chebulaev Oleg on 04.04.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import "IKTagsView.h"
#import <IKTagViewCell.h>

@implementation IKTagsView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor redColor];
        self.layer.cornerRadius = 2.5;
        self.clipsToBounds = YES;
        self.layer.borderColor = [[UIColor darkGrayColor] CGColor];
        
        
        _table = [[UITableView alloc] initWithFrame:CGRectMake(0, 90, frame.size.width, frame.size.height - 90)];
        _table.delegate = self;
        _table.dataSource = self;
        _table.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        _table.scrollsToTop = NO;
        
        [self addSubview:_table];
        
        [_table reloadData];
        
        _topTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, 90.0)];
        _topTable.delegate = self;
        _topTable.dataSource = self;
        _topTable.scrollsToTop = NO;
        _topTable.scrollEnabled = NO;
        
        [self addSubview:_topTable];
        [_topTable reloadData];
    }
    return self;
}

- (void)setMode:(NSInteger)mode tags:(NSArray *)tags counts:(NSArray *)counts selectedTags:(NSMutableSet *)selectedTags {
    _mode = mode;
    _tags = tags;
    _counts = counts;
    _selectedTags = selectedTags;
    [_table reloadData];
    [_topTable reloadData];
}

#pragma mark - TableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == _topTable) {
        return 2;
    } else {
        return [_tags count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == _topTable) {
        IKTagViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
        if (!cell) {
            cell = [[IKTagViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        }
        if (indexPath.row == 0) {
            cell.titleLabel.text = @"Any Tag";
            if (_mode == 0) {
                cell.isChecked = YES;
            } else {
                cell.isChecked = NO;
            }
            cell.backgroundColor = UIColorFromRGB(0xf5f6f6);
            cell.separatorInset = UIEdgeInsetsMake(0, 39, 0, 0);
        } else if (indexPath.row == 1) {
            cell.titleLabel.text = @"No Tag";
            if (_mode == 1) {
                cell.isChecked = YES;
            } else {
                cell.isChecked = NO;
            }
            cell.backgroundColor = UIColorFromRGB(0xf5f6f6);
            cell.separatorInset = UIEdgeInsetsZero;
        }
        return cell;
    } else {
        IKTagViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
        if (!cell) {
            cell = [[IKTagViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        }
        IKTag *tag = _tags[indexPath.row];
//        NSInteger count  = [_counts[indexPath.row] integerValue];
        if (_mode == 0 || _mode == 1) {
            cell.isChecked = NO;
        } else if (_mode == 2) {
            if ([_selectedTags containsObject:tag]) {
                cell.isChecked = YES;
            } else {
                cell.isChecked = NO;
            }
        }
        cell.titleLabel.text = [NSString stringWithFormat:@"%@", tag.name];
        cell.backgroundColor = UIColorFromRGB(0xffffff);
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (tableView == _topTable) {
        if (indexPath.row == 0) {
            [self onSelectAnyTags];
        } else if (indexPath.row == 1) {
            [self onSelectNoTags];
        }
    } else {
        [self onSelectTag:indexPath.row];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 45.0;
}

- (void)onSelectAnyTags {
    _mode = 0;
    _selectedTags = [NSMutableSet set];
    [_table reloadData];
    [_topTable reloadData];
    [self.delegate onSelectAnyTags];
}

- (void)onSelectNoTags {
    _mode = 1;
    _selectedTags = [NSMutableSet set];
    [_table reloadData];
    [_topTable reloadData];
    [self.delegate onSelectNoTags];
}

- (void)onSelectTag:(NSInteger)number {
    _mode = 2;
    IKTag *tag = _tags[number];
    if ([_selectedTags containsObject:tag]) {
        [_selectedTags removeObject:tag];
        [self.delegate onUnSelectTag:tag];
    } else {
        [_selectedTags addObject:tag];
        [self.delegate onSelectTag:tag];
    }
    [_table reloadData];
    [_topTable reloadData];
    
    
    if ([_selectedTags count] == 0) {
        [self onSelectAnyTags];
    }
}

@end
