//
//  IKCardEditViewController.m
//  IKeeper
//
//  Created by Chebulaev Oleg on 06.01.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import "IKCardEditViewController.h"
#import "IKCardTemplate.h"
#import "IKTemplateView.h"
#import "IKTemplatesListViewController.h"
#import "IKIconsListViewController.h"
#import "IKCard.h"
#import "IKCardField.h"
#import "IKFieldValue.h"
#import "IKFieldCell.h"
#import <UIViewController+MMDrawerController.h>
#import "IKTypeEditViewController.h"
#import "UIColor+RGB.h"
#import <BlocksKit/UIActionSheet+BlocksKit.h>
#import <BlocksKit/UIAlertView+BlocksKit.h>
#import <UIImage-Resize/UIImage+Resize.h>
#import "IKFieldType.h"
#import "UIImage+RTTint.h"
#import <SAMGradientView.h>
#import "IKCardNoteCell.h"
#import "IKCardTagsCell.h"
#import <TITokenField/TITokenField.h>
#import "IKTag.h"
#import "IKTagCardLink.h"
#import "IKCardShowFieldCell.h"
#import "SVProgressHUD.h"
#import <VCTransitionsLibrary/CECardsAnimationController.h> 
#import <UIImage+Resize.h>
#import "IKPasteboard.h"
#import "IKButton.h"
#import "HKKTagWriteView.h"
#import "IDMSyncManager.h"

@interface IKCardEditViewController () <IKIconsListViewControllerDelegate,
                                        IKTemplatesListViewControllerDelegate,
                                        IKTypeEditViewControllerDelegate,
                                        UITextFieldDelegate,
                                        UITextViewDelegate,
                                        TITokenFieldDelegate,
                                        UIViewControllerTransitioningDelegate,
                                        HKKTagWriteViewDelegate, IKTemplateViewDelegate>
{
    IKCard *_card;
    NSMutableArray *_customFields;
    NSMutableDictionary *_textViews;
    NSMutableDictionary *_tagViews;
    BOOL _tagsEditing;
    NSUndoManager *_undoManager;
//    TITokenFieldView *_calculationView;
    BOOL _isNewCard;
    NSString *_originTitle;
    NSString *_editTitle;
    IKTemplateView *_templateView;
}

@property (nonatomic, strong) NSIndexPath *editingIndexPath;

@end

@implementation IKCardEditViewController

#pragma mark - Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.contentInset = UIEdgeInsetsMake(154 + 25, 0, 0, 0);
    self.tableView.scrollIndicatorInsets = UIEdgeInsetsMake(154, 0, 0, 0);
    UIView *topview = [[UIView alloc] initWithFrame:CGRectMake(0, -520, 320, 520)];
    topview.backgroundColor = [UIColor whiteColor];
    [self.tableView addSubview:topview];
    
    _templateView = [[IKTemplateView alloc] initWithFrame:CGRectMake(0, 64, 320, 90)];
    _templateView.delegate = self;
    [_templateView setFavoriteValue:[_card.isFavorite boolValue]];
    [self.view addSubview:_templateView];
    
    [_templateView.categoryBtn addTarget:self action:@selector(onChangeTeplateTapped:) forControlEvents:UIControlEventTouchUpInside];
    [_templateView.iconBtn addTarget:self action:@selector(onChangeIconTapped:) forControlEvents:UIControlEventTouchUpInside];
    _templateView.descriptionField.delegate = self;

    [SVProgressHUD appearance].hudBackgroundColor = [UIColor colorWithWhite:0.0 alpha:0.75];
    
    _textViews = [NSMutableDictionary dictionary];
    _tagViews = [NSMutableDictionary dictionary];
    
    [self setEditing:NO animated:NO];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    _undoManager = [[NSManagedObjectContext MR_defaultContext] undoManager];
    
    
    if (_card) {
        _isNewCard = NO;
        self.navigationItem.title = @"Detail";
    } else {
        _isNewCard = YES;
        self.navigationItem.title = @"New Card";
        _card = [IKCard MR_createInContext:[NSManagedObjectContext MR_defaultContext]];
        _card.template = _cardTemplate;
        if (self.isNewFav) {
            _card.isFavorite = [NSNumber numberWithBool:YES];
        }
        
        [self replaceCardFieldsFromTemplate];
        [self replaceCardIconFromTemplate];
    }
    
    _customFields = [[_card orderedFilledCustomFileds] mutableCopy];
    
    [self setEditBarBtn];
    [self setBackBarBtn];
    [self setEmptyViewToFooter];
    [_templateView setEditing:NO animated:NO];
    
    if (_isNewCard) {
        [self startEditingCard];
    }
    
    [self.tableView reloadData];
    [self reloadTemplateView];
}

- (void)onChangeFavorite:(BOOL)value {
    _card.isFavorite = [NSNumber numberWithBool:value];
    [[_card managedObjectContext] MR_saveToPersistentStoreAndWait];
}

- (void)setRemoveButtonToFooter {
    UIButton *removeCardButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [removeCardButton setFrame:CGRectMake(-1, 36, 322, 41)];
    removeCardButton.layer.borderWidth = 0.5;
    removeCardButton.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    [removeCardButton addTarget:self action:@selector(onRemoveCardTapped:) forControlEvents:UIControlEventTouchUpInside];
    [removeCardButton setTitle:@"Delete Card" forState:UIControlStateNormal];
    [removeCardButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [removeCardButton setBackgroundColor:[UIColor whiteColor]];
    removeCardButton.titleLabel.font = [UIFont systemFontOfSize:17.0];
    
    UIView *buttonsContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 160)];
    [buttonsContainer addSubview:removeCardButton];
    buttonsContainer.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.tableView.tableFooterView = buttonsContainer;
    self.tableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
}

- (void)setEmptyViewToFooter {
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 1)];
    UIView *v2 = [[UIView alloc] initWithFrame:CGRectMake(0, -1, 320, 2)];
    v2.backgroundColor = [UIColor whiteColor];
    v.backgroundColor = [UIColor whiteColor];
    [v addSubview:v2];
    self.tableView.tableFooterView = v;
    
    self.tableView.backgroundColor = [UIColor whiteColor];
}


- (void)viewWillAppear:(BOOL)animated {
    [self.mm_drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModePanningNavigationBar];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.mm_drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Keyboard
- (void)keyboardWillShow:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets;
    if (UIInterfaceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation])) {
        contentInsets = UIEdgeInsetsMake(154 + 25, 0.0, (keyboardSize.height), 0.0);
    } else {
        contentInsets = UIEdgeInsetsMake(154, 0.0, (keyboardSize.width), 0.0);
    }
    
    CGPoint contentOffset = self.tableView.contentOffset;
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
    self.tableView.contentOffset = contentOffset;
    

    NSLog(@"%@", self.editingIndexPath);
    if (self.editingIndexPath && self.editingIndexPath.section != 3) {
        [self.tableView scrollToRowAtIndexPath:self.editingIndexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    CGPoint oldOffset = self.tableView.contentOffset;
    
    self.tableView.contentInset = UIEdgeInsetsMake(154 + 25, 0.0, 0.0, 0.0);;
    self.tableView.scrollIndicatorInsets = UIEdgeInsetsMake(154, 0.0, 0.0, 0.0);;
    
    self.tableView.contentOffset = oldOffset;
    
    if (self.editingIndexPath) {
        [self.tableView scrollToRowAtIndexPath:self.editingIndexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    }
}

#pragma mark - Event Handlers
- (void)setEditBarBtn {
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:@"Edit" style:UIBarButtonItemStylePlain target:self action:@selector(onEditTapped:)];
    self.navigationItem.rightBarButtonItem = item;
}

- (void)setCancelBarBtn {
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(onCancelTapped:)];
    self.navigationItem.leftBarButtonItem = item;
}

- (void)setDoneBarBtn {
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(onDoneTapped:)];
    self.navigationItem.rightBarButtonItem = item;
}

- (void)setBackBarBtn {
//    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:@"Go" style:UIBarButtonItemStyleBordered target:self action:@selector(onBackTapped:)];
    
//    self.navigationItem.backBarButtonItem = item;
    self.navigationItem.leftBarButtonItem = self.navigationItem.backBarButtonItem;
}

- (void)onEditTapped:(id)sender {
    [self startEditingCard];
}

- (void)startEditingCard {
    [_undoManager beginUndoGrouping];
    [self setDoneBarBtn];
    [self setCancelBarBtn];
    [self.tableView setEditing:YES animated:YES];
    [_templateView setEditing:YES animated:NO];
    
    _customFields = [[_card orderedCustomFields] mutableCopy];
//    [self.tableView reloadData];
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, 3)] withRowAnimation:UITableViewRowAnimationAutomatic];
    [self reloadTemplateView];

    if (!_isNewCard) {
        [self setRemoveButtonToFooter];
        self.navigationItem.title = @"Edit Card";
    } else {
        [self setEmptyViewToFooter];
    }
    
}



- (void)onDoneTapped:(id)sender {
    if (self.editingIndexPath) {
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:self.editingIndexPath];
        self.editingIndexPath = nil;
        if ([cell isKindOfClass:[IKFieldCell class]]) {
            IKFieldCell *editingCell = (IKFieldCell *)cell;
            if ([editingCell.fieldField isFirstResponder]) {
                [editingCell.fieldField resignFirstResponder];
            }
        }
        
//        if ([cell isKindOfClass:[IKCardTagsCell class]]) {
//            IKCardTagsCell *editingCell = (IKCardTagsCell *)cell;
//            if ([editingCell.tokenFieldView.tokenField isFirstResponder]) {
//                [editingCell.tokenFieldView.tokenField resignFirstResponder];
//            }
//        }
    }
    
    if ([_templateView.descriptionField isFirstResponder]) {
        [_templateView.descriptionField resignFirstResponder];
    }
    
    NSError *error = nil;
    
    if ([_card isValidModel:&error]) {
        [_card updateNameField];
        __weak NSUndoManager *undoMgr =_undoManager;
        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
            if (error) {
                NSString *errorMessage = [error localizedDescription];
                [UIAlertView bk_showAlertViewWithTitle:errorMessage message:nil cancelButtonTitle:@"Ok" otherButtonTitles:@[] handler:nil];
            } else {
                [self setEditBarBtn];
                [self setBackBarBtn];
                [self.tableView setEditing:NO animated:YES];
                [_templateView setEditing:NO animated:YES];
                
                _customFields = [[_card orderedFilledCustomFileds] mutableCopy];
//                [self.tableView reloadData];
                [self.tableView reloadSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, 3)] withRowAnimation:UITableViewRowAnimationAutomatic];
                [self reloadTemplateView];
                
                [self setEmptyViewToFooter];
                [undoMgr endUndoGrouping];
                self.navigationItem.title = @"Detail";
                [self.delegate onSaveCard:_card viewContoller:self];
                [[IDMSyncManager sharedSyncManager] synchronizeWithCompletion:NULL];
            }
        }];
    } else {
        NSString *errorMessage = [error localizedDescription];
        [UIAlertView bk_showAlertViewWithTitle:errorMessage
                                       message:nil
                             cancelButtonTitle:@"Ok"
                             otherButtonTitles:@[]
                                       handler:nil];
    }
}

- (void)onCancelTapped:(id)sender {
    if (self.editingIndexPath) {
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:self.editingIndexPath];
        if ([cell isKindOfClass:[IKFieldCell class]]) {
            IKFieldCell *editingCell = (IKFieldCell *)cell;
            if ([editingCell.fieldField isFirstResponder]) {
                [editingCell.fieldField resignFirstResponder];
            }
        }
        
//        if ([cell isKindOfClass:[IKCardTagsCell class]]) {
//            IKCardTagsCell *editingCell = (IKCardTagsCell *)cell;
//            if ([editingCell.tokenFieldView.tokenField isFirstResponder]) {
//                [editingCell.tokenFieldView.tokenField resignFirstResponder];
//            }
//        }
    }
    
    if ([_templateView.descriptionField isFirstResponder]) {
        [_templateView.descriptionField resignFirstResponder];
    }
    
    [_undoManager endUndoGrouping];
    [_undoManager undo];
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
    [self.delegate onCancelCard:_card viewContoller:self];
    
    [self setEditBarBtn];
    [self setBackBarBtn];
    [self.tableView setEditing:NO animated:YES];
    [_templateView setEditing:NO animated:YES];
//    [self.tableView reloadData];
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, 3)] withRowAnimation:UITableViewRowAnimationAutomatic];
    [self reloadTemplateView];
    
    [self setEmptyViewToFooter];
}

- (void)onBackTapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)onChangeIconTapped:(id)sender {
    UIStoryboard *s = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    IKIconsListViewController *vc = [s instantiateViewControllerWithIdentifier:@"IconList"];
    vc.delegate = self;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)onChangeTeplateTapped:(id)sender {
    if (self.editingIndexPath) {
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:self.editingIndexPath];
        if ([cell isKindOfClass:[IKFieldCell class]]) {
            IKFieldCell *editingCell = (IKFieldCell *)cell;
            if ([editingCell.fieldField isFirstResponder]) {
                [editingCell.fieldField resignFirstResponder];
            }
        }
    }
    
    if ([_card hasFilledCustomFields]) {
        [UIAlertView bk_showAlertViewWithTitle:@"Sorry" message:@"You cannot change Type, because you have filled values" cancelButtonTitle:@"Ok" otherButtonTitles:@[] handler:nil];
        return;
    }
    
    UIStoryboard *s = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    IKTemplatesListViewController *vc = [s instantiateViewControllerWithIdentifier:@"TemplateList"];
    vc.isMediator = YES;
    vc.currentTemplate = _card.template;
    vc.delegate = self;
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)onChangeFieldTypeTapped:(id)sender {
    UIStoryboard *s = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];

    IKTypeEditViewController *vc = [s instantiateViewControllerWithIdentifier:@"TypeEdit"];
    
    UITableViewCell *cell = (UITableViewCell *)[[[sender superview] superview] superview];
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    
    vc.card = self.card;
    
    if (indexPath.section == 0) {
        IKCardField *field = [_customFields objectAtIndex:indexPath.row];
        vc.delegate = self;
        vc.field = field;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

-(void) presentModalView:(UIViewController *)controller {
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
    
    [self presentViewController:controller animated:YES completion:nil];
}

- (void)onAddFiledTapped:(id)sender {
    IKCardField *f = [self.card createPasswordField];
    
    [_customFields addObject:f];
    NSInteger index = [_customFields count] - 1;
    NSIndexPath *insertedIndexPath = [NSIndexPath indexPathForRow:index inSection:0];
    [self.tableView beginUpdates];
    [self.tableView insertRowsAtIndexPaths:@[insertedIndexPath] withRowAnimation:UITableViewRowAnimationTop];
    [self.tableView endUpdates];
}

- (void)onRemoveCardTapped:(id)sender {
    UIActionSheet *actionShet = [UIActionSheet bk_actionSheetWithTitle:nil];
    [actionShet bk_setDestructiveButtonWithTitle:@"Delete Card" handler:^{
        [[NSManagedObjectContext MR_defaultContext] deleteObject:_card];
        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
        [self.navigationController popToRootViewControllerAnimated:YES];
        [self.delegate onSaveCard:_card viewContoller:self];
    }];
    
    [actionShet bk_setCancelButtonWithTitle:@"Cancel" handler:^{
        
    }];
    [actionShet showInView:self.view];
}

- (void)setDescriptionFromUIToCard {
    NSString *text = [[_templateView descriptionField] text];
    
    if (![[_card descriptionField] value]) {
        _card.descriptionField.value = [IKFieldValue MR_createEntity];
    }
    _card.descriptionField.value.value = text;
}

#pragma mark - Data Methods
- (void)replaceCardFieldsFromTemplate {
    [_card replaceFieldsFromTemplate:_card.template];
}

- (void)replaceCardIconFromTemplate {
    _card.iconName = _card.template.imageName;
    _card.iconColor = _card.template.imageColor ? _card.template.imageColor : [UIColor blackColor];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        NSInteger count = [_customFields count];
        if ([self.tableView isEditing]) {
            count++;
        }
        return count;
    } else if (section == 1) {
        if ([[_card tagLinks] count] == 0 && !tableView.isEditing) {
            return 0;
        } else {
            return 1;
        }
    } else if (section == 2) {
        if (![[_card noteField] hasFilledValue] && !tableView.isEditing) {
            return 0;
        } else {
            return 1;
        }
    }
    return 0;
}

- (void)reloadTemplateView {
    UIImage *icon = [_card icon];
    
    UIColor *tintColor = _card.iconColor;
    if (!tintColor) {
        tintColor = [UIColor blackColor];
    }
    icon = [icon rt_tintedImageWithColor:tintColor];
    
    [_templateView.iconBtn setBackgroundImage:icon forState:UIControlStateNormal];
    
    NSString *categoryName = _card.template.name;
    [_templateView.categoryBtn setTitle:categoryName forState:UIControlStateNormal];
    
    IKCardField *field = [_card descriptionField];
    [field.type customizeTextField:_templateView.descriptionField];
    _templateView.descriptionField.autocapitalizationType = UITextAutocapitalizationTypeWords;
    

    IKFieldValue *value = field.value;
    if (value) {
        _templateView.descriptionField.text = value.value;
    } else {
        _templateView.descriptionField.text = @"";
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *FieldCellIdentifier = @"FieldCell";
    static NSString *FieldShowCellIdentifier = @"FieldShowCell";
//    static NSString *TemplateCellIdentifier = @"TemplateCell";
    static NSString *DefaultCellIdentifier = @"Cell";
    static NSString *NoteCellIdentifier = @"NoteCell";
    static NSString *TagsCellIdentifier = @"TagsCell";
    
    if (indexPath.section == 0){
        if (![self.tableView isEditing]) {
            IKCardShowFieldCell *cell = [tableView dequeueReusableCellWithIdentifier:FieldShowCellIdentifier];
            
            IKCardField *field = [_customFields objectAtIndex:indexPath.row];
            NSString *name = field.name;
            cell.fieldNameLabel.text = name;
            cell.fieldValueLabel.textColor = UIColorFromRGB(0xE83500);
            IKFieldValue *value = [field value];
            NSString *text = [value value];
            UIFont *f = [UIFont systemFontOfSize:17.0];
            UIColor *valueFontColor = [UIColor blackColor];
            
            if ([field.type isPasswordType]) {
                f = [IKFonts cousineRegularWithSize:17.0];
                valueFontColor = UIColorFromRGB(0x336699);
            }
            
            
            cell.fieldValueLabel.font = f;
            cell.fieldValueLabel.textColor = valueFontColor;
            cell.fieldValueLabel.text = text;
            return cell;
        }
        
        if (indexPath.row == [_customFields count]) {
            UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:DefaultCellIdentifier forIndexPath:indexPath];
            return cell;
        }
        
        IKFieldCell *cell = [tableView dequeueReusableCellWithIdentifier:FieldCellIdentifier forIndexPath:indexPath];
        IKCardField *field = _customFields[indexPath.row];
        NSString *fieldName = field.name;
        [cell.fieldButton setTitle:fieldName forState:UIControlStateNormal];
        [field.type customizeTextField:cell.fieldField];
        IKFieldValue *value = field.value;
        if (value) {
            cell.fieldField.text = value.value;
        } else {
            cell.fieldField.text = @"";
        }
        return cell;
    } else if (indexPath.section == 1 && indexPath.row == 0) {
        IKCardTagsCell *cell = [tableView dequeueReusableCellWithIdentifier:TagsCellIdentifier forIndexPath:indexPath];
        
        
        NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"listOrder" ascending:YES];
        NSArray *tagLinks = [[_card tagLinks] sortedArrayUsingDescriptors:@[sortDescriptor]];
        NSMutableArray *tags = [NSMutableArray array];
        for (IKTagCardLink *l in tagLinks) {
            IKTag *tag = l.tag;
            [tags addObject:tag.name];
        }
        [cell.tagView clear];
        [cell.tagView addTags:tags];
        
        cell.tagView.delegate = self;

//        cell.tokenFieldView.tokenField.tag = 100;
//        [_tagViews setObject:cell.tokenFieldView forKey:indexPath];
//        
//        NSArray *tagTitles = [IKTag allTagTitles];
//        [cell.tokenFieldView setSourceArray:tagTitles];
//        cell.tokenFieldView.separator.backgroundColor = [UIColor whiteColor];
//        [cell.tokenFieldView setShowAlreadyTokenized:YES];
//        [cell.tokenFieldView.tokenField setTokenizingCharacters:[NSCharacterSet characterSetWithCharactersInString:@",;. "]]; // Default is a comma
//        cell.tokenFieldView.tokenField.removesTokensOnEndEditing = NO;
//        [cell.tokenFieldView.tokenField setPromptText:@""];
//        cell.tokenFieldView.tokenField.font = [UIFont systemFontOfSize:15.0];
//        [cell.tokenFieldView.tokenField setPlaceholder:@"Add Tag"];
//        cell.tokenFieldView.resultsTable.backgroundColor = [UIColor whiteColor];
//        cell.tokenFieldView.tokenField.delegate =  nil;
//        
//        [cell.tokenFieldView.tokenField removeAllTokens];
//        NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"listOrder" ascending:YES];
//        NSArray *tagLinks = [[_card tagLinks] sortedArrayUsingDescriptors:@[sortDescriptor]];
//        for (IKTagCardLink *l in tagLinks) {
//            IKTag *tag = l.tag;
//            [cell.tokenFieldView.tokenField addTokenWithTitle:[tag name] representedObject:l];
//        }
//        cell.tokenFieldView.tokenField.delegate =  self;
//        
//        [cell.tokenFieldView.tokenField addTarget:self action:@selector(tokenFieldChangedEditing:) forControlEvents:UIControlEventEditingDidBegin];
//        [cell.tokenFieldView.tokenField addTarget:self action:@selector(tokenFieldChangedEditing:) forControlEvents:UIControlEventEditingDidEnd];
        return cell;
    } else if (indexPath.section == 2 && indexPath.row == 0) {
        IKCardNoteCell *cell = [tableView dequeueReusableCellWithIdentifier:NoteCellIdentifier forIndexPath:indexPath];
        cell.noteTextView.font = [UIFont systemFontOfSize:15.0];
        
        IKCardField *field = [_card noteField];
        
        [_textViews setObject:cell.noteTextView forKey:indexPath];
        
        IKFieldValue *value = field.value;
        NSString *textValue  = value.value? value.value : @"";
        
        cell.noteTextView.text = textValue;
        return cell;
    }
    return nil;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView.isEditing) {
        return YES;
    }
    return NO;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0 && [tableView isEditing]) {
        return 44;
    } else if (indexPath.section == 0 && ![tableView isEditing]) {
        return [self textHightForRowAtIndexPath:indexPath];
    } else if (indexPath.section == 1 && indexPath.row == 0) {
        if (_tagsEditing && ([indexPath isEqual:self.editingIndexPath])) {
            return 44;
//            return [self tagViewHeightForRowAtIndexPath:indexPath] + 60;
        } else {
            return 44;
//            return [self tagViewHeightForRowAtIndexPath:indexPath];;
        }
    } else if (indexPath.section == 2 && indexPath.row == 0){
        return [self textViewHeightForRowAtIndexPath:indexPath];
    } else {
        return 44.0;
    }
}

- (CGFloat)textHightForRowAtIndexPath:(NSIndexPath *)indexPath {
    IKCardField *field = _customFields[indexPath.row];
    IKFieldValue *value  = [field value];
    NSString *text = value.value? value.value : @"";
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:17.0]};
    
    NSAttributedString *attrStr = [[NSAttributedString alloc] initWithString:text attributes:attributes];
    CGRect textRect = [attrStr boundingRectWithSize:CGSizeMake(275.0, FLT_MAX) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) context:nil];
    CGFloat height = textRect.size.height;
    height = height + 26 + 5;
    
    return MAX(height, 60);
}

- (CGFloat)textViewHeightForRowAtIndexPath: (NSIndexPath*)indexPath {
    IKCardField *field = [_card noteField];
    IKFieldValue *value  = [field value];
    
    NSString *textValue  = value.value? value.value : @"";
    
    UITextView *t = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    t.font = [UIFont systemFontOfSize:15.0];
    
    CGFloat width = 280.0;
    if (self.tableView.isEditing) {
        width = 300.0;
    }
    t.frame = CGRectMake(0, 0, width, 44);
    t.text = textValue;
    
    CGSize size = [t sizeThatFits:CGSizeMake(width, FLT_MAX)];
    CGFloat h1 = size.height + 32;
    
    CGFloat h = MAX (100.0, h1);
    NSLog(@"%f", h);
    return h;
}

//- (CGFloat)tagViewHeightForRowAtIndexPath: (NSIndexPath*)indexPath {
//    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"listOrder" ascending:YES];
//    NSArray *tagLinks = [[_card tagLinks] sortedArrayUsingDescriptors:@[sortDescriptor]];
//    
//    CGFloat maxHeight = 0;
//    
//    if (!_calculationView) {
//        _calculationView = [[TITokenFieldView alloc] initWithFrame:CGRectMake(0, 0, 285, 44)]   ;
//        [_calculationView setShowAlreadyTokenized:YES];
//        _calculationView.tokenField.font = [UIFont systemFontOfSize:15.0];
//        [_calculationView.tokenField setTokenizingCharacters:[NSCharacterSet characterSetWithCharactersInString:@",;. "]]; // Default
//        [_calculationView.tokenField setPromptText:@""];
//        [_calculationView.tokenField setPlaceholder:@"Add Tag"];
//    }
//    if (self.tableView.isEditing) {
//        _calculationView.frame = CGRectMake(0, 0, 275, 44);
//        [_calculationView.tokenField setPlaceholder:@"Add Tag"];
//    } else {
//        _calculationView.frame = CGRectMake(0, 0, 268, 44);
//        [_calculationView.tokenField setPlaceholder:@""];
//    }
//    
//    [_calculationView.tokenField removeAllTokens];
//    
//    for (IKTagCardLink *l in tagLinks) {
//        IKTag *tag = l.tag;
//        TIToken *token = [_calculationView.tokenField addTokenWithTitle:[tag name] representedObject:l];
////        maxHeight = MAX(maxHeight, CGRectGetMaxY([token frame]));
//    }
//
//    CGFloat h = [_calculationView.tokenField layoutTokensInternal];
//    return MAX(90, h);
//}

- (void)textViewDidChange:(UITextView *)textView {
    NSString *text = textView.text;
    
    NSRange selectedRange = textView.selectedRange;
    textView.scrollEnabled = NO;
    
    textView.text = nil;
    textView.text = text;
    
    selectedRange.length = 0;
    
    textView.selectedRange = selectedRange;
    textView.scrollEnabled = YES;
    
    IKCardField *f = [_card noteField];
    
    if (!f.value) {
        f.value = [IKFieldValue MR_createEntity];
    }
    NSString *fieldValue = text;
    f.value.value = fieldValue;

    [self.tableView beginUpdates];
    [self.tableView endUpdates];
    
    [self scrollToCursorForTextView:textView];
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    [self scrollToCursorForTextView:textView];
    
    UITableViewCell *cell = (UITableViewCell *)[[[textView superview] superview] superview];
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    self.editingIndexPath = indexPath;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    IKCardField *f = [_card noteField];
    
    if (!f.value) {
        f.value = [IKFieldValue MR_createEntity];
    }
    NSString *fieldValue = [[textView attributedText] string];
    f.value.value = fieldValue;
    
    self.editingIndexPath = nil;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    return YES;
}

- (void)scrollToCursorForTextView:(UITextView *)textView {
    
    CGRect cursorRect = [textView caretRectForPosition:textView.selectedTextRange.start];
    
    cursorRect = [self.tableView convertRect:cursorRect fromView:textView];
    
    if (![self rectVisible:cursorRect]) {
        cursorRect.size.height += 10; // To add some space underneath the cursor
        [self.tableView scrollRectToVisible:cursorRect animated:YES];
    }
}

- (BOOL)rectVisible: (CGRect)rect {
    CGRect visibleRect;
    visibleRect.origin = self.tableView.contentOffset;
    visibleRect.origin.y += self.tableView.contentInset.top;
    visibleRect.size = self.tableView.bounds.size;
    visibleRect.size.height -= self.tableView.contentInset.top + self.tableView.contentInset.bottom;
    
    return CGRectContainsRect(visibleRect, rect);
}


- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (indexPath.row == [_customFields count]) {
            return UITableViewCellEditingStyleInsert;
        } else {
            return UITableViewCellEditingStyleDelete;
        }
    } else {
        return UITableViewCellEditingStyleNone;
    }
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    NSMutableArray *things = _customFields;
    IKCardTemplate *thing = [_customFields objectAtIndex:fromIndexPath.row];
    
    [things removeObject:thing];
    [things insertObject:thing atIndex:[toIndexPath row]];
    
    int i = 0;
    for (NSManagedObject *mo in things)
    {
        [mo setValue:[NSNumber numberWithInt:i++] forKey:@"listOrder"];
    }
}

- (NSIndexPath *)tableView:(UITableView *)tableView targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath *)sourceIndexPath toProposedIndexPath:(NSIndexPath *)proposedDestinationIndexPath
{
    if (proposedDestinationIndexPath.section == 0) {
        NSInteger row = MIN([tableView numberOfRowsInSection:sourceIndexPath.section] - 2, proposedDestinationIndexPath.row);
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
        return indexPath;
    } else  {
        NSInteger row = [tableView numberOfRowsInSection:0] - 2;
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
        return indexPath;
    }
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row != [_customFields count]) {
        return YES;
    }
    return NO;
}

- (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return YES;
    } else {
        return NO;
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        IKCardField *f = [_customFields objectAtIndex:indexPath.row];
        [_customFields removeObjectAtIndex:indexPath.row];
        [_card removeFieldsObject:f];
        
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        [self onAddFiledTapped:nil];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (self.tableView.isEditing) {
        if (indexPath.section == 0 && indexPath.row == [_customFields count]) {
            [self onAddFiledTapped:nil];
        }
    } else {
        if (indexPath.section == 0) {
            IKCardShowFieldCell *cell = (IKCardShowFieldCell *)[tableView cellForRowAtIndexPath:indexPath];
            
            UIImage *tinted = [[UIImage imageNamed:@"copy"] rt_tintedImageWithColor:[UIColor whiteColor]];
            [SVProgressHUD appearance].hudSuccessImage = tinted;
            [SVProgressHUD appearance].hudForegroundColor = [UIColor whiteColor];
            [SVProgressHUD showSuccessWithStatus:@"Copied"];
            
            
            NSString *text = [cell.fieldValueLabel text];
            [[IKPasteboard sharedInstance] setToPasteboard:text];
        }
    }
}


#pragma mark - IconList Delegate
- (void)onSelectIcon:(UIImage *)iconImage iconName:(NSString *)iconName color:(UIColor *)color inViewcontroller:(UIViewController *)vc
{
    [self.navigationController popToViewController:self animated:YES];
    
    _card.iconName = iconName;
    [_card setValue:color forKey:@"iconColor"];
    
    [self reloadTemplateView];
}

- (void)onSelectTemplate:(IKCardTemplate *)cardTemplate
        inViewController:(IKTemplatesListViewController *)vc
{
    [self.navigationController popViewControllerAnimated:YES];
    
    _cardTemplate = cardTemplate;
    _card.template = _cardTemplate;
    [self replaceCardFieldsFromTemplate];
    [self replaceCardIconFromTemplate];
    [self setDescriptionFromUIToCard];

    _customFields = [[_card orderedCustomFields] mutableCopy];

    [self.tableView reloadData];
    [self reloadTemplateView];
}

#pragma mark - Type Edit Delegate
- (void)onCancelEditField:(IKBaseField *)field viewController:(UIViewController *)vc {
    
}

- (void)onSaveEditField:(IKBaseField *)field viewController:(UIViewController *)vc {
    NSInteger index = -1;
    for (int i = 0; i < [_customFields count]; i++) {
        if (_customFields[i] == field) {
            index = i;
        }
    }
    [self.tableView beginUpdates];
    
    if (index == -1) {
        [_customFields addObject:field];
        index = [_customFields count] - 1;
        NSIndexPath *insertedIndexPath = [NSIndexPath indexPathForRow:index inSection:0];
        [self.tableView insertRowsAtIndexPaths:@[insertedIndexPath] withRowAnimation:UITableViewRowAnimationNone];
    } else {
        NSIndexPath *updatedIndexPath = [NSIndexPath indexPathForRow:index inSection:0];
        [self.tableView reloadRowsAtIndexPaths:@[updatedIndexPath] withRowAnimation:UITableViewRowAnimationNone];
    }
    
    [self.tableView endUpdates];
}

#pragma mark - TextField Delegate
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (textField == _templateView.descriptionField) {
        self.editingIndexPath = nil;
    } else {
        UITableViewCell *cell = (UITableViewCell *)[[[textField superview] superview] superview];
        NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
        self.editingIndexPath = indexPath;
    }
    

}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    IKCardField *f;
    
    if (textField == _templateView.descriptionField) {
        f = [_card descriptionField];
    } else {
        UITableViewCell *cell = (UITableViewCell *)[[[textField superview] superview] superview];
        NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
        if (indexPath.section == 2) {
            f = [_card noteField];
        } else {
            f = [_customFields objectAtIndex:indexPath.row];
        }
    }
    
    if (!f.value) {
        f.value = [IKFieldValue MR_createEntity];
    }
    NSString *fieldValue = [textField text];
    f.value.value = fieldValue;
    
    self.editingIndexPath = nil;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}



#pragma mark - TokenFieldView Delegate
//- (void)tokenFieldChangedEditing:(TITokenField *)tokenField {
//	[tokenField setRightViewMode:(tokenField.editing ? UITextFieldViewModeAlways : UITextFieldViewModeNever)];
//    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:1];
//    
//    
//    self.editingIndexPath = indexPath;
//    
//    _tagsEditing = [tokenField isEditing];
//    
//    [self.tableView beginUpdates];
//    [self.tableView endUpdates];
//}
//
//
//
//- (void)tokenField:(TITokenField *)tokenField didAddToken:(TIToken *)token {
//    NSString *tagTitle = [token title];
//    
//    IKTagCardLink *link = [_card addTagWithName:tagTitle];
//    token.representedObject = link;
//
//}
//- (void)tokenField:(TITokenField *)tokenField didRemoveToken:(TIToken *)token {
//    IKTagCardLink *link = (IKTagCardLink *)[token representedObject];
//    if (link) {
//        [_card removeTagLinksObject:link];
//    }
//}
//- (BOOL)tokenField:(TITokenField *)tokenField willRemoveToken:(TIToken *)token {
//    return YES;
//}
//- (CGFloat)tokenField:(TITokenField *)tokenField resultsTableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    return 30;
//}
//- (UITableViewCell *)tokenField:(TITokenField *)tokenField resultsTableView:(UITableView *)tableView cellForRepresentedObject:(id)object {
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
//    if (!cell) {
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
//        cell.textLabel.font = [UIFont systemFontOfSize:15.0];
//    }
//    cell.textLabel.text = object;
//    
//    return cell;
//}
//- (BOOL)shouldDissmissOnEmptyReturnWithTokenField:(TITokenField *)tokenField {
//    return YES;
//}

- (void)tagWriteViewWillBeginEditing:(HKKTagWriteView *)view {
    self.editingIndexPath = [NSIndexPath indexPathForRow:0 inSection:1];
}

- (void)tagWriteViewWillEndEditing:(HKKTagWriteView *)view {
    self.editingIndexPath = nil;
}

- (void)tagWriteViewDidBeginEditing:(HKKTagWriteView *)view {
    
}

- (void)tagWriteViewDidEndEditing:(HKKTagWriteView *)view {
    
}

- (void)tagWriteView:(HKKTagWriteView *)view didChangeText:(NSString *)text {
    
}

- (void)tagWriteView:(HKKTagWriteView *)view didMakeTag:(NSString *)tag {
    [_card addTagWithName:tag];
}

- (void)tagWriteView:(HKKTagWriteView *)view didRemoveTag:(NSString *)tag {
    [_card removeTagWithName:tag];
}



@end
