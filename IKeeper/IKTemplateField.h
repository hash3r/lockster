//
//  IKTemplateField.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 06.01.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "IKBaseField.h"

@class IKCardTemplate;

@interface IKTemplateField : IKBaseField

@property (nonatomic, retain) IKCardTemplate *cardTemplate;

@end
