//
//  IKMainViewController.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 06.01.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IKCardTemplate.h"

typedef enum {
    CardsFilterModeAll = 0,
    CardsFilterModeFavorites = 1,
    CardsFilterModeCustom = 2
    } CardsFilterMode;

@interface IKMainViewController : UIViewController

@property (nonatomic, weak) IBOutlet UIImageView *backgroundImageView;
@property (nonatomic, assign) CardsFilterMode mode;
@property (nonatomic, strong) IKCardTemplate *cardTemplate;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *origin;
@property (nonatomic, assign) BOOL statusBarHidden;

- (IBAction)onMenuBarBtnTapped:(id)sender;
- (IBAction)onAddBarBtnTapped:(id)sender;
@end
