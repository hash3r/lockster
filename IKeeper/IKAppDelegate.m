//
//  IKAppDelegate.m
//  IKeeper
//
//  Created by Chebulaev Oleg on 22.12.13.
//  Copyright (c) 2013 Chebulaev Oleg. All rights reserved.
//

#import "IKAppDelegate.h"
#import "EncryptedStore.h"
#import <DropboxSDK/DropboxSDK.h>
#import "IKCardTemplate.h"
#import "IKFieldType.h"
#import <MMDrawerController/MMDrawerController.h>
#import <MMDrawerController/MMDrawerVisualState.h>
#import "IKCard.h"
#import "APPass.h"
#import "APViewController.h"
#import <IKUIApplication.h>
#import "IKSettings.h"
#import "IDMSyncManager.h"
#import <MTDates/NSDate+MTDates.h>
#import <MMExampleDrawerVisualStateManager.h>
#import "UINavigationController+StatusBarStyle.h"
#import "IKMainViewController.h"
#import "IKMenuViewController.h"

@implementation IKAppDelegate  {
    UIAlertView *cloudContentCorruptedAlert;
    UIAlertView *cloudContentHealingAlert;
    UIAlertView *handleCloudContentWarningAlert;
    UIAlertView *handleLocalStoreAlert;
    NSManagedObjectContext *managedObjectContext;
}

-(BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
//    [NSDate mt_setFirstDayOfWeek:2];
    [NSDate mt_setLocale:[NSLocale currentLocale]];
    [NSDate mt_setTimeZone:[NSTimeZone localTimeZone]];
    
    NSError *error = nil;
    
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"ic"]) {
        [fileManager removeItemAtPath:[[fileManager URLForUbiquityContainerIdentifier:nil] path] error:&error];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"ic"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }

    
    [fileManager createDirectoryAtURL:self.storeDirectoryURL withIntermediateDirectories:YES attributes:nil error:NULL];
    
    
//    NSArray *allPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentsDirectory = [allPaths objectAtIndex:0];
//    NSString *backupsPath = [documentsDirectory stringByAppendingPathComponent:@"backups"];
//    [fileManager createDirectoryAtPath:backupsPath
//                              withIntermediateDirectories:YES
//                                               attributes:nil
//                                                    error:&error];
//    
//    NSString *archivesPath = [documentsDirectory stringByAppendingPathComponent:@"archives"];
//    [fileManager createDirectoryAtPath:archivesPath
//           withIntermediateDirectories:YES
//                            attributes:nil
//                                 error:&error];
    
    
    [self setupContext];
    
    CDESetCurrentLoggingLevel(CDELoggingLevelVerbose);
    
    IDMSyncManager *syncManager = [IDMSyncManager sharedSyncManager];
    syncManager.managedObjectContext = [NSManagedObjectContext MR_defaultContext];
    syncManager.storePath = self.storeURL.path;
    [syncManager setup];

    UIStoryboard *s = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    UIViewController *centerNavigationController = [s instantiateViewControllerWithIdentifier:@"MainNavigationController"];
    UIViewController *leftDrawerController = [s instantiateViewControllerWithIdentifier:@"MenuViewController"];
    
    MMDrawerController *drawerController = [[MMDrawerController alloc]
                                            initWithCenterViewController:centerNavigationController
                                            leftDrawerViewController:leftDrawerController];
//    drawerController.delegate = self;
    
    [drawerController setShowsShadow:YES];
    [drawerController setMaximumLeftDrawerWidth:320.f];
    [drawerController setMaximumRightDrawerWidth:233.f];
    [drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
    drawerController.animationVelocity = 1150;
    
//    __weak IKAppDelegate *weakSelf = self;
    [drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
    [drawerController
     setDrawerVisualStateBlock:^(MMDrawerController *drawerController, MMDrawerSide drawerSide, CGFloat percentVisible) {
//         weakSelf.oldPercentVisible = weakSelf.newPercentVisible;
//         weakSelf.newPercentVisible = percentVisible;
//         if (weakSelf.oldPercentVisible == weakSelf.newPercentVisible && (self.oldPercentVisible == 0.0 || self.oldPercentVisible == 1.0)) {
//             [weakSelf setStatusBarHidden:NO];
//         } else {
//             [weakSelf setStatusBarHidden:YES];
//         }

         
         MMDrawerControllerDrawerVisualStateBlock block;
         block = [[MMExampleDrawerVisualStateManager sharedManager]
                  drawerVisualStateBlockForDrawerSide:drawerSide];
         if(block){
             block(drawerController, drawerSide, percentVisible);
         }
     }];
    
    [drawerController addObserver:self forKeyPath:@"isDragged" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld) context:NULL];
    [drawerController addObserver:self forKeyPath:@"openSide" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld) context:NULL];

    self.window.rootViewController = drawerController;
    
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)performLaunchSteps {
    BOOL isLocked = IMSCryptoManagerIsLocked();
    
    UIViewController *rootVc = [self.window rootViewController];
    BOOL isShowAppVC = NO;
    if (rootVc.presentedViewController.presentedViewController) {
        isShowAppVC = [[[[self.window rootViewController] presentedViewController] presentedViewController] isKindOfClass:[APViewController class]] ? YES : NO;
    } else {
        isShowAppVC = [[[self.window rootViewController] presentedViewController] isKindOfClass:[APViewController class]] ? YES : NO;
    }
    
    NSLog(@"%@", [NSString stringWithFormat:@"IS LOCKED:%d  ISSHOWAPPVC %d", isLocked, isShowAppVC]);
    
    if (isLocked && !isShowAppVC) {
        self.lastStatusBarStyle = [[UIApplication sharedApplication] statusBarStyle];
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        APViewController *apc = [[APViewController alloc] init];
        apc.delegate = self;
        
        if (rootVc.presentedViewController) {
            [self.window.rootViewController.presentedViewController presentViewController:apc animated:NO completion:nil];
        } else {
            [self.window.rootViewController presentViewController:apc animated:NO completion:nil];
        }
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    if ([keyPath isEqual:@"isDragged"]) {
//        NSNumber *value = [change objectForKey:NSKeyValueChangeNewKey];
//        BOOL isDragged = [value integerValue];
//        NSLog(@"DRAGGED:%@", isDragged?@"yes":@"no");
//        if (!isDragged) {
//             [self setStatusBarHidden:NO];
//        }
    } else if ([keyPath isEqualToString:@"openSide"]) {
//        NSNumber *value = [change objectForKey:NSKeyValueChangeNewKey];
//        MMDrawerSide newSide = [value integerValue];
//        if (newSide == MMDrawerSideLeft) {
//            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
//        } else if (newSide == MMDrawerSideNone){
//            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
//        }
        
    } else {
        [super observeValueForKeyPath:keyPath
                             ofObject:object
                               change:change
                              context:context];
    }
}

- (void)onStartDrugging:(MMDrawerController *)vc {
//    NSLog(@"Start");
    [self setStatusBarHidden:YES];
    
}

- (void)onFinishDrugging:(MMDrawerController *)vc close:(NSNumber *)close {
//    NSLog(@"Finish");
    [self setStatusBarHidden:NO];
    BOOL closeSide = [close boolValue];
    if (closeSide) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    } else {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    }
}


- (void)setStatusBarHidden:(BOOL)hidden {
    if (_hiddenStB != hidden) {
        _hiddenStB = hidden;
        MMDrawerController *drawerController = (MMDrawerController *)self.window.rootViewController;
        IKMainViewController *vc = (IKMainViewController *)[(UINavigationController *)drawerController.centerViewController topViewController];
        IKMenuViewController *lVc = (IKMenuViewController *)drawerController.leftDrawerViewController;
        [[UIApplication sharedApplication] setStatusBarHidden:hidden];
        if ([vc isKindOfClass:[IKMainViewController class]]) {
            vc.statusBarHidden = hidden;
        }
        lVc.statusBarHidden = hidden;
        [vc setNeedsStatusBarAppearanceUpdate];
        [lVc setNeedsStatusBarAppearanceUpdate];
        
    }
}

- (void)validUserAccess:(APViewController *)controller {
    NSLog(@"validUserAccess - Delegate");
    [[UIApplication sharedApplication] setStatusBarStyle:self.lastStatusBarStyle];
    
    if (self.window.rootViewController.presentedViewController.presentedViewController) {
        [self.window.rootViewController.presentedViewController dismissViewControllerAnimated:YES completion:nil];
    } else {
        [self.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
    }
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:19]}];
    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"seed"]) {
        [IKFieldType seed];
        [IKCardTemplate seed];
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"seed"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidTimeout:)
                                                 name:kApplicationDidIdleNotification object:nil];
    
    return YES;
}

- (void) applicationDidTimeout:(NSNotification *) notif {
    [(IKUIApplication *)[UIApplication sharedApplication] resetIdleTimer];
    
    BOOL isLocked = IMSCryptoManagerIsLocked();
    
    if (!isLocked) {
        IMSCryptoManagerPurge();
        APViewController *apc = [[APViewController alloc] init];
        apc.delegate = self;
        self.lastStatusBarStyle = [[UIApplication sharedApplication] statusBarStyle];
        
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        if ([self.window.rootViewController presentedViewController]) {
            [self.window.rootViewController.presentedViewController presentViewController:apc animated:YES completion:nil];
        } else {
            [self.window.rootViewController presentViewController:apc animated:YES completion:nil];
        }
    }
}

- (void)applicationWillResignActive:(UIApplication *)application {
    BOOL lockOnExit = [[IKSettings sharedInstance] lockOnExit];
    if (lockOnExit) {
        
        BOOL isLocked = IMSCryptoManagerIsLocked();
        if (!isLocked) {
            IMSCryptoManagerPurge();
            
            APViewController *apc = [[APViewController alloc] init];
            apc.delegate = self;
            self.lastStatusBarStyle = [[UIApplication sharedApplication] statusBarStyle];
            
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
            
            if (self.window.rootViewController.presentedViewController) {
                [self.window.rootViewController.presentedViewController presentViewController:apc animated:NO completion:nil];
            } else {
                [self.window.rootViewController presentViewController:apc animated:NO completion:nil];
            }
        }
    }
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    UIBackgroundTaskIdentifier identifier = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:NULL];
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [managedObjectContext performBlock:^{
            if (managedObjectContext.hasChanges) {
                [managedObjectContext save:NULL];
            }
            
            [[IDMSyncManager sharedSyncManager] synchronizeWithCompletion:^(NSError *error) {
                [[UIApplication sharedApplication] endBackgroundTask:identifier];
            }];
        }];
    });
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [[IDMSyncManager sharedSyncManager] synchronizeWithCompletion:NULL];
    [self performLaunchSteps];
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    return [[IDMSyncManager sharedSyncManager] handleOpenURL:url];
}


- (void)applicationWillTerminate:(UIApplication *)application {
    [self.window.rootViewController removeObserver:self forKeyPath:@"openSide"];
    [self.window.rootViewController removeObserver:self forKeyPath:@"isDragged"];
}

#pragma mark - Core Data stack
- (void)setupContext
{
    /* init model */
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Store" withExtension:@"momd"];
    NSManagedObjectModel *model = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    
    
    /* init persistent store coordinator */
//    NSPersistentStoreCoordinator *coordinator = [EncryptedStore makeStore:model :@"PASS"];
    NSError *error = nil;
    NSPersistentStoreCoordinator *coordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:model];
    NSDictionary *options = @{NSMigratePersistentStoresAutomaticallyOption: @YES, NSInferMappingModelAutomaticallyOption: @YES};
    [coordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:self.storeURL options:options error:&error];
    
    
    [NSPersistentStoreCoordinator MR_setDefaultStoreCoordinator:coordinator];
    
    /* init context */
    [NSManagedObjectContext MR_initializeDefaultContextWithCoordinator:coordinator];
    
    
    /* init undo manager */
    NSUndoManager *undoManager = [[NSUndoManager alloc] init];
    [[NSManagedObjectContext MR_defaultContext] setUndoManager:undoManager];


}

- (NSURL *)storeDirectoryURL
{
    NSURL *directoryURL = [[NSFileManager defaultManager] URLForDirectory:NSApplicationSupportDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:YES error:NULL];
    directoryURL = [directoryURL URLByAppendingPathComponent:NSBundle.mainBundle.bundleIdentifier isDirectory:YES];
    return directoryURL;
}

- (NSURL *)storeURL
{
    NSURL *storeURL = [self.storeDirectoryURL URLByAppendingPathComponent:@"iKeeper.sqlite"];
    return storeURL;
}

@end
