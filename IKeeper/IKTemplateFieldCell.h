//
//  IKTemplateFieldCell.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 01.02.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IKTemplateFieldCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIButton *fieldButton;

@end
