//
//  IKSettingsSwitchCell.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 03.03.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IKSettingsSwitchCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UISwitch *settingSwitch;

@end
