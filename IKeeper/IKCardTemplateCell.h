//
//  IKCardTemplateCell.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 13.01.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SAMGradientView/SAMGradientView.h>

@interface IKCardTemplateCell : UITableViewCell {
    UIImage *_iconImage;
}

@property (nonatomic, weak) IBOutlet UIView *roundView;
@property (nonatomic, weak) IBOutlet UITextField *descriptionField;
@property (nonatomic, weak) IBOutlet UIButton *iconBtn;
@property (nonatomic, weak) IBOutlet UIButton *categoryBtn;
@property (nonatomic, weak) IBOutlet UIImageView *indicator;

@end
