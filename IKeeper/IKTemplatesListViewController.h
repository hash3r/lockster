//
//  IKTemplatesListViewController.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 10.01.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@class IKTemplatesListViewController;
@class IKCardTemplate;

@protocol IKTemplatesListViewControllerDelegate
- (void)onSelectTemplate:(IKCardTemplate *)cardTemplate inViewController:(IKTemplatesListViewController *)vc;
- (void)onCancelSelectTemplateInViewController:(IKTemplatesListViewController *)vc;
@end

@interface IKTemplatesListViewController : UITableViewController

@property (nonatomic, assign) BOOL isMediator;
@property (nonatomic, assign) BOOL isNeedShowDetail;
@property (nonatomic, assign) BOOL startEditing;
@property (nonatomic, assign) IKCardTemplate *currentTemplate;
@property (nonatomic, weak) id<IKTemplatesListViewControllerDelegate>delegate;

- (void)onCancelTapped:(id)sender;
- (void)onEditTapped:(id)sender;
- (void)onDoneTapped:(id)sender;
- (void)onAddTapped:(id)sender;

@end
