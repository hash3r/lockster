//
//  IKColorCell.m
//  IKeeper
//
//  Created by Chebulaev Oleg on 13.05.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import "IKColorCell.h"

@implementation IKColorCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
