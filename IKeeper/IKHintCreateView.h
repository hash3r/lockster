//
//  IKHintCreateView.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 25.03.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IKHintCreateView : UIView <UITextFieldDelegate>{
    UIView *_hintView;
}

@property (nonatomic, strong) UIButton *useEmptyButton;
@property (nonatomic, strong) UIButton *doneButton;
@property (nonatomic, strong) UITextField *textField;

- (void)show;
- (void)hideAndRemove;

@end
