//
//  IKWallaperSettingsViewController.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 18.02.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IKWallaperSettingsViewController : UITableViewController

- (IBAction)onScreen1Tapped:(id)sender;
- (IBAction)onScreen2Tapped:(id)sender;
- (IBAction)onScreen3Tapped:(id)sender;

@end
