//
//  UINavigationController+StatusBarStyle.m
//  IKeeper
//
//  Created by Chebulaev Oleg on 17.04.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import "UINavigationController+StatusBarStyle.h"

@implementation UINavigationController (StatusBarStyle)
- (UIViewController *)childViewControllerForStatusBarStyle {
    return self.topViewController;
}
@end
