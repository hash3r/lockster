//
//  IKWallaperShowViewController.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 01.03.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@class IKWallaperShowViewController;

@protocol IKWallaperShowViewControllerDelegate
- (void)onDissmissViewController:(IKWallaperShowViewController *)vc;
@end

@interface IKWallaperShowViewController : UIViewController

@property (nonatomic, strong) NSString *previewImagePath;
@property (nonatomic, strong) UIImage *previewImage;

@property (nonatomic, strong) IBOutlet UIButton *settingButton;
@property (nonatomic, strong) IBOutlet UIButton *cancelButton;
@property (nonatomic, strong) IBOutlet UIImageView *imageView;
@property (nonatomic, strong) IBOutlet id<IKWallaperShowViewControllerDelegate> delegate;

- (IBAction)onSetBackgroundTapped:(id)sender;
- (IBAction)onCancelTapped:(id)sender;

@end
