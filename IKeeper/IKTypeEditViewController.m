//
//  IKTypeEditViewController.m
//  IKeeper
//
//  Created by Chebulaev Oleg on 15.01.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import "IKTypeEditViewController.h"
#import "IKBaseField.h"
#import "IKFieldType.h"
#import "IKCardField.h"
#import "IKCard.h"
#import "IKCardTemplate.h"
#import "IKTemplateField.h"
#import "IKFieldTypeListViewController.h"

#import "IKEditFieldFavoriteCell.h"
#import "IKEditFieldNameCell.h"
#import "IKEditFieldTypeCell.h"
#import <UIViewController+MMDrawerController.h>


@interface IKTypeEditViewController () <UITextFieldDelegate,
                                        IKFieldTypeListViewControllerDelegate, UIGestureRecognizerDelegate>
{
    NSUndoManager *_undoManager;
}

@end

@implementation IKTypeEditViewController

#pragma mark - Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    _undoManager = [[NSManagedObjectContext MR_defaultContext] undoManager];
    [_undoManager beginUndoGrouping];

    
    if (!_field && self.card) {
        _field = [self.card createField];
    } else if (!_field && self.cardTemplate) {
        _field = [self.cardTemplate createField];
    }
    
    if (self.card) {
        UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 100)];
        UILabel *helpLbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 290, 100)];
        helpLbl.numberOfLines = 0;
        helpLbl.lineBreakMode = NSLineBreakByWordWrapping;
        helpLbl.text = @"You may turn on up to 3 fields in each Card to make them visible in Cards List.";
        helpLbl.font = [UIFont systemFontOfSize:14];
        helpLbl.textColor = UIColorFromRGB(0x808080);
        [helpLbl sizeToFit];
        [v addSubview:helpLbl];
        
        self.tableView.tableFooterView = v;
    }
    
    UIImageView *imageView=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"back_icon"]];
    [imageView setTintColor:[UIColor colorWithRed:0.0 green:122.0/255.0 blue:1.0 alpha:1.0]];
    UILabel *label=[[UILabel alloc] init];
    label.textColor = [UIColor colorWithRed:0.0 green:122.0/255.0 blue:1.0 alpha:1.0];
    [label setText:@"Back"];
    [label sizeToFit];
    
    int space=6;
    label.frame=CGRectMake(imageView.frame.origin.x+imageView.frame.size.width+space, label.frame.origin.y, label.frame.size.width, label.frame.size.height);
    UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, label.frame.size.width+imageView.frame.size.width+space, imageView.frame.size.height)];
    
    view.bounds=CGRectMake(view.bounds.origin.x+8, view.bounds.origin.y-1, view.bounds.size.width, view.bounds.size.height);
    [view addSubview:imageView];
    [view addSubview:label];
    
    UIButton *button=[[UIButton alloc] initWithFrame:view.frame];
    [button addTarget:self action:@selector(onCancelTypeBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:button];
    
    [UIView animateWithDuration:0.33 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        label.alpha = 0.0;
        CGRect orig=label.frame;
        label.frame=CGRectMake(label.frame.origin.x+25, label.frame.origin.y, label.frame.size.width, label.frame.size.height);
        label.alpha = 1.0;
        label.frame=orig;
    } completion:nil];
    
    UIBarButtonItem *backButton =[[UIBarButtonItem alloc] initWithCustomView:view];
    self.navigationItem.leftBarButtonItem = backButton;
    
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(onSaveTypeBtnTapped:)];
    self.navigationItem.rightBarButtonItem = rightButton;
}

- (void)viewWillAppear:(BOOL)animated {
    [self.mm_drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModePanningNavigationBar];

    [self.tableView reloadData];
//    IKEditFieldNameCell *cell = (IKEditFieldNameCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
//    [cell.fieldNameTextField becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.mm_drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
}


#pragma mark - Event Handlers
- (void)onCancelTypeBtnTapped:(id)sender {
    IKEditFieldNameCell *cell = (IKEditFieldNameCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    [cell.fieldNameTextField resignFirstResponder];
    
    [_undoManager endUndoGrouping];
    [_undoManager undoNestedGroup];
    
    [self.delegate onCancelEditField:_field viewController:self];
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)onSaveTypeBtnTapped:(id)sender {
    IKEditFieldNameCell *cell = (IKEditFieldNameCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    [cell.fieldNameTextField resignFirstResponder];
    
    [_undoManager endUndoGrouping];
    
    [self.delegate onSaveEditField:_field viewController:self];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onChageShowInMainViewValue:(id)sender {
    UISwitch *s = (UISwitch *)sender;
    BOOL value = [s isOn];
    
    if (value) {
        IKCardField *f = (IKCardField *)_field;
        if ([self.card canAddFavoriteField]) {
            [f setShowInMainView:[NSNumber numberWithBool:value]];
        } else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"You have reached limit of 3 fields visible in Cards List. If you want to make this field visible in Cards List you should turn off one of the other visible fields in this Card" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
            [s setOn:!value animated:YES];
        }
    } else {
        [(IKCardField *)_field setShowInMainView:[NSNumber numberWithBool:value]];
    }
}

#pragma mark - UITableView DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.cardTemplate) {
        return 2;
    }
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        IKEditFieldNameCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EditNameCell"];
        cell.fieldNameTextField.text = _field.name;
        cell.fieldNameTextField.textColor = UIColorFromRGB(0x4e4e4e);
        return cell;
    } else if (indexPath.row == 1) {
        IKEditFieldTypeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EditTypeCell"];
        cell.fieldTypeLbl.text = _field.type.name;
        return cell;
    } else if (indexPath.row == 2) {
        IKEditFieldFavoriteCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EditFavoriteCell"];
        IKCardField *f = (IKCardField *)_field;
        BOOL inMainViewValue = [f.showInMainView boolValue];
        [cell.favoriteSwitch setOn:inMainViewValue animated:NO];
        return cell;
    }
    return nil;
}

#pragma mark - UITableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row == 1) {
        UIStoryboard *s = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
        IKFieldTypeListViewController *vc = [s instantiateViewControllerWithIdentifier:@"FieldTypeList"];
        vc.currentType = self.field.type;
        vc.delegate = self;
        [self.navigationController pushViewController:vc animated:YES];
    }
}


#pragma mark - TextField Delegate
- (void)textFieldDidEndEditing:(UITextField *)textField {
    NSString *text = [textField text];
    _field.name = text;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
//    [textField resignFirstResponder];
    return YES;
}


#pragma mark - IKFieldTypeListViewControllerDelegate
- (void)onTypeSelect:(IKFieldType *)type viewController:(UIViewController *)vc {
    _field.type = type;
    [self.navigationController popViewControllerAnimated:YES];
    [self.tableView beginUpdates];
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:1 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
    [self.tableView endUpdates];
}

@end
