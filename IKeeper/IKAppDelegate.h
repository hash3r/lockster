//
//  IKAppDelegate.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 22.12.13.
//  Copyright (c) 2013 Chebulaev Oleg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APViewController.h"

@class APPass;

@class CEReversibleAnimationController, CEBaseInteractionController;

@interface IKAppDelegate : UIResponder <UIApplicationDelegate,
                                        APViewControllerDelegate>
{
//    BOOL _showPasscodeScreen;
    BOOL _hiddenStB;
}

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, assign) UIStatusBarStyle lastStatusBarStyle;
@property (nonatomic, assign) CGFloat oldPercentVisible;
@property (nonatomic, assign) CGFloat newPercentVisible;

@end
