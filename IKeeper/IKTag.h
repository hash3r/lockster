//
//  IKTag.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 14.01.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class IKCardLink;

@interface IKTag : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *cardLinks;
@property (nonatomic, retain) NSString *uniqueIdentifier;
@property (nonatomic, retain) NSNumber *listOrder;

+ (IKTag *)tagByName:(NSString *)name;
+ (NSArray *)allTagTitles;
@end

@interface IKTag (CoreDataGeneratedAccessors)

- (void)addcardLinksObject:(IKCardLink *)value;
- (void)removecardLinksObject:(IKCardLink *)value;
- (void)addcardLinks:(NSSet *)values;
- (void)removecardLinks:(NSSet *)values;

@end
