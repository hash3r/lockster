//
//  IKMenuCell.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 17.02.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LKBadgeView;
@interface IKMenuCell : UITableViewCell {
    UIView *_selectedView;
}

@property (nonatomic, weak) IBOutlet UIImageView *iconImageView;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet LKBadgeView *countLabel;

@end
