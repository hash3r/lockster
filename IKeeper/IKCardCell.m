//
//  IKCardCell.m
//  IKeeper
//
//  Created by Chebulaev Oleg on 19.01.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import "IKCardCell.h"

@implementation IKCardCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
    }
    return self;
}

- (void)awakeFromNib {
    self.roundView.layer.cornerRadius = self.roundView.frame.size.width / 2;
    self.roundView.layer.borderWidth = 0.5;
    self.roundView.layer.borderColor = [UIColorFromRGB(0xdbdbdb) CGColor];
    self.roundView.backgroundColor = UIColorFromRGB(0xf5f5f5);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setIsFavorite:(BOOL)favorite {
    if (favorite) {
        self.roundView.layer.borderColor = [UIColorFromRGB(0xff4e00) CGColor];
    } else {
        self.roundView.layer.borderColor = [UIColorFromRGB(0xdbdbdb) CGColor];
    }
}

@end
