//
//  IKSyncSettingsViewController.m
//  IKeeper
//
//  Created by Chebulaev Oleg on 03.04.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import "IKSyncSettingsViewController.h"
#import "IKSyncCell.h"
#import "IKServiceCell.h"
#import <IDMSyncManager.h>
#import <MTDates/NSDate+MTDates.h>

#define kiCloudServiceSwitchTag 1
#define kDropBoxServiceSwitchTag 2

@interface IKSyncSettingsViewController () {
    id syncDidBeginNotif, syncDidEndNotif, userDefaultsUpdateNotif;
    BOOL merging;
}

@end

@implementation IKSyncSettingsViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        [self addSyncObservers];
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:syncDidBeginNotif];
    [[NSNotificationCenter defaultCenter] removeObserver:syncDidEndNotif];
    [[NSNotificationCenter defaultCenter] removeObserver:userDefaultsUpdateNotif];
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)addSyncObservers {
    __weak typeof(self) weakSelf = self;
    syncDidBeginNotif = [[NSNotificationCenter defaultCenter] addObserverForName:IDMSyncActivityDidBeginNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        __strong typeof(self) strongSelf = weakSelf;
        if (!strongSelf) return;
        strongSelf->merging = YES;
        [strongSelf updateUI];
    }];
    syncDidEndNotif = [[NSNotificationCenter defaultCenter] addObserverForName:IDMSyncActivityDidEndNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        __strong typeof(self) strongSelf = weakSelf;
        if (!strongSelf) return;
        strongSelf->merging = NO;
        [strongSelf updateUI];
    }];
    userDefaultsUpdateNotif = [[NSNotificationCenter defaultCenter] addObserverForName:NSUserDefaultsDidChangeNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        __strong typeof(self) strongSelf = weakSelf;
        if (!strongSelf) return;
        [strongSelf updateUI];
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    [[IDMSyncManager sharedSyncManager] synchronizeWithCompletion:^(NSError *error) {
        [self updateUI];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (NSInteger)tagForEnabledSyncService
{
    NSString *currentService = [[NSUserDefaults standardUserDefaults] valueForKey:IDMCloudServiceUserDefaultKey];
    return [currentService isEqualToString:IDMICloudService] ? kiCloudServiceSwitchTag : kDropBoxServiceSwitchTag;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    IDMSyncManager *syncManager = [IDMSyncManager sharedSyncManager];
    
    
    id resultCell = nil;
    
    if (indexPath.row == 0 || indexPath.row == 1) {
        IKServiceCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ServiceCell" forIndexPath:indexPath];
        
        cell.syncSwitch.tag = indexPath.row + 1;
        cell.syncServiceName.text = (indexPath.row == 0) ? @"iCloud" : @"Drop Box";
        
        BOOL value = [syncManager canSynchronize] && ([self tagForEnabledSyncService] == cell.syncSwitch.tag);
        [cell.syncSwitch setOn:value animated:NO];
        cell.syncSwitch.enabled = !merging;
        
        [cell.syncSwitch removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
        [cell.syncSwitch addTarget:self action:@selector(onChangeSync:) forControlEvents:UIControlEventTouchUpInside];
        
        if (value) {
            NSDate *syncDate = [[IDMSyncManager sharedSyncManager] lastSyncDate];
            NSString *value = [syncDate mt_stringValueWithDateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterMediumStyle];
            cell.syncLabel.text = value;
        } else {
            cell.syncLabel.text = @"No sync time";
        }
        resultCell = cell;
    }
    else if (indexPath.row == 2) {
        IKSyncCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SyncCell" forIndexPath:indexPath];
        
        cell.syncButton.enabled = syncManager.canSynchronize && !merging;
        [cell.syncButton removeTarget:self action:NULL forControlEvents:UIControlEventAllEvents];
        [cell.syncButton addTarget:self action:@selector(onSyncTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        resultCell = cell;
    }
    return resultCell;
}
         
- (void)onChangeSync:(id)sender {
    UISwitch *switchControl = (UISwitch *)sender;
    BOOL value  = switchControl.isOn;

    IDMSyncManager *syncManager = [IDMSyncManager sharedSyncManager];
    if (value) {
        NSString *service = (switchControl.tag == kDropBoxServiceSwitchTag) ? IDMDropboxService : IDMICloudService;
        [syncManager connectToSyncService:service withCompletion:^(NSError *error){
            [self updateUI];
        }];
    } else {
        [syncManager disconnectFromSyncServiceWithCompletion:^{
            [self updateUI];
        }];
    }
}

- (void)onSyncTapped:(id)sender {
    IDMSyncManager *syncManager = [IDMSyncManager sharedSyncManager];
    [syncManager synchronizeWithCompletion:^(NSError *error) {
        [self updateUI];
    }];
}

- (void)updateUI {
    [self.tableView beginUpdates];
    [self.tableView reloadData];
//    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0], [NSIndexPath indexPathForRow:1 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
//    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:1 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
    [self.tableView endUpdates];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0 || indexPath.row == 1) {
        return 70;
    } else {
        return 44.0;
    }
}

@end
