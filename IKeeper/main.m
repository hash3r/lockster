 //
//  main.m
//  IKeeper
//
//  Created by Chebulaev Oleg on 22.12.13.
//  Copyright (c) 2013 Chebulaev Oleg. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "IKAppDelegate.h"
#import "IKUIApplication.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, NSStringFromClass([IKUIApplication class]), NSStringFromClass([IKAppDelegate class]));
    }
}
