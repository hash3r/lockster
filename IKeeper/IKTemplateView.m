//
//  IKTemplateView.m
//  IKeeper
//
//  Created by Chebulaev Oleg on 09.04.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import "IKTemplateView.h"
#import "IKButton.h"
#import "IKFonts.h"
#import <UIImage+RTTint.h>

@implementation IKTemplateView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.9];
        
        self.roundView = [[UIView alloc] initWithFrame:CGRectMake(20, 20, 49, 49)];
        self.roundView.layer.cornerRadius = self.roundView.frame.size.width / 2.0;
        self.roundView.layer.borderWidth = 1.0;
        self.roundView.layer.borderColor = [UIColorFromRGB(0x999999) CGColor];
        [self addSubview:self.roundView];
        
        self.iconBtn = [IKButton buttonWithType:UIButtonTypeSystem];
        self.iconBtn.frame = CGRectMake(0, 0, 49, 49);
        self.iconBtn.titleLabel.font = [IKFonts helveticaNewWithSize:12.0];
        self.iconBtn.contentEdgeInsets = UIEdgeInsetsMake(65, 0, 0, 0);
        [self.roundView addSubview:self.iconBtn];
        
        self.descriptionField = [[UITextField alloc] initWithFrame:CGRectMake(104, 10, 206, 27)];
        self.descriptionField.clearButtonMode = UITextFieldViewModeWhileEditing;
        self.descriptionField.font = [IKFonts helveticaNewWithSize:18];
        self.descriptionField.placeholder = @"Description";
        self.descriptionField.autocapitalizationType = UITextAutocapitalizationTypeWords;
        [self addSubview:self.descriptionField];
        
        self.categoryBtn = [UIButton buttonWithType:UIButtonTypeSystem];
        self.categoryBtn.frame = CGRectMake(104, 56, 183, 21);
        self.categoryBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [self.categoryBtn setTitle:@"Template" forState:UIControlStateNormal];
        self.categoryBtn.titleLabel.font = [IKFonts helveticaNewWithSize:18];
        [self addSubview:self.categoryBtn];
        
        UIView *gradientView = [[UIView alloc] initWithFrame:CGRectMake(89, 44, 231, 0.5)];
        gradientView.backgroundColor = [UIColor lightGrayColor];
        [self addSubview:gradientView];
        
        UIButton *b = [UIButton buttonWithType:UIButtonTypeCustom];
        b.frame = CGRectMake(20, 20, 49, 49);
        [self addSubview:b];
        self.favoriteButton = b;
        
        UIImageView *checkMark = [[UIImageView alloc] initWithFrame:CGRectMake(296, 61, 8, 13)];
        checkMark.image = [UIImage imageNamed:@"indicator.png"];
        self.indicator = checkMark;
        [self addSubview:checkMark];
    }
    return self;
}

- (void)setFavoriteValue:(BOOL)favorite {
    if (favorite) {
        [self setUnsetFavButton];
    } else {
        [self setSetFavButton];
    }
}

- (void)setSetFavButton {
    UIButton *b = self.favoriteButton;
    [b removeTarget:self action:@selector(onUnSetFavorite:) forControlEvents:UIControlEventTouchUpInside];
    
    UIImage *image = [UIImage imageNamed:@"fav.png"];
    image = [image rt_tintedImageWithColor:UIColorFromRGB(0x999999)];
    [b setImage:image forState:UIControlStateNormal];
    [b addTarget:self action:@selector(onSetFavorite:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)setUnsetFavButton {
    UIButton *b = self.favoriteButton;
    [b removeTarget:self action:@selector(onSetFavorite:) forControlEvents:UIControlEventTouchUpInside];
    
    UIImage *image = [UIImage imageNamed:@"fav.png"];
    image = [image rt_tintedImageWithColor:UIColorFromRGB(0xff4e00)];
    [b setImage:image forState:UIControlStateNormal];
    [b addTarget:self action:@selector(onUnSetFavorite:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)onSetFavorite:(id)sender {
    [self setUnsetFavButton];
    [self.delegate onChangeFavorite:YES];
}

- (void)onUnSetFavorite:(id)sender {
    [self setSetFavButton];
    [self.delegate onChangeFavorite:NO];
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
    self.descriptionField.userInteractionEnabled = editing;
    self.categoryBtn.enabled = editing;
    self.indicator.hidden = !editing;
    
    if (editing) {
        self.roundView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.1];
        self.roundView.layer.borderColor = [UIColorFromRGB(0x999999) CGColor];
        self.favoriteButton.hidden = YES;
        [self.iconBtn setTitle:@"Edit" forState:UIControlStateNormal];
        self.iconBtn.userInteractionEnabled = editing;
    } else {
        self.roundView.backgroundColor = [UIColor whiteColor];
        self.roundView.layer.borderColor = [[UIColor clearColor] CGColor];
        self.favoriteButton.hidden = NO;
        [self.iconBtn setTitle:@"" forState:UIControlStateNormal];
        self.iconBtn.userInteractionEnabled = NO;
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
