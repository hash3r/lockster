//
//  IKTextView.m
//  IKeeper
//
//  Created by Chebulaev Oleg on 15.02.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import "IKTextView.h"

@implementation IKTextView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)scrollRangeToVisible:(NSRange)range {
    [super scrollRangeToVisible:range];
    if (self.layoutManager.extraLineFragmentTextContainer != nil && self.selectedRange.location == range.location) {
        CGRect caretRect = [self caretRectForPosition:self.selectedTextRange.end];
        [self scrollRectToVisible:caretRect animated:NO];
    }
    
}

@end
