//
//  IKButton.m
//  IKeeper
//
//  Created by Chebulaev Oleg on 16.02.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import "IKButton.h"

@implementation IKButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (CGRect)backgroundRectForBounds:(CGRect)bounds {
    UIImage *bkgImage = [self backgroundImageForState:self.state];
    if (bkgImage) {
        CGFloat x = floorf((self.bounds.size.width-bkgImage.size.width)/2);
        CGFloat y = floorf((self.bounds.size.height-bkgImage.size.height)/2);
        return CGRectMake(x, y, bkgImage.size.width, bkgImage.size.height);
    } else
        return [super backgroundRectForBounds:bounds];
}

@end
