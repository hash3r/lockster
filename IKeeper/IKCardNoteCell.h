//
//  IKCardNoteCell.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 23.01.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IKCardNoteCell : UITableViewCell {
    NSLayoutConstraint *_noteLblLeadingContraint;
    NSLayoutConstraint *_noteTextViewLeadingContraint;
}

@property (nonatomic, weak) IBOutlet UITextView *noteTextView;
@property (nonatomic, weak) IBOutlet UILabel *noteLbl;
@end
