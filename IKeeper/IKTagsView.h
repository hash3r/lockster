//
//  IKTagsView.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 04.04.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <IKTag.h>

@protocol IKTagsViewDelegate
- (void)onSelectTag:(IKTag *)tag;
- (void)onUnSelectTag:(IKTag *)tag;
- (void)onSelectAnyTags;
- (void)onSelectNoTags;
@end

@interface IKTagsView : UIView <UITableViewDataSource, UITableViewDelegate> {
    UITableView *_table;
    UITableView *_topTable;
    NSArray *_tags;
    NSArray *_counts;
    NSMutableSet *_selectedTags;
    NSInteger _mode;
}

@property (nonatomic, weak) id<IKTagsViewDelegate> delegate;

- (void)setMode:(NSInteger)mode tags:(NSArray *)tags counts:(NSArray *)counts selectedTags:(NSMutableSet *)selectedTags;

@end
