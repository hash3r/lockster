//
//  IKCardShowFieldCell.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 26.01.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IKCardShowFieldCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *fieldNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *fieldValueLabel;

@end
