//
//  IKMainViewController.m
//  IKeeper
//
//  Created by Chebulaev Oleg on 06.01.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import "IKMainViewController.h"
#import "IKCard.h"
#import "IKCardEditViewController.h"
#import <UIViewController+MMDrawerController.h>
#import "IKTemplatesListViewController.h"
#import "IKCardField.h"
#import "IKFieldValue.h"
#import "IKCardCell.h"
#import <UIImage+RTTint/UIImage+RTTint.h>
#import "IKCardEditViewController.h"
#import "TDMScreenEdgePanGestureRecognizer.h"
#import "IKSettings.h"

#import <AppPassword/AppPassword.h>
#import <SecureFoundation/SecureFoundation.h>
#import "IKSearchDisplayController.h"
#import "IKAppDelegate.h"
#import "IDMSyncManager.h"
#import "IKTagsView.h"
#import "IKTagCardLink.h"
#import <UIImage+BlurredFrame.h>
#import <UIImage+Additions.h>
#import "IKFieldType.h"

@interface IKMainViewController () <UISearchBarDelegate,
                                UISearchDisplayDelegate,
                                    UITableViewDelegate,
                                    UITableViewDataSource,
                                    IKTemplatesListViewControllerDelegate,
                                    IKCardEditViewControllerDelegate,
                                    NSFetchedResultsControllerDelegate,
                                    UIGestureRecognizerDelegate,
                                    UIActionSheetDelegate,
                                    IKTagsViewDelegate>
{
    NSFetchedResultsController *_fetchResultsContoller;
    UISearchDisplayController *_searchDisplayController;
    NSMutableArray *_searchResults;
    __weak IKTagsView *_tagsMenu;
    __weak UIView *_backView;
    UISearchBar *_searchBar;
    BOOL _isPermanentSearchBar;
    UIActionSheet *syncServiceActionSheet;
    
    
    NSMutableSet *_selectedTags;
    NSInteger _tagsMode;
    NSMutableArray *_uniqTags;
    UIScreenEdgePanGestureRecognizer *_screenRegognizer;
    UIPanGestureRecognizer *_panRecognizer;
    
    id _wallaperChangeNotif, _storeChangeNotif, _searhBarNotif;
}

@property (nonatomic, weak) IBOutlet UITableView *table;
@property (nonatomic, assign) BOOL inSearchMode;
@property (nonatomic, strong) UIActivityIndicatorView *storeLoadingActivity;

@end

@implementation IKMainViewController

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self addSettingsObservers];
    }
    return self;
}

- (void)dealloc {
    [self removeSettingsObservers];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self updateWallaper];
    
    
    
    _selectedTags = [NSMutableSet set];
    _uniqTags = [NSMutableArray array];
    _tagsMode = 0;
    
    _searchBar = [[UISearchBar alloc] init];
    _searchBar.translucent = YES;
    _searchBar.layer.borderWidth = 1;
    [_searchBar setSearchTextPositionAdjustment:UIOffsetMake(5, 0)];
//    _searchBar.barTintColor = [UIColor clearColor];
//    _searchBar.backgroundColor = [UIColor clearColor];
    
    UIImage *searchBack = [UIImage resizableImageWithColor:[UIColor colorWithWhite:1.0 alpha:0.85]];
    [_searchBar setBackgroundImage:searchBack];
    
    
    UIImage *fieldBack = [UIImage imageWithColor:[UIColor colorWithWhite:1.0 alpha:0.85] size:CGSizeMake(300, 30) cornerRadius:5];
    [_searchBar setSearchFieldBackgroundImage:fieldBack forState:UIControlStateNormal];
    _searchBar.barTintColor = UIColorFromRGB(0xd7d7d7);
//    _searchBar.backgroundColor = UIColorFromRGB(0xe7e7e7);
    _searchBar.layer.borderColor = [UIColorFromRGB(0xe7e7e7) CGColor];
    [_searchBar setFrameWidth:320];
    
    _searchDisplayController = [[IKSearchDisplayController alloc] initWithSearchBar:_searchBar contentsController:self];
    _searchDisplayController.searchBar.placeholder = @" ";
    _searchDisplayController.delegate = self;
    _searchDisplayController.searchResultsDataSource = self;
    _searchDisplayController.searchResultsDelegate = self;
    
    
    [self setPermanentSearch];
    
    self.table.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.searchDisplayController.searchResultsTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.table.scrollsToTop = YES;
    
    _screenRegognizer = [[UIScreenEdgePanGestureRecognizer alloc] initWithTarget:self action:@selector(handleScreenPan:)];
    _screenRegognizer.delegate = self;
    _screenRegognizer.edges = UIRectEdgeRight;
    [self.view addGestureRecognizer:_screenRegognizer];
    
    UIView *backView = [[UIView alloc] initWithFrame:self.mm_drawerController.view.bounds];
    backView.backgroundColor = [UIColor blackColor];
    backView.layer.opacity = 0.0;
    [self.mm_drawerController.view addSubview:backView];
    backView.hidden = YES;
    _backView = backView;
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap:)];
    [_backView addGestureRecognizer:tapRecognizer];
    
    IKTagsView *v = [[IKTagsView alloc] initWithFrame:CGRectMake(320, 20, 191, self.mm_drawerController.view.bounds.size.height - 25)];
    v.delegate = self;
    [self.mm_drawerController.view addSubview:v];
    _tagsMenu = v;
    
    
    _panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(onPan:)];
    _panRecognizer.delegate = self;
    [_backView addGestureRecognizer:_panRecognizer];
    _panRecognizer.enabled = NO;
    
    
    self.storeLoadingActivity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.storeLoadingActivity.center = CGPointMake(160, 160);
    self.storeLoadingActivity.hidesWhenStopped = YES;
    [self.view addSubview:self.storeLoadingActivity];
    
    [self.mm_drawerController addObserver:self forKeyPath:@"openSide" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld) context:NULL];
    [self.mm_drawerController addObserver:self forKeyPath:@"isDragged" options:(NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld) context:NULL];
}

- (void)setStatusBarHidden:(BOOL)statusBarHidden {
    _statusBarHidden = statusBarHidden;
    if (_statusBarHidden) {
        CGRect fr = self.navigationController.navigationBar.frame;
        fr.size.height = 64;
        fr.origin.y = 0;
        self.navigationController.navigationBar.frame = fr;
    } else {
        CGRect fr = self.navigationController.navigationBar.frame;
        fr.origin.y = 20;
        fr.size.height = 44;
        self.navigationController.navigationBar.frame = fr;
    }
    [self.navigationController.navigationBar setNeedsLayout];
}


- (void)didReceiveMemoryWarning {
//    [self.mm_drawerController removeObserver:self forKeyPath:@"openSide"];
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    if ([keyPath isEqual:@"isDragged"]) {
        NSNumber *value = [change objectForKey:NSKeyValueChangeNewKey];
        BOOL isDragged = [value integerValue];
        if (isDragged && [self.searchDisplayController isActive]) {
            [self.searchDisplayController.searchBar resignFirstResponder];
        }
        
    } else if ([keyPath isEqual:@"openSide"]) {
        NSNumber *value = [change objectForKey:NSKeyValueChangeNewKey];
        MMDrawerSide newSide = [value integerValue];
        if (newSide == MMDrawerSideLeft) {
            if (_screenRegognizer.enabled) {
                _screenRegognizer.enabled = NO;
            }
        } else {
            if (!_screenRegognizer.enabled) {
                _screenRegognizer.enabled = YES;
            }
        }
    } else {
        [super observeValueForKeyPath:keyPath
                             ofObject:object
                               change:change
                              context:context];
    }
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)recognizer {
    if (recognizer == _panRecognizer) {
        UIPanGestureRecognizer *panRecognizer = (UIPanGestureRecognizer *)recognizer;
        CGPoint velocity = [panRecognizer velocityInView:self.mm_drawerController.view];
        return ABS(velocity.x) > ABS(velocity.y); // Horizontal panning
    }
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldBeRequiredToFailByGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    if (gestureRecognizer == _screenRegognizer || gestureRecognizer == _panRecognizer) {
        return YES;
    } else {
        return NO;
    }
}


- (void)onPan:(UIPanGestureRecognizer *)recognizer {
    CGPoint translation = [recognizer translationInView:_tagsMenu];
    
    _tagsMenu.center = CGPointMake(MAX(_tagsMenu.center.x+translation.x, (124.0 + _tagsMenu.frame.size.width/2.0)), _tagsMenu.center.y);
    
    [recognizer setTranslation:CGPointMake(0, 0) inView:_tagsMenu];
    
    CGFloat progress =  (320 - _tagsMenu.frame.origin.x) / (320 - 124);
    progress = MIN(1.0, MAX(0.0, progress));
    
    
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        _backView.hidden = NO;
    }
    else if (recognizer.state == UIGestureRecognizerStateChanged) {
        _backView.layer.opacity = progress * 0.3;
    }
    else if (recognizer.state == UIGestureRecognizerStateEnded || recognizer.state == UIGestureRecognizerStateCancelled) {
        // Finish or cancel the interactive transition
        CGRect newTagsFrame = _tagsMenu.frame;
        if (progress > 0.4) {
            newTagsFrame.origin.x = 124;
            [UIView animateWithDuration:0.2 animations:^{
                _tagsMenu.frame = newTagsFrame;
                _backView.layer.opacity = 0.3;
            } completion:^(BOOL finished) {
                self.mm_drawerController.openDrawerGestureModeMask = MMOpenDrawerGestureModeNone;
            }];
            
        } else {
            _panRecognizer.enabled = NO;
            newTagsFrame.origin.x = 320;
            [UIView animateWithDuration:0.2 animations:^{
                _tagsMenu.frame = newTagsFrame;
                _backView.layer.opacity = 0.0;
            } completion:^(BOOL finished) {
                _backView.hidden = YES;
                self.mm_drawerController.openDrawerGestureModeMask = MMOpenDrawerGestureModeAll;
            }];
        }
    }
}

- (void)setPermanentSearch {
    BOOL permanent = [[IKSettings sharedInstance] permanentSearchBar];
    _isPermanentSearchBar = permanent;
    
    [_searchDisplayController setActive:NO];
    
    _searchDisplayController = nil;
    [_searchBar removeFromSuperview];
    self.table.tableHeaderView = nil;
    
    _searchDisplayController = [[IKSearchDisplayController alloc] initWithSearchBar:_searchBar contentsController:self];
    _searchDisplayController.delegate = self;
    _searchDisplayController.searchResultsDataSource = self;
    _searchDisplayController.searchResultsDelegate = self;
    
    if (_isPermanentSearchBar) {
        [self.view addSubview:_searchBar];
        [_searchBar setFrameOriginY:63.5];
//        CGPoint offset = self.table.contentOffset;
        self.table.contentInset = UIEdgeInsetsMake(106.5, 0, 0, 0);
        [self.table setContentOffset:CGPointMake(0, -106.5) animated:NO];
    } else {
        self.table.tableHeaderView = _searchBar;
        self.table.contentInset = UIEdgeInsetsMake(63.5, 0, 0, 0);
        self.searchDisplayController.searchResultsTableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    }

    _searchBar.delegate = self;
}

- (void)addSettingsObservers {
    _searhBarNotif = [[NSNotificationCenter defaultCenter] addObserverForName:kNotificationPermanentSeaarchBarChanged object:nil queue:nil usingBlock:^(NSNotification *note) {
        [self onPermanentChange:note];
    }];
    
    _wallaperChangeNotif = [[NSNotificationCenter defaultCenter] addObserverForName:kNotificationWallaperChanged object:nil queue:nil usingBlock:^(NSNotification *note) {
        [self onWallaperChange:note];
    }];
    
    _storeChangeNotif = [[NSNotificationCenter defaultCenter] addObserverForName:kNotificationPersistentStoreChanged object:nil queue:nil usingBlock:^(NSNotification *note) {
        [self onChangeStore:note];
    }];
}

- (void)onWallaperChange:(NSNotification *)note {
    [self updateWallaper];
}

- (void)onChangeStore:(NSNotification *)note {
    _selectedTags = [NSMutableSet set];
    [self updateDataSource];
    [self.table reloadData];
    [self updateTitle];
    
    [self updateTagsSource];
    [self updateTagsFrame];
}

- (void)updateWallaper {
    NSString *wallaperPath = [[IKSettings sharedInstance] wallaperPath];
    UIImage *image = [UIImage imageWithContentsOfFile:wallaperPath];
    UIColor *tintColor = [UIColor colorWithWhite:1.0 alpha:0.5];
    image = [image applyBlurWithRadius:10.0 tintColor:tintColor saturationDeltaFactor:1.8 maskImage:nil atFrame:CGRectMake(0, 0, image.size.width, image.size.height)];
    self.backgroundImageView.image = image;
}

- (void)removeSettingsObservers {
    [[NSNotificationCenter defaultCenter] removeObserver:_searhBarNotif];
    [[NSNotificationCenter defaultCenter] removeObserver:_wallaperChangeNotif];
    [[NSNotificationCenter defaultCenter] removeObserver:_storeChangeNotif];
}

- (void)onPermanentChange:(NSNotification *)n {
    [self setPermanentSearch];
}

- (void) searchDisplayControllerWillBeginSearch:(UISearchDisplayController *)controller {
    CGFloat duration = 0.19;
    if (_isPermanentSearchBar) {
        [UIView animateWithDuration:duration delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
            [_searchBar setFrameOriginY:20];
            self.table.contentInset = UIEdgeInsetsMake(64, 0, 0, 0);
        } completion:nil];
    } else {
        [UIView animateWithDuration:duration delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
//            [_searchBar setFrameOriginY:0];
            self.table.contentInset = UIEdgeInsetsMake(20, 0, 0, 0);
        } completion:nil];
        
    }
}
- (void) searchDisplayControllerWillEndSearch:(UISearchDisplayController *)controller {
    CGFloat duration = 0.19;
    if (_isPermanentSearchBar) {
        [UIView animateWithDuration:duration delay:0.00 options:UIViewAnimationOptionCurveEaseIn animations:^{
            [_searchBar setFrameOriginY:63.5];
            self.table.contentInset = UIEdgeInsetsMake(106.5, 0, 0, 0);
            self.table.contentOffset = CGPointMake(0, -106.5);
        } completion:nil];
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [UIView animateWithDuration:duration delay:0.00 options:UIViewAnimationOptionCurveEaseIn animations:^{
                [_searchBar setFrameOriginY:0];
                self.table.contentInset = UIEdgeInsetsMake(64, 0, 0, 0);
                self.table.contentOffset = CGPointMake(0, -64);
            } completion:nil];
        });
    }
}

- (void)handleScreenPan:(UIScreenEdgePanGestureRecognizer*)recognizer
{
    CGPoint t = [recognizer locationInView:recognizer.view];
    
    CGRect newFrame = _tagsMenu.frame;
    CGFloat x = t.x;
    if (x >= 124.0) {
        newFrame.origin.x = x;
    } else {
        newFrame.origin.x = 124.0;
    }
    _tagsMenu.frame = newFrame;
    
    CGFloat progress =  (320 - _tagsMenu.frame.origin.x) / (320 - 124);
    
    
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        _backView.hidden = NO;
        CGFloat maybeY = t.y - newFrame.size.height / 2;
        maybeY = MAX(maybeY, 20.0);
        maybeY = MIN(maybeY, self.view.frame.size.height - newFrame.size.height - 5);
        newFrame.origin.y = maybeY;
        _tagsMenu.frame = newFrame;
    } else if (recognizer.state == UIGestureRecognizerStateChanged) {
        _backView.layer.opacity = progress * 0.3;
    } else if (recognizer.state == UIGestureRecognizerStateEnded || recognizer.state == UIGestureRecognizerStateCancelled) {
        _panRecognizer.enabled = YES;
        CGRect newTagsFrame = _tagsMenu.frame;
        if (progress > 0.4) {
            newTagsFrame.origin.x = 124;
            [UIView animateWithDuration:0.2 animations:^{
                _tagsMenu.frame = newTagsFrame;
                _backView.layer.opacity = 0.3;
            } completion:^(BOOL finished) {
                self.mm_drawerController.openDrawerGestureModeMask = MMOpenDrawerGestureModeNone;
            }];

        } else {
            newTagsFrame.origin.x = 320;
            [UIView animateWithDuration:0.2 animations:^{
                _tagsMenu.frame = newTagsFrame;
                _backView.layer.opacity = 0.0;
            } completion:^(BOOL finished) {
                _backView.hidden = YES;
                self.mm_drawerController.openDrawerGestureModeMask = MMOpenDrawerGestureModeAll;
            }];
        }
    }
}

- (void)onSelectTag:(IKTag *)tag {
    _tagsMode = 2;
    [_selectedTags addObject:tag];
    [self updateDataSourceWithSelectedTags];
    [_table reloadData];
}

- (void)onSelectAnyTags {
    _tagsMode = 0;
    [_selectedTags removeAllObjects];
    [self updateDataSourceWithSelectedTags];
    [_table reloadData];
}

- (void)onSelectNoTags {
    _tagsMode = 1;
    [_selectedTags removeAllObjects];
    [self updateDataSourceWithSelectedTags];
    [_table reloadData];
}

- (void)onUnSelectTag:(IKTag *)tag {
    _tagsMode = 2;
    [_selectedTags removeObject:tag];
    [self updateDataSourceWithSelectedTags];
    [_table reloadData];
}

- (void)updateDataSourceWithSelectedTags {
    NSPredicate *tagsCommonPredicate = nil;
    
    if (_tagsMode == 0) {
        tagsCommonPredicate = nil;
    } else if (_tagsMode == 1) {
        tagsCommonPredicate = [NSPredicate predicateWithFormat:@"tagLinks.@count == 0"];
    } else if (_tagsMode == 2) {
        NSMutableArray *tagPredicates = [NSMutableArray array];
        
        for (IKTag *t in _selectedTags) {
            NSPredicate *p = [NSPredicate predicateWithFormat:@"ANY tagLinks.tag == %@", t];
            [tagPredicates addObject:p];
        }
        if ([tagPredicates count] > 0) {
            tagsCommonPredicate = [NSCompoundPredicate orPredicateWithSubpredicates:tagPredicates];
        }
    }
    
    if (_mode == CardsFilterModeAll) {
        _fetchResultsContoller = [IKCard MR_fetchAllGroupedBy:nil withPredicate:tagsCommonPredicate sortedBy:@"name" ascending:YES delegate:self inContext:[NSManagedObjectContext MR_defaultContext]];
    } else if (_mode == CardsFilterModeCustom) {
        NSPredicate *p = [NSPredicate predicateWithFormat:@"self.template == %@", self.cardTemplate];
        if (tagsCommonPredicate) {
            p = [NSCompoundPredicate andPredicateWithSubpredicates:@[p, tagsCommonPredicate]];
        }
        _fetchResultsContoller = [IKCard MR_fetchAllGroupedBy:nil withPredicate:p sortedBy:@"name" ascending:YES delegate:self inContext:[NSManagedObjectContext MR_defaultContext]];
    }
}

- (void)onTap:(UITapGestureRecognizer *)recognizer {
    _panRecognizer.enabled = NO;
    CGRect newTagsFrame = _tagsMenu.frame;
    newTagsFrame.origin.x = 320;
    [UIView animateWithDuration:0.2 animations:^{
        _tagsMenu.frame = newTagsFrame;
        _backView.layer.opacity = 0.0;
    } completion:^(BOOL finished) {
        _backView.hidden = YES;
        self.mm_drawerController.openDrawerGestureModeMask = MMOpenDrawerGestureModeAll;
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    _selectedTags = [NSMutableSet set];
    [self updateDataSource];
    [self.table reloadData];
    [self updateTitle];
    
    [self updateTagsSource];
    [self updateTagsFrame];
    
    [self.searchDisplayController.searchResultsTableView reloadData];
    [[IDMSyncManager sharedSyncManager] synchronizeWithCompletion:^(NSError *error) {
        NSLog(@"SYNC");
        if (error) {
            NSLog(@"%@", [error localizedDescription]);
        }
        NSInteger count = [IKCard MR_countOfEntities];
        
        NSLog(@"COUNT %d", count);
    }];
}

- (void)updateDataSource {
    if (_mode == CardsFilterModeAll) {
        _fetchResultsContoller = [IKCard MR_fetchAllGroupedBy:nil withPredicate:nil sortedBy:@"name" ascending:YES delegate:self inContext:[NSManagedObjectContext MR_defaultContext]];
    } else if (_mode == CardsFilterModeCustom) {
        NSPredicate *p = [NSPredicate predicateWithFormat:@"self.template == %@", self.cardTemplate];
        _fetchResultsContoller = [IKCard MR_fetchAllGroupedBy:nil withPredicate:p sortedBy:@"name" ascending:YES delegate:self inContext:[NSManagedObjectContext MR_defaultContext]];
    } else if (_mode == CardsFilterModeFavorites) {
        NSPredicate *p = [NSPredicate predicateWithFormat:@"self.isFavorite == %@", [NSNumber numberWithBool:YES]];
        _fetchResultsContoller = [IKCard MR_fetchAllGroupedBy:nil withPredicate:p sortedBy:@"name" ascending:YES delegate:self inContext:[NSManagedObjectContext MR_defaultContext]];
    }
}

- (void)updateTagsSource {
    NSMutableArray *tags = [NSMutableArray array];
    NSArray *cards = [_fetchResultsContoller fetchedObjects];
    for (IKCard *card in cards) {
        for (IKTagCardLink *l in card.tagLinks) {
            [tags addObject:l.tag];
        }
    }
    NSArray *uniqueTags = [tags valueForKeyPath:@"@distinctUnionOfObjects.self"];
    NSMutableArray *counts = [NSMutableArray array];
    for (IKTag *t in uniqueTags) {
        if (_mode == CardsFilterModeAll) {
            NSPredicate *p = [NSPredicate predicateWithFormat:@"ANY tagLinks.tag == %@", t];
            NSInteger count = [IKCard MR_countOfEntitiesWithPredicate:p];
            [counts addObject:@(count)];
        } else {
            NSPredicate *p = [NSPredicate predicateWithFormat:@"ANY tagLinks.tag == %@", t];
            NSPredicate *p1 = [NSPredicate predicateWithFormat:@"self.template == %@", self.cardTemplate];
            NSPredicate *p2 = [NSCompoundPredicate andPredicateWithSubpredicates:@[p ,p1]];
            NSInteger count = [IKCard MR_countOfEntitiesWithPredicate:p2];
            [counts addObject:@(count)];
        }
    }
    
    _uniqTags = [uniqueTags mutableCopy];
    NSSortDescriptor *s = [[NSSortDescriptor alloc] initWithKey:@"listOrder" ascending:YES];
    _uniqTags = [[uniqueTags sortedArrayUsingDescriptors:@[s]] mutableCopy];
    [_tagsMenu setMode:_tagsMode tags:_uniqTags counts:counts selectedTags:_selectedTags];
}

- (void)updateTagsFrame {
    CGFloat screenHeight = [[UIScreen mainScreen] bounds].size.height;
    
    CGFloat possibleHeight = (_uniqTags.count + 2.0) * 45.0;
    CGFloat maxHeight = screenHeight - 20.0 - 5.0;
    CGFloat actualHeight = MIN(maxHeight, possibleHeight);
    [_tagsMenu setFrameHeight:actualHeight];
    
    CGFloat centerY = 20 + (screenHeight - 20.0 - 5.0) / 2.0;
    _tagsMenu.center = CGPointMake(_tagsMenu.center.x, centerY);
    
    
}

- (void)updateTitle {
    if (_mode == CardsFilterModeAll) {
        [self setTitle:@"All Cards"];
    } else if (_mode == CardsFilterModeFavorites) {
        [self setTitle:@"Favorites"];
    } else if (_mode == CardsFilterModeCustom) {
        NSString *templateName = [_cardTemplate name];
        [self setTitle:templateName];
    }
}

- (void)setMode:(CardsFilterMode)mode {
    _mode = mode;
    
    _tagsMode = 0;
    _selectedTags = [NSMutableSet set];
    [self updateDataSource];
    [self.table reloadData];
    [self updateTitle];
     
    [self updateTagsSource];
    [self updateTagsFrame];
}


- (IBAction)onMenuBarBtnTapped:(id)sender {
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

- (IBAction)onAddBarBtnTapped:(id)sender {
    UIStoryboard *s = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    
    
    if (_mode == CardsFilterModeCustom) {
        IKCardEditViewController *editVc = [s instantiateViewControllerWithIdentifier:@"CardEdit"];
        editVc.delegate = self;
        editVc.cardTemplate = _cardTemplate;
        
        UINavigationController *navVc = [[UINavigationController alloc] initWithRootViewController:editVc];
        [self presentViewController:navVc animated:YES completion:nil];
    } else {
        UINavigationController *navVc = [s instantiateViewControllerWithIdentifier:@"TemplateListNav"];
        
        IKTemplatesListViewController *vc = (IKTemplatesListViewController *)[navVc topViewController];
        vc.delegate = self;
        [self presentViewController:navVc animated:YES completion:nil];
    }
}

#pragma mark - IKTemplatesListViewControllerDelegate
- (void)onCancelSelectTemplateInViewController:(IKTemplatesListViewController *)vc {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)onSelectTemplate:(IKCardTemplate *)cardTemplate inViewController:(IKTemplatesListViewController *)vc {
    
    UIStoryboard *s = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    
    IKCardEditViewController *editVc = [s instantiateViewControllerWithIdentifier:@"CardEdit"];
    editVc.delegate = self;
    editVc.cardTemplate = cardTemplate;
    
    if (_mode == CardsFilterModeFavorites) {
        editVc.isNewFav = YES;
    }
    
    
    UINavigationController *navVc = [[UINavigationController alloc] initWithRootViewController:editVc];
    [self dismissViewControllerAnimated:YES completion:^{
        [self presentViewController:navVc animated:YES completion:nil];
    }];
}

- (void)onSaveCard:(IKCard *)card viewContoller:(UIViewController *)vc {
    [self dismissViewControllerAnimated:YES completion:nil];
    [[IDMSyncManager sharedSyncManager] synchronizeWithCompletion:NULL];
}

- (void)onCancelCard:(IKCard *)card viewContoller:(UIViewController *)vc {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return 1;
    } else {
        return [[_fetchResultsContoller sections] count];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return  [_searchResults count];
    } else {
        if ([[_fetchResultsContoller sections] count] > 0) {
            id <NSFetchedResultsSectionInfo> sectionInfo = [[_fetchResultsContoller sections] objectAtIndex:section];
            NSInteger numberOfObjects = [sectionInfo numberOfObjects];
            return numberOfObjects;
        } else {
            return 0;
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [self.table dequeueReusableCellWithIdentifier:CellIdentifier];
    
    [self configureCell:cell atIndexPath:indexPath tableView:tableView];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView {
    IKCard *card;
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        card = [_searchResults objectAtIndex:indexPath.row];
    } else {
        card = [_fetchResultsContoller objectAtIndexPath:indexPath];
    }
    
    IKCardField *descriptionField = [card descriptionField];
    IKFieldValue *value = descriptionField.value;
    
    IKCardCell *cusomCell = (IKCardCell *)cell;
    cusomCell.descriptionLabel.text = [value value] ? [value value] : @"";
    
    UIImage *image = [card icon];
    UIColor *tintColor = card.iconColor;
    if (!tintColor) {
        tintColor = [UIColor blackColor];
    }
    image = [image rt_tintedImageWithColor:tintColor];
    
    cusomCell.iconImageView.image = image;
    
    [cusomCell setIsFavorite:[[card isFavorite] boolValue]];
    
    UIView *container = [cusomCell fieldsContainerView];
    [[container subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    NSArray *customFields = [card favoriteFields];
    NSInteger actualCountCustomFields = [customFields count];
    int bounedCount = MIN(actualCountCustomFields, 3);
    
    if (indexPath.row == [[_fetchResultsContoller fetchedObjects] count] - 1) {
        cell.separatorInset = UIEdgeInsetsMake(0, 0, 0, cell.bounds.size.width);
    } else {
        cell.separatorInset = UIEdgeInsetsMake(0, 70, 0, 0);
    }
    
    if (bounedCount == 0) {
        cusomCell.topSpace.constant = 24.5;
    } else {
        cusomCell.topSpace.constant = 10;
    }
    
    for (int i = 0; i < bounedCount; i++) {
        IKCardField *f = customFields[i];
        
        NSString *name = f.name ? f.name : @"";
        NSString *value = f.value.value ? f.value.value : @"";
        
        if (name && ![name isEqualToString:@""] && value && ![value isEqualToString:@""]) {
            NSString *formattedString = [NSString stringWithFormat:@"%@: %@", name, value];
            
            UILabel *fieldLbl = [[UILabel alloc] initWithFrame:CGRectMake(0, i*17, 217, 17)];
            fieldLbl.backgroundColor = [UIColor clearColor];
            
            NSMutableAttributedString *attrubutedString = [[NSMutableAttributedString alloc] initWithString:formattedString];
            
            UIFont *valueFont = [UIFont boldSystemFontOfSize:13.0];
            if ([f.type isPasswordType]) {
                valueFont = [IKFonts cousineRegularWithSize:13.0];
            }
            
            UIColor *valueFontColor = [UIColor blackColor];
            if ([f.type isPasswordType]) {
                valueFontColor = UIColorFromRGB(0x336699);
            }
            
            NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:valueFont, NSFontAttributeName, valueFontColor, NSForegroundColorAttributeName, nil];
            
            NSRange valueRandge = [formattedString rangeOfString:value];
            NSRange nameRandge = [formattedString rangeOfString:[NSString stringWithFormat:@"%@:", name]];
            
            [attrubutedString setAttributes:attributes range:valueRandge];
            [attrubutedString setAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:13], NSForegroundColorAttributeName: UIColorFromRGB(0x4e4e4e)} range:nameRandge];
            
            
            
            fieldLbl.attributedText = attrubutedString;
            
            [container addSubview:fieldLbl];
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    IKCard *card;
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        card =  _searchResults[indexPath.row];
    } else {
        card = [_fetchResultsContoller objectAtIndexPath:indexPath];
    }
    NSArray *customFields = [card favoriteFields];
    NSInteger actualCountCustomFields = [customFields count];
    int bounedCount = MIN(actualCountCustomFields, 3);
    int size = bounedCount * 17 + 33 + 10;
    int actualSize = MAX(70, size);
    return actualSize;
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    CGRect barFrame = _searchBar.frame;
    barFrame.size.width = self.view.bounds.size.width;
    _searchBar.frame = barFrame;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    IKCard *card = nil;
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        card = [_searchResults objectAtIndex:indexPath.row];
    } else {
        card = [_fetchResultsContoller objectAtIndexPath:indexPath];
    }
    
    card = [card MR_inContext:[NSManagedObjectContext MR_defaultContext]];
    
    UIStoryboard *s = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    IKCardEditViewController *vc = [s instantiateViewControllerWithIdentifier:@"CardEdit"];
    vc.card = card;
    
    
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Search
-(BOOL)searchDisplayController:(UISearchDisplayController *)controller
shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return YES;
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
//    NSPredicate *searchPredicate = [NSPredicate predicateWithFormat:@"name contains[cd] %@", searchText];
    
    BOOL titleSearchOnly = [[IKSettings sharedInstance] titleSearchOnly];
    
    NSArray *cards = [_fetchResultsContoller fetchedObjects];
    NSPredicate *searchPredicate = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
        IKCard *card = (IKCard *)evaluatedObject;
    
        if (titleSearchOnly) {
            IKCardField *descriptionField = [card descriptionField];
            IKFieldValue *value = [descriptionField value];
            NSString *cardName = [value value];
            if (cardName && [cardName rangeOfString:searchText options:NSCaseInsensitiveSearch].location != NSNotFound) {
                return YES;
            } else {
                return NO;
            }
        } else {
            for (IKCardField *f in card.fields) {
                if ([self field:f containText:searchText]) {
                    return YES;
                }
                
            }
            return NO;
        }
    }];
    _searchResults = [[cards filteredArrayUsingPredicate:searchPredicate] mutableCopy];
}

- (BOOL)field:(IKCardField *)cardField containText:(NSString *)text {
    NSString *fieldName = [cardField name];
    if (fieldName && [fieldName rangeOfString:text options:NSCaseInsensitiveSearch].location != NSNotFound) {
        return YES;
    }
    
    IKFieldValue *v = [cardField value];
    NSString *textValue  = [v value];
    
    if (textValue && [textValue rangeOfString:text options:NSCaseInsensitiveSearch].location != NSNotFound) {
        return YES;
    }
    
    return NO;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar {
    [self.table reloadData];
}

#pragma mark - NSFetchController Delegate
#pragma mark - NSFetchedResultsControllerDelegate
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.table beginUpdates];
}


- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.table insertSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.table deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    UITableView *tableView = self.table;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                                 withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath]
                    atIndexPath:indexPath tableView:tableView];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.table endUpdates];
}

@end
