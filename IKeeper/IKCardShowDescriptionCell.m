//
//  IKCardShowDescriptionCell.m
//  IKeeper
//
//  Created by Chebulaev Oleg on 26.01.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import "IKCardShowDescriptionCell.h"

@implementation IKCardShowDescriptionCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
    }
    return self;
}

- (void)awakeFromNib {
    self.roundView.layer.cornerRadius = self.roundView.frame.size.width / 2.0;
    self.roundView.layer.borderWidth = 1.0;
    self.roundView.layer.borderColor = [UIColorFromRGB(0x999999) CGColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
