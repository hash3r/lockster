//
//  IKCard.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 06.01.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <UIColorRGBValueTransformer.h>

@class IKCardField, IKCardTemplate, IKTagCardLink;

@interface IKCard : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * iconName;
@property (nonatomic, retain) NSSet *fields;
@property (nonatomic, retain) NSSet *tagLinks;
@property (nonatomic, retain) IKCardTemplate *template;
@property (nonatomic, retain) NSString *uniqueIdentifier;
@property (nonatomic, retain) NSNumber *isFavorite;
@property (nonatomic, retain) id iconColor;

- (BOOL)isValidModel:(NSError **)error;
- (NSArray *)favoriteFields;                    
- (NSArray *)orderedFilledCustomFileds;

- (UIImage *)icon;

- (NSArray *)customFields;
- (BOOL)hasFilledCustomFields;
- (NSArray *)orderedCustomFields;
- (NSInteger)maxListOrder;
- (IKCardField *)descriptionField;
- (IKCardField *)noteField;
- (NSArray *)tagsNames;
- (NSArray *)tags;
- (NSInteger)tagsMaxListOrder;

- (IKTagCardLink *)addTagWithName:(NSString *)name;
- (void)removeTagWithName:(NSString *)name;

- (BOOL)canAddFavoriteField;
- (IKCardField *)createField;
- (IKCardField *)createPasswordField;
- (void)replaceFieldsFromTemplate:(IKCardTemplate *)cardTemplate;
- (void)updateNameField;
@end

@interface IKCard (CoreDataGeneratedAccessors)

- (void)addFieldsObject:(IKCardField *)value;
- (void)removeFieldsObject:(IKCardField *)value;
- (void)addFields:(NSSet *)values;
- (void)removeFields:(NSSet *)values;

- (void)addTagLinksObject:(IKTagCardLink *)value;
- (void)removeTagLinksObject:(IKTagCardLink *)value;
- (void)addTagLinks:(NSSet *)values;
- (void)removeTagLinks:(NSSet *)values;

@end
