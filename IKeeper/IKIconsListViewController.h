//
//  IKIconsListViewController.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 13.01.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@class IKIconsListViewController;

@protocol IKIconsListViewControllerDelegate
- (void)onSelectIcon:(UIImage *)iconImage iconName:(NSString *)iconName color:(UIColor *)color  inViewcontroller:(UIViewController *)vc;
@end

@interface IKIconsListViewController : UICollectionViewController

@property (nonatomic, weak) id<IKIconsListViewControllerDelegate>delegate;

@end
