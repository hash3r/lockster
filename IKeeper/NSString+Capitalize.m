//
//  NSString+Capitalize.m
//  TrainingDiary
//
//  Created by Chebulaev Oleg on 27.08.13.
//  Copyright (c) 2013 Alex Edunov. All rights reserved.
//

#import "NSString+Capitalize.h"

@implementation NSString(Capitalize)

- (NSString *)capitalizeFirstLettterString {
    if ([self length] == 0) {
        return @"";
    }
    NSString *firstLetter = [self  substringToIndex:1];
    NSString *capitalizeFirstLetter = [firstLetter capitalizedString];
    NSString *capitalizeFirstLettterString = [self stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:capitalizeFirstLetter];
    return capitalizeFirstLettterString;
}

@end
