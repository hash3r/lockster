//
//  IKTemplateFieldCell.m
//  IKeeper
//
//  Created by Chebulaev Oleg on 01.02.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import "IKTemplateFieldCell.h"

@implementation IKTemplateFieldCell

- (void)awakeFromNib {
    if (!self.backgroundView) {
        self.backgroundView = [UIView new];
    }
    
    if (![self.backgroundView viewWithTag:200]) {
        UIView *v = [[UIView alloc] initWithFrame:CGRectMake(44, 43.5, 320-44, 0.5)];
        v.tag = 200;
        v.backgroundColor = [UIColor lightGrayColor];
        [self.backgroundView addSubview:v];
    }
}

@end
