//
//  IKBaseField.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 06.01.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class IKFieldType;

@interface IKBaseField : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * listOrder;
@property (nonatomic, retain) IKFieldType *type;
@property (nonatomic, retain) NSString *uniqueIdentifier;

- (BOOL)isDescriptionField;
- (BOOL)isNoteField;

-(BOOL)isValidModel:(NSError **)error;

@end
