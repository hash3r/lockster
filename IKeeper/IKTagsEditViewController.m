//
//  IKTagsEditViewController.m
//  IKeeper
//
//  Created by Chebulaev Oleg on 06.05.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import "IKTagsEditViewController.h"
#import "IKTagsEditCell.h"
#import "IKTag.h"

@interface IKTagsEditViewController () <UITextFieldDelegate, NSFetchedResultsControllerDelegate> {
    NSFetchedResultsController *_fetchController;
    NSIndexPath *_removedIndexPath;
    BOOL _userDrivenDataModelChange;
}
@property (nonatomic, strong) NSIndexPath *editingIndexPath;

@end

@implementation IKTagsEditViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _fetchController = [IKTag MR_fetchAllGroupedBy:nil withPredicate:nil sortedBy:@"listOrder" ascending:YES delegate:self];
    [self.tableView setEditing:YES animated:NO];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    self.tableView.tableFooterView = [UIView new];
}

- (void)keyboardWillShow:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets;
    if (UIInterfaceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation])) {
        contentInsets = UIEdgeInsetsMake(64.0, 0.0, (keyboardSize.height), 0.0);
    } else {
        contentInsets = UIEdgeInsetsMake(64.0, 0.0, (keyboardSize.width), 0.0);
    }
    
    
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
    
    if (self.editingIndexPath) {
        [self.tableView scrollToRowAtIndexPath:self.editingIndexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    CGPoint oldOffset = self.tableView.contentOffset;
    
    self.tableView.contentInset = UIEdgeInsetsMake(64.0, 0.0, 0.0, 0.0);;
    self.tableView.scrollIndicatorInsets = UIEdgeInsetsMake(64.0, 0.0, 0.0, 0.0);;
    
    self.tableView.contentOffset = oldOffset;
    
    [self.tableView scrollToRowAtIndexPath:self.editingIndexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

- (IBAction)onDoneTapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    UITableViewCell *cell = (UITableViewCell *)[[[textField superview] superview] superview];
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    self.editingIndexPath = indexPath;
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    NSString *text = textField.text ? textField.text : @"No name";
    _userDrivenDataModelChange = YES;
    UITableViewCell *cell = (UITableViewCell *)[[[textField superview] superview] superview];
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    IKTag *currentTag = [_fetchController objectAtIndexPath:indexPath];
    
    BOOL uniq = YES;
    id <NSFetchedResultsSectionInfo> sectionInfo = [[_fetchController sections] objectAtIndex:0];
    NSInteger count = [sectionInfo numberOfObjects];
    for(int i = 0; i < count; i++) {
        IKTag *tag = [_fetchController objectAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
        NSString *tagName = tag.name;
        if (([tagName isEqualToString:text]) && (tag != currentTag)) {
            uniq = NO;
        }
    }
    
    if (!uniq) {
        text = [NSString stringWithFormat:@"%@(copy)", text];
    }

    currentTag.name = text;
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
    self.editingIndexPath = nil;
    
    if (indexPath) {
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    }
    
    _userDrivenDataModelChange = NO;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[_fetchController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [[_fetchController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    IKTagsEditCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(IKTagsEditCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    IKTag *tag = [_fetchController objectAtIndexPath:indexPath];
    NSString *name = tag.name;
    cell.textField.text = name;
    cell.textField.delegate = self;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

-(BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        _removedIndexPath = indexPath;
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Remove Tag" message:@"Deleting a Tag also deletes in all cards included in it. Are you sure you want delete the tag?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
        [alert show];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 1)
    {
        IKTag *tag = [_fetchController objectAtIndexPath:_removedIndexPath];
        [tag MR_deleteEntity];
        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
    }
}


#pragma mark - UITableView Delegate
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    _userDrivenDataModelChange = YES;
    
    NSMutableArray *things = [[_fetchController fetchedObjects] mutableCopy];
    IKTag *thing = [_fetchController objectAtIndexPath:fromIndexPath];
    
    [things removeObject:thing];
    [things insertObject:thing atIndex:[toIndexPath row]];
    
    int i = 0;
    for (NSManagedObject *mo in things)
    {
        [mo setValue:[NSNumber numberWithInt:i++] forKey:@"listOrder"];
    }
    
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
    
    _userDrivenDataModelChange = NO;
}


- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - NSFetchedResultsControllerDelegate
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    if (_userDrivenDataModelChange) return;
    
    [self.tableView beginUpdates];
}


- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    
    if (_userDrivenDataModelChange) return;
    
    UITableView *tableView = self.tableView;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:(IKTagsEditCell *)[tableView cellForRowAtIndexPath:indexPath]
                    atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    if (_userDrivenDataModelChange) return;
    [self.tableView endUpdates];
}

@end
