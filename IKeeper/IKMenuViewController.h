//
//  IKMenuViewController.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 06.01.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IKMenuViewController : UIViewController

@property (nonatomic, weak) IBOutlet UITableView *topTableView;

@property (nonatomic, weak) IBOutlet UITableView *categoriesTableView;

@property (nonatomic, weak) IBOutlet UIImageView *backgroundImageView;
@property (nonatomic, weak) IBOutlet UIImageView *appImageView;
@property (nonatomic, weak) IBOutlet UIButton *infoButton;

@property (nonatomic, weak) IBOutlet UILabel *versionLabel;
@property (nonatomic, weak) IBOutlet UILabel *appNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *syncDateLabel;
@property (nonatomic, weak) IBOutlet UIImageView *icloudImage;
@property (nonatomic, assign) BOOL statusBarHidden;
@property (nonatomic, assign) UIStatusBarStyle lastStyle;

- (IBAction)onSettingsBtnTapped:(id)sender;
- (void)onAllCardsBtnTapped:(id)sender;
- (void)onFavoritesCardsBtnTapped:(id)sender;

@end
