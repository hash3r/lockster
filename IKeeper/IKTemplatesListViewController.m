//
//  IKTemplatesListViewController.m
//  IKeeper
//
//  Created by Chebulaev Oleg on 10.01.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import "IKTemplatesListViewController.h"
#import "IKCardTemplate.h"
#import "IKTemplateListCell.h"
#import "IKCardEditViewController.h"
#import "IKCardTemplate.h"
#import <UIImage-Resize/UIImage+Resize.h>
#import "UIImage+RTTint.h"
#import "IKTemplateEditViewController.h"
#import "IKTypeEditViewController.h"

@interface IKTemplatesListViewController () <NSFetchedResultsControllerDelegate, IKTemplateEditViewControllerDelegate> {
    NSFetchedResultsController  *_fetchController;
    BOOL _userDrivenDataModelChange;
    NSIndexPath* _checkedIndexPath;
    NSIndexPath *_removedIndexPath;
}

@end

@implementation IKTemplatesListViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setEditBarBtn];
    
    if (self.isMediator) {
        [self setBackBtn];
        NSInteger numberOfObjects = [[_fetchController fetchedObjects] count];
        for (int i = 0; i < numberOfObjects; i++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
            id obj = [_fetchController objectAtIndexPath:indexPath];
            if (obj == self.currentTemplate) {
                _checkedIndexPath = indexPath;
            }
        }
    } else {
        [self setCancelBarBtn];
    }
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    if (self.startEditing) {
        [self onEditTapped:nil];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    _fetchController = [IKCardTemplate MR_fetchAllGroupedBy:nil withPredicate:nil sortedBy:@"listOrder" ascending:YES delegate:self];
    [self.tableView reloadData];
}

- (void)setEditBarBtn {
    UIBarButtonItem *btn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(onEditTapped:)];
    self.navigationItem.rightBarButtonItem = btn;
}

- (void)setCancelBarBtn {
    UIBarButtonItem *btn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(onCancelTapped:)];
    self.navigationItem.leftBarButtonItem = btn;
}

- (void)setDoneBarBtn {
    UIBarButtonItem *btn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(onDoneTapped:)];
    self.navigationItem.rightBarButtonItem = btn;
    
}

- (void)setAddBarBtn {
    UIBarButtonItem *btn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(onAddTapped:)];
    self.navigationItem.leftBarButtonItem = btn;
}

- (void)setBackBtn {
//    UIBarButtonItem *btn = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:self action:@selector(onBackTapped:)];
//    self.navigationItem.leftBarButtonItem = btn;
    self.navigationItem.leftBarButtonItem = self.navigationItem.backBarButtonItem;
}

- (void)onCancelTapped:(id)sender {
    [self.delegate onCancelSelectTemplateInViewController:self];
}

- (void)onEditTapped:(id)sender {
    [self setAddBarBtn];
    [self setDoneBarBtn];
    [self.tableView setEditing:YES animated:YES];
}

- (void)onDoneTapped:(id)sender {
    [self setEditBarBtn];
    
    if (self.isMediator) {
        [self setBackBtn];
    } else {
        [self setCancelBarBtn];
    }
    
    [self.tableView setEditing:NO animated:YES];
}

- (void)onAddTapped:(id)sender {
    UIStoryboard *s = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    UINavigationController *navVc = [s instantiateViewControllerWithIdentifier:@"TemplateEditNav"];
    IKTemplateEditViewController *vc = (IKTemplateEditViewController*)[navVc topViewController];
    vc.delegate = self;
    [self presentViewController:navVc animated:YES completion:nil];
}

- (void)onDismissViewController:(UIViewController *)vc {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)onBackTapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[_fetchController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([[_fetchController sections] count] > 0) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [[_fetchController sections] objectAtIndex:section];
        return [sectionInfo numberOfObjects];
    } else
        return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    IKTemplateListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(IKTemplateListCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    IKCardTemplate *template = [_fetchController objectAtIndexPath:indexPath];
    NSString *name = template.name;
//    NSNumber *order = template.listOrder;
//    cell.titleLabel.text = [NSString stringWithFormat:@"%@ %d", name, [order integerValue]];
    cell.titleLabel.text = name;
    
    UIImage *image  = [template image];
    UIColor *tintColor = [template imageColor];
    if (!tintColor) {
        tintColor = [UIColor blackColor];
    }
    image = [image rt_tintedImageWithColor:tintColor];
    cell.iconImageView.image = image;
    
    if([_checkedIndexPath isEqual:indexPath]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

-(BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
//    return 0.5;
//}
//
//- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
//    return [UIView new];
//}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        _removedIndexPath = indexPath;
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Remove Category" message:@"Deleting a Сategory also deletes all cards included in it. Are you sure you want delete the category?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
        [alert show];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 1)
    {
        IKCardTemplate *template = [_fetchController objectAtIndexPath:_removedIndexPath];
        [template MR_deleteEntity];
        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
    }
}


#pragma mark - UITableView Delegate
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    _userDrivenDataModelChange = YES;
    
    NSMutableArray *things = [[_fetchController fetchedObjects] mutableCopy];
    IKCardTemplate *thing = [_fetchController objectAtIndexPath:fromIndexPath];
    
    [things removeObject:thing];
    [things insertObject:thing atIndex:[toIndexPath row]];
    
    int i = 0;
    for (NSManagedObject *mo in things)
    {
        [mo setValue:[NSNumber numberWithInt:i++] forKey:@"listOrder"];
    }
    
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
    
    _userDrivenDataModelChange = NO;
}


- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (tableView.isEditing) {
        UIStoryboard *s = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
        UINavigationController *navVc = [s instantiateViewControllerWithIdentifier:@"TemplateEditNav"];
        IKTemplateEditViewController *vc = (IKTemplateEditViewController*)[navVc topViewController];
        vc.delegate = self;
        IKCardTemplate *template = [_fetchController objectAtIndexPath:indexPath];
        vc.cardTemplate = template;
        [self presentViewController:navVc animated:YES completion:nil];
    } else {
        if (self.isMediator && !self.isNeedShowDetail) {
            if(_checkedIndexPath) {
                UITableViewCell* uncheckCell = [tableView
                                                cellForRowAtIndexPath:_checkedIndexPath];
                uncheckCell.accessoryType = UITableViewCellAccessoryNone;
            }
            
            UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            _checkedIndexPath = indexPath;
        } else if (self.isMediator && !self.isNeedShowDetail) {
            
        }
        
        IKCardTemplate *cardTemplate  = [_fetchController objectAtIndexPath:indexPath];
//        if (self.isMediator) {
            [self.delegate onSelectTemplate:cardTemplate inViewController:self];
//        } else {
//            [self.delegate onSelectTemplate:cardTemplate inViewController:self];
//        }
    }
}

#pragma mark - NSFetchedResultsControllerDelegate
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    if (_userDrivenDataModelChange) return;
    
    [self.tableView beginUpdates];
}


- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    
    if (_userDrivenDataModelChange) return;
    
    UITableView *tableView = self.tableView;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:(IKTemplateListCell *)[tableView cellForRowAtIndexPath:indexPath]
                    atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    if (_userDrivenDataModelChange) return;
    [self.tableView endUpdates];
}



@end
