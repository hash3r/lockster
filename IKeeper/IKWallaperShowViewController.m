//
//  IKWallaperShowViewController.m
//  IKeeper
//
//  Created by Chebulaev Oleg on 01.03.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import "IKWallaperShowViewController.h"
#import "IKSettings.h"
#import <UIImage+Additions/UIImage+Additions.h>

@interface IKWallaperShowViewController () {
    UIView *_delimetr;
    UIView *_delimetr1;
}

@end

@implementation IKWallaperShowViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.previewImage) {
        self.imageView.image = self.previewImage;
    } else {
        self.imageView.image = [UIImage imageWithContentsOfFile:self.previewImagePath];
    }
    
    UIImage *whiteImage = [UIImage imageWithColor:[UIColor whiteColor] size:CGSizeMake(159.5, 44)];
    UIImage *grayImage = [UIImage imageWithColor:[UIColor lightGrayColor] size:CGSizeMake(159.5, 44)];
    
    [self.cancelButton setBackgroundImage:whiteImage forState:UIControlStateNormal];
    [self.settingButton setBackgroundImage:whiteImage forState:UIControlStateNormal];
    [self.cancelButton setBackgroundImage:grayImage forState:UIControlStateHighlighted];
    [self.settingButton setBackgroundImage:grayImage forState:UIControlStateHighlighted];
    
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(159.5, 0, 0.5, 44)];
    [self.view addSubview:v];
    v.backgroundColor = [UIColor blackColor];
    _delimetr = v;
    
    UIView *v1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 0.5)];
    [self.view addSubview:v1];
    v1.backgroundColor = [UIColor blackColor];
    _delimetr1 = v1;
    
    
}

- (void)viewWillAppear:(BOOL)animated {
    _delimetr.frame = CGRectMake(159.5, self.view.frame.size.height - 44, 0.5, 44);
    _delimetr1.frame = CGRectMake(0, self.view.frame.size.height - 44, 320, 0.5);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)onSetBackgroundTapped:(id)sender {
    if (self.previewImage) {
        NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString * basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
        
        NSData *imageData = UIImageJPEGRepresentation(self.previewImage, 1.0);

        NSString *name = @"custom.jpg";
        NSString *namePath = [basePath stringByAppendingPathComponent:name];
        [imageData writeToFile:namePath atomically:YES];
        self.previewImagePath = namePath;
    }
    
    [[IKSettings sharedInstance] setWallaperPath:self.previewImagePath];
    [self.delegate onDissmissViewController:self];
}

- (IBAction)onCancelTapped:(id)sender {
    
    [self.delegate onDissmissViewController:self];
}

@end
