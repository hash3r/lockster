//
//  IKCardTagsCell.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 24.01.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TITokenField/TITokenField.h>
#import <HKKTagWriteView.h>

@interface IKCardTagsCell : UITableViewCell {
    NSLayoutConstraint *_leadingContraint;
    NSLayoutConstraint *_trailingContraint;
    UIView *_v;
}

//@property (nonatomic, weak) IBOutlet TITokenFieldView *tokenFieldView;
@property (nonatomic, weak) IBOutlet HKKTagWriteView *tagView;

@end
