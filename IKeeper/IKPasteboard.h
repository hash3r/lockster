//
//  IKPasteboardHelper.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 13.03.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IKPasteboard : NSObject {
    NSTimer *_clipboardTimer;
}

+ (instancetype)sharedInstance;

- (void)setToPasteboard:(NSString *)text;
@end
