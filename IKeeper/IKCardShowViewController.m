//
//  IKCardShowViewController.m
//  IKeeper
//
//  Created by Chebulaev Oleg on 26.01.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import "IKCardShowViewController.h"
#import "IKCardShowDescriptionCell.h"
#import "IKCardShowFieldCell.h"
#import "IKCardEditViewController.h"
#import "IKCardShowNotesCell.h"
#import "IKCardShowTagsCell.h"

#import "IKCardTemplate.h"
#import "IKTag.h"
#import "IKTagCardLink.h"
#import "IKCardField.h"
#import "IKFieldValue.h"

#import <UIImage+RTTint.h>
#import "NSString+Capitalize.h"
#import "SVProgressHUD.h"
#import "IKPasteboard.h"

@interface IKCardShowViewController () <IKCardEditViewControllerDelegate> {
    NSArray *_customFileds;
     UILabel *_notesTextLabel;
    NSMutableDictionary *_textViews;
    NSMutableDictionary *_tagViews;
}

@end

@implementation IKCardShowViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[TIToken appearance] setTintColor:UIColorFromRGB(0x16457d)];
    [[TIToken appearance] setTextColor:[UIColor whiteColor]];
    
    _textViews = [NSMutableDictionary dictionary];
    _tagViews = [NSMutableDictionary dictionary];
    
    _customFileds = [_card orderedFilledCustomFileds];
}

#pragma mark - Event Handlers
- (IBAction)onEditBtnTapped:(id)sender {
    UIStoryboard *s = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    IKCardEditViewController *vc = [s instantiateViewControllerWithIdentifier:@"CardEdit"];
    vc.title = @"Edit Card";
    vc.delegate = self;
    vc.card = _card;
    
    UINavigationController *navVc = [[UINavigationController alloc] initWithRootViewController:vc];
    
    [self presentViewController:navVc animated:YES completion:nil];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    }
    
    if (section == 1) {
        return [_customFileds count];
    }
    
    if (section == 2) {
        if ([[_card tagLinks] count] > 0) {
            return 1;
        } else {
            return 0;
        }
    }
    
    if (section == 3) {
        if ([[_card noteField] hasFilledValue]) {
            return 1;
        } else {
            return 0;
        }
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *DescriptionCellIdentifier = @"DescriptionCell";
    static NSString *FieldCellIdentifier = @"FieldCell";
    static NSString *NotesCellIdentifier = @"NotesCell";
    static NSString *TagsCellIdentifier = @"TagsCell";
    
    if (indexPath.section == 0) {
        IKCardShowDescriptionCell *cell = [tableView dequeueReusableCellWithIdentifier:DescriptionCellIdentifier];
        
        IKCardField *descritionField = [_card descriptionField];
        IKFieldValue *value = [descritionField value];
        NSString *text = [value value];
        cell.descriptionLabel.text = text;
        
        UIImage *icon = [_card icon];
//        UIImage *tinedIcon = [icon rt_tintedImageWithColor:[UIColor blackColor]];
        cell.iconImageView.image = icon;
        
        IKCardTemplate *cardTemplate = [_card template];
        NSString *templateName = [cardTemplate name];
        cell.templateLabel.text = templateName;

        return cell;
    } else if (indexPath.section == 1) {
        IKCardShowFieldCell *cell = [tableView dequeueReusableCellWithIdentifier:FieldCellIdentifier];
        
        IKCardField *field = [_customFileds objectAtIndex:indexPath.row];
        NSString *name = field.name;
        cell.fieldNameLabel.text = name;
        IKFieldValue *value = [field value];
        NSString *text = [value value];
        cell.fieldValueLabel.text = text;
        
        return cell;
    } else if (indexPath.section == 2) {
        IKCardShowTagsCell *cell = [tableView dequeueReusableCellWithIdentifier:TagsCellIdentifier];
        [_tagViews setObject:cell.fieldTagsView forKey:indexPath];
        [self configureTagsCell:cell];
        
        return cell;
    } else if (indexPath.section == 3) {
        IKCardShowNotesCell *cell = [tableView dequeueReusableCellWithIdentifier:NotesCellIdentifier];
        [_textViews setObject:cell.noteTextView forKey:indexPath];
        [self configureNoteCell:cell];
        
        return cell;
    }
    
    return nil;
}

- (void)configureNoteCell:(IKCardShowNotesCell *)cell {
    cell.noteTextView.placeholder = @"";
    cell.noteTextView.font = [UIFont systemFontOfSize:15.0];
    [cell.noteTextView setEditable:NO];
    
    IKCardField *field = [_card noteField];
    IKFieldValue *value = field.value;
    if (value) {
        cell.noteTextView.text = value.value;
    } else {
        cell.noteTextView.text = @"";
    }
}

- (void)configureTagsCell:(IKCardShowTagsCell *)cell {
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"listOrder" ascending:YES];
    
    NSArray *tagLinks = [[_card tagLinks] sortedArrayUsingDescriptors:@[sortDescriptor]];
    
    [cell.fieldTagsView.tokenField removeAllTokens];
    for (IKTagCardLink *l in tagLinks) {
        IKTag *tag = l.tag;
        [cell.fieldTagsView.tokenField addTokenWithTitle:[tag name] representedObject:l];
    }
    
    [cell.fieldTagsView setShowAlreadyTokenized:YES];
    
    cell.fieldTagsView.tokenField.font = [UIFont systemFontOfSize:15];
    cell.fieldTagsView.separator.backgroundColor = [UIColor whiteColor];
    cell.fieldTagsView.scrollEnabled = NO;
    [cell.fieldTagsView.tokenField setTokenizingCharacters:[NSCharacterSet characterSetWithCharactersInString:@",;. "]]; // Default is a comma
    [cell.fieldTagsView.tokenField setPromptText:@"Tags:"];
    [cell.fieldTagsView.tokenField setPlaceholder:@""];
    [cell.fieldTagsView.tokenField setEditable:NO];
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0 ) {
        return 90;
    } else if (indexPath.section == 1) {
        return 60;
    } else if (indexPath.section == 2) {
            return [self tagViewHeightForRowAtIndexPath:indexPath];
    } else if (indexPath.section == 3) {
            return [self textViewHeightForRowAtIndexPath:indexPath];
    }
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.5;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.0;
}
//
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *v = [UIView new];
    v.frame = CGRectMake(0, 0, 320, 0.5);
    
    if (section == 0) {
        v.backgroundColor = [UIColor lightGrayColor];
    } else {
        v.backgroundColor = [UIColor whiteColor];
    }
    return v;
}

- (CGFloat)textViewHeightForRowAtIndexPath: (NSIndexPath*)indexPath {
    UITextView *calculationView = [_textViews objectForKey: indexPath];
    CGFloat textViewWidth = calculationView.frame.size.width;
    if (!calculationView.attributedText) {
        // This will be needed on load, when the text view is not inited yet
        
        calculationView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, 300, 44)];
        
        IKCardField *field = [_card noteField];
        NSString *text = [field.value value] ? field.value.value : @"";
        calculationView.attributedText = [[NSAttributedString alloc] initWithString:text attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15.0]}];
        textViewWidth = 300.0;
    }
    CGSize size = [calculationView sizeThatFits:CGSizeMake(textViewWidth, FLT_MAX)];
    return MAX (100.0, size.height + 32);
}

- (CGFloat)tagViewHeightForRowAtIndexPath: (NSIndexPath*)indexPath {
    TITokenFieldView *calculationView = [_tagViews objectForKey: indexPath];

    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"listOrder" ascending:YES];
    NSArray *tagLinks = [[_card tagLinks] sortedArrayUsingDescriptors:@[sortDescriptor]];
    NSLog(@"%d", [tagLinks count]);
    
    if (!calculationView) {
        calculationView = [[TITokenFieldView alloc] initWithFrame:CGRectMake(0, 0, 275, 44)];
        [_tagViews setObject:calculationView forKey:indexPath];
        [calculationView setShowAlreadyTokenized:YES];
        calculationView.tokenField.font = [UIFont systemFontOfSize:15.0];
        [calculationView.tokenField setTokenizingCharacters:[NSCharacterSet characterSetWithCharactersInString:@",;. "]]; // Default
        [calculationView.tokenField setPromptText:@"Tags:"];
        [calculationView.tokenField setPlaceholder:@""];
    }
    
    calculationView.delegate = nil;
    
    [calculationView.tokenField removeAllTokens];
    
    CGFloat maxHeight = 0;
    for (IKTagCardLink *l in tagLinks) {
        IKTag *tag = l.tag;
        TIToken *token = [calculationView.tokenField addTokenWithTitle:[tag name] representedObject:l];
        maxHeight = MAX(maxHeight, CGRectGetMaxY([token frame]));
    }
    
    calculationView.delegate = self;
    
    return MAX (44.0, maxHeight + 7);
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 1) {
        IKCardShowFieldCell *cell = (IKCardShowFieldCell *)[tableView cellForRowAtIndexPath:indexPath];
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        [SVProgressHUD appearance].hudBackgroundColor = [UIColor colorWithWhite:0.0 alpha:0.75];
        UIImage *tinted = [[UIImage imageNamed:@"copy"] rt_tintedImageWithColor:[UIColor whiteColor]];
        [SVProgressHUD appearance].hudSuccessImage = tinted;
        [SVProgressHUD appearance].hudForegroundColor = [UIColor whiteColor];
        [SVProgressHUD showSuccessWithStatus:@"Copied"];
        
        
        NSString *text = [cell.fieldValueLabel text];
        [[IKPasteboard sharedInstance] setToPasteboard:text];
    }
}

#pragma mark - IKCardEditControllerDelegate 
- (void)onSaveCard:(IKCard *)card viewContoller:(UIViewController *)vc {
    [self dismissViewControllerAnimated:YES completion:nil];
    
    _card = card;
    [[NSManagedObjectContext MR_defaultContext] refreshObject:_card mergeChanges:YES];
    
    
    _customFileds = [_card orderedFilledCustomFileds];
    
    [self.tableView reloadData];
}

- (void)onCancelCard:(IKCard *)card viewContoller:(UIViewController *)vc {
    [self dismissViewControllerAnimated:YES completion:nil];
    
    [self.tableView reloadData];
}

@end
