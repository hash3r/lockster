//
//  UIPlaceHolderTextView.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 23.04.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIPlaceHolderTextView : UITextView


@property (nonatomic, retain) NSString *placeholder;
@property (nonatomic, retain) UIColor *placeholderColor;

-(void)textChanged:(NSNotification*)notification;

@end
