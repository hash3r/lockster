//
//  IKCardField.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 06.01.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "IKBaseField.h"

@class IKCard, IKFieldValue;

@interface IKCardField : IKBaseField

@property (nonatomic, retain) NSNumber *showInMainView;
@property (nonatomic, retain) IKFieldValue *value;
@property (nonatomic, retain) IKCard *card;

- (BOOL)hasFilledValue;

@end
