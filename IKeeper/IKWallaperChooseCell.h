//
//  IKWallaperChooseCell.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 01.03.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IKWallaperChooseCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIButton *screen1;
@property (nonatomic, weak) IBOutlet UIButton *screen2;
@property (nonatomic, weak) IBOutlet UIButton *screen3;

@property (nonatomic, weak) IBOutlet UIImageView *chek1;
@property (nonatomic, weak) IBOutlet UIImageView *chek2;
@property (nonatomic, weak) IBOutlet UIImageView *chek3;

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;

@end

