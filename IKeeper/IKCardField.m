//
//  IKCardField.m
//  IKeeper
//
//  Created by Chebulaev Oleg on 06.01.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import "IKCardField.h"
#import "IKCard.h"
#import "IKFieldValue.h"


@implementation IKCardField

@dynamic showInMainView;
@dynamic value;
@dynamic card;

- (BOOL)hasFilledValue {
    IKFieldValue *value  = [self value];
    NSString *textValue = [value value];
    
    if (!value || !textValue || [textValue isEqualToString:@""]) {
        return NO;
    }
    
    return YES;
}


- (void)willChangeValueForKey:(NSString *)key {
    [super willChangeValueForKey:key];
    [self.card willChangeValueForKey:@"fields"];
}

- (void)didChangeValueForKey:(NSString *)key {
    [super didChangeValueForKey:key];
    [self.card didChangeValueForKey:key];
}

@end
