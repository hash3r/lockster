//
//  IKSettingsViewController.m
//  IKeeper
//
//  Created by Chebulaev Oleg on 23.12.13.
//  Copyright (c) 2013 Chebulaev Oleg. All rights reserved.
//

#import "IKSettingsViewController.h"
#import <DropboxSDK/DropboxSDK.h>
#import "IKWallaperSettingsViewController.h"
#import "IKSettings.h"
#import "IKSettingsSwitchCell.h"
#import "IKTemplatesListViewController.h"
#import "IKBackupSettingsViewController.h"
#import "APViewController.h"
#import "IKAppDelegate.h"
#import "IKSettingsDetailCell.h"
#import "IDMSyncManager.h"
#import <MessageUI/MFMailComposeViewController.h>
#import <UIDevice-Hardware.h>

//#import <MessageUI/MFMailComposeViewController+BlocksKit.h>
@interface IKSettingsViewController () <MFMailComposeViewControllerDelegate>
@property (strong, nonatomic) IBOutlet UISwitch *syncSwitch;
@end

@implementation IKSettingsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (IBAction)onComplete {
    [self.delegate onDismissViewController:self];
}

- (void)onChangeTitleSearchOnly:(id)sender {
    UISwitch *s = (UISwitch *)sender;
    BOOL newValue = [s isOn];
    IKSettings *settings = [IKSettings sharedInstance];
    [settings setTitleSearchOnly:newValue];
}

- (void)onChangeLockOnExit:(id)sender {
    UISwitch *s = (UISwitch *)sender;
    BOOL newValue = [s isOn];
    IKSettings *settings = [IKSettings sharedInstance];
    [settings setLockOnExit:newValue];
}

- (void)onPermanentChanged:(id)sender {
    UISwitch *s = (UISwitch *)sender;
    BOOL newValue = [s isOn];
    IKSettings *settings = [IKSettings sharedInstance];
    [settings setPermanentSearchBar:newValue];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 6;
    } else if (section == 1) {
        return 4;
    } else if (section == 2){
        return 4;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0 && indexPath.row == 0) {
        IKSettingsDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DetailCell"];
        cell.titleLabel.font = [UIFont systemFontOfSize:17];
        cell.titleLabel.text = @"Sync";
        IDMSyncManager *syncManager = [IDMSyncManager sharedSyncManager];
        BOOL value = [syncManager canSynchronize];
        if (value) {
            cell.detailLabel.text = @"Enabled";
        } else {
            cell.detailLabel.text = @"Disabled";
        }
        return cell;
    } else if (indexPath.section == 0 && indexPath.row == 1) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        cell.textLabel.font = [UIFont systemFontOfSize:17];
        cell.textLabel.text = @"Wallpaper";
        return cell;
//    } else if (indexPath.section == 0 && indexPath.row == 2) {
//        IKSettingsSwitchCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SettingsSwitchCell"];
//        cell.titleLabel.text = @"Permanent Search Bar";
//        cell.titleLabel.font = [UIFont systemFontOfSize:17];
//        BOOL value = [[IKSettings sharedInstance] permanentSearchBar];
//        [cell.settingSwitch setOn:value animated:NO];
//        
//        [cell.settingSwitch removeTarget:nil
//                           action:NULL
//                 forControlEvents:UIControlEventAllEvents];
//        [cell.settingSwitch addTarget:self action:@selector(onPermanentChanged:) forControlEvents:UIControlEventValueChanged];
//        
//        return cell;
    } else if (indexPath.section == 0 && indexPath.row == 2) {
        IKSettingsSwitchCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SettingsSwitchCell"];
        cell.titleLabel.text = @"Title Search only";
        cell.titleLabel.font = [UIFont systemFontOfSize:17];
        
        BOOL value = [[IKSettings sharedInstance] titleSearchOnly];
        [cell.settingSwitch setOn:value animated:NO];
        
        [cell.settingSwitch removeTarget:nil
                                  action:NULL
                        forControlEvents:UIControlEventAllEvents];
        [cell.settingSwitch addTarget:self action:@selector(onChangeTitleSearchOnly:) forControlEvents:UIControlEventValueChanged];
        
        return cell;
    } else if (indexPath.section == 0 && indexPath.row == 3) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        cell.textLabel.font = [UIFont systemFontOfSize:17];
        cell.textLabel.text = @"Edit Type List";
        return cell;
    } else if (indexPath.section == 0 && indexPath.row == 4) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        cell.textLabel.font = [UIFont systemFontOfSize:17];
        cell.textLabel.text = @"Edit Tags";
        return cell;
    } else if (indexPath.section == 0 && indexPath.row == 5) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        cell.textLabel.font = [UIFont systemFontOfSize:17];
        cell.textLabel.text = @"Backup & Restore";
        return cell;
    } else if (indexPath.section == 1 && indexPath.row == 0) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        cell.textLabel.font = [UIFont systemFontOfSize:17];
        cell.textLabel.text = @"Change Master Password";
        return cell;
    } else if (indexPath.section == 1 && indexPath.row == 1) {
        IKSettingsDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DetailCell"];
        
        NSInteger s = [[IKSettings sharedInstance] requestAuthAfter];
        NSString *value = @"";
        if (s == INT_MAX) {
            value = @"Never";
        } else {
            value = [NSString stringWithFormat:@"%d min", s];
        }
        cell.titleLabel.text = @"Auto Lock After";
        cell.detailLabel.text = value;
        return cell;
    } else if (indexPath.section == 1 && indexPath.row == 2) {
        IKSettingsDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DetailCell"];
        cell.titleLabel.font = [UIFont systemFontOfSize:17];
        cell.titleLabel.text = @"Clear Clipboard After";
        
        NSInteger s = [[IKSettings sharedInstance] clearClipboardAfter];
        NSString *value = @"";
        if (s == INT_MAX) {
            value = @"Never";
        } else {
            value = [NSString stringWithFormat:@"%d min", s];
        }
        cell.detailLabel.text = value;
        
        return cell;
    } else if (indexPath.section == 1 && indexPath.row == 3) {
        IKSettingsSwitchCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SettingsSwitchCell"];
        cell.titleLabel.text = @"Lock on Exit";
        
        BOOL value = [[IKSettings sharedInstance] lockOnExit];
        [cell.settingSwitch setOn:value animated:NO];
        
        [cell.settingSwitch removeTarget:nil
                                  action:NULL
                        forControlEvents:UIControlEventAllEvents];
        [cell.settingSwitch addTarget:self action:@selector(onChangeLockOnExit:) forControlEvents:UIControlEventValueChanged];
        
        return cell;
    } else if (indexPath.section == 2 && indexPath.row == 0) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        cell.textLabel.font = [UIFont systemFontOfSize:17];
        cell.textLabel.text = @"About";
        return cell;
    } else if (indexPath.section == 2 && indexPath.row == 1) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        cell.textLabel.font = [UIFont systemFontOfSize:17];
        cell.textLabel.text = @"Send Feedback";
        return cell;
    } else if (indexPath.section == 2 && indexPath.row == 2) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        cell.textLabel.font = [UIFont systemFontOfSize:17];
        cell.textLabel.text = @"Quick Tour";
        return cell;
    } else if (indexPath.section == 2 && indexPath.row == 3) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        cell.textLabel.font = [UIFont systemFontOfSize:17];
        cell.textLabel.text = @"FAQ";
        return cell;
    }
    
    return nil;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return @"General";
    } else if (section == 1) {
        return @"Security";
    } else if (section == 2) {
        return @"Support";
    }
    return @"";
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UIStoryboard *s = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    if (indexPath.section == 0 && indexPath.row == 0) {
        UIViewController *vc = [s instantiateViewControllerWithIdentifier:@"Sync"];
        [self.navigationController pushViewController:vc animated:YES];
    } else if (indexPath.section == 0 && indexPath.row == 1) {
        UIStoryboard *s = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
        IKWallaperSettingsViewController *vc = [s instantiateViewControllerWithIdentifier:@"WallaperSettings"];
        [self.navigationController pushViewController:vc animated:YES];
    } else if (indexPath.section == 0 && indexPath.row == 3) {
        IKTemplatesListViewController *vc = [s instantiateViewControllerWithIdentifier:@"TemplateList"];
        vc.isMediator = YES;
        vc.isNeedShowDetail = YES;
        vc.startEditing = YES;
        
        [self.navigationController pushViewController:vc animated:YES];
    } else if (indexPath.section == 0 && indexPath.row == 4) {
        UIViewController *vc = [s instantiateViewControllerWithIdentifier:@"TagsEdit"];
        [self.navigationController pushViewController:vc animated:YES];
    } else if (indexPath.section == 0 && indexPath.row == 5) {
        IKSettingsViewController *vc = [s instantiateViewControllerWithIdentifier:@"BackupSettings"];
        [self.navigationController pushViewController:vc animated:YES];
    } else if (indexPath.section == 1 && indexPath.row == 0) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        APViewController *apc = [[APViewController alloc] initWithParameter:RESET_PASSCODE];
        apc.title = @"Change Password";
        apc.delegate = (id)self;
        [self presentViewController:apc animated:YES completion:nil];
    } else if (indexPath.section == 1 && indexPath.row == 1) {
        UIViewController *vc = [s instantiateViewControllerWithIdentifier:@"RequestAuthAfterSettings"];
        [self.navigationController pushViewController:vc animated:YES];
    } else if (indexPath.section == 1 && indexPath.row == 2) {
        UIViewController *vc = [s instantiateViewControllerWithIdentifier:@"ClearClipboardAfterSettings"];
        [self.navigationController pushViewController:vc animated:YES];
    } else if (indexPath.section == 2 && indexPath.row == 0) {
        UIViewController *vc = [s instantiateViewControllerWithIdentifier:@"About"];
        [self.navigationController pushViewController:vc animated:YES];
    } else if (indexPath.section == 2 && indexPath.row == 1) {
//        UIViewController *vc = [s instantiateViewControllerWithIdentifier:@"Feedback"];
//        [self.navigationController pushViewController:vc animated:YES];
        MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
        [controller setToRecipients:@[@"support@lockster.info"]];
        [controller setSubject:@"Feedback"];
        
        NSString *message = [NSString stringWithFormat:@"\n\n\n----------------\n"];
        message = [message stringByAppendingString:@"Lockster version: 1.0\n"];
        
        NSString *m = [[UIDevice currentDevice] platformString];
        NSString *device  = [NSString stringWithFormat:@"Device: %@\n", m];
        message = [message stringByAppendingString:device];
        
        NSString *v = [[UIDevice currentDevice] systemVersion];
        NSString *version  = [NSString stringWithFormat:@"iOS: %@\n", v];
        message = [message stringByAppendingString:version];
        
        NSArray *langs = [NSLocale preferredLanguages];
        if (langs.count > 0) {
            NSString *l = langs[0];
            NSString *locale = [NSString stringWithFormat:@"Locale: %@\n", l];
            message = [message stringByAppendingString:locale];
        }
        
        [controller setMessageBody:message isHTML:NO];
        [self presentViewController:controller animated:YES completion:nil];
        
    } else if (indexPath.section == 2 && indexPath.row == 2) {
        UIViewController *vc = [s instantiateViewControllerWithIdentifier:@"Tour"];
        [self.navigationController pushViewController:vc animated:YES];
    } else if (indexPath.section == 2 && indexPath.row == 3) {
        UIViewController *vc = [s instantiateViewControllerWithIdentifier:@"Faq"];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (void)validUserAccess:(APViewController *)controller {
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    NSLog(@"MainView - validUserAccess - Delegate");
    //** callback for RESET
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)onCancel:(APViewController *)controller {
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
	[self becomeFirstResponder];
	[self dismissViewControllerAnimated:YES completion:nil];
}

@end
