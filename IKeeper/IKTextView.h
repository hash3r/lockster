//
//  IKTextView.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 15.02.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IKTextView : UITextView

@end
