//
//  TDFonts.m
//  TrainingDiary
//
//  Created by Chebulaev Oleg on 17.08.13.
//  Copyright (c) 2013 Alex Edunov. All rights reserved.
//

#import "IKFonts.h"

@implementation IKFonts
+ (UIFont *)futuraNewDemiWithSize:(CGFloat)size {
    UIFont *font = [UIFont fontWithName:@"FuturaNewDemi-Reg" size:size];
    return font;
}

+ (UIFont *)futuraNewLightWithSize:(CGFloat)size {
    UIFont *font = [UIFont fontWithName:@"FuturaNewLight-Reg" size:size];
    return font;
}

+ (UIFont *)futuraNewBookWithSize:(CGFloat)size {
    UIFont *font = [UIFont fontWithName:@"FuturaNewBook-Reg" size:size];
    return font;
}

+ (UIFont *)futuraNewMediumWithSize:(CGFloat)size {
    UIFont *font = [UIFont fontWithName:@"FuturaNewMedium-Reg" size:size];
    return font;
}

+ (UIFont *)futuraNewBoldWithSize:(CGFloat)size {
    UIFont *font = [UIFont fontWithName:@"FuturaNewBold-Reg" size:size];
    return font;
}

+ (UIFont *)helveticaNewWithSize:(CGFloat)size {
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size:size];
    return font;
}

+ (UIFont *)helveticaNewMediumWithSize:(CGFloat)size {
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:size];
    return font;
}

+ (UIFont *)helveticaNewUltraLightWithSize:(CGFloat)size {
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue-UltraLight" size:size];
    return font;
}
+ (UIFont *)helveticaNewLightWithSize:(CGFloat)size {
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Light" size:size];
    return font;
}

+ (UIFont *)helveticaNewBoldWithSize:(CGFloat)size {
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:size];
    return font;
}

+ (UIFont *)openSansRegularWithSize:(CGFloat)size {
    UIFont *font = [UIFont fontWithName:@"OpenSans" size:size];
    return font;
}

+ (UIFont *)openSansLightWithSize:(CGFloat)size {
    UIFont *font = [UIFont fontWithName:@"OpenSans-Light" size:size];
    return font;
}

+ (UIFont *)cousineRegularWithSize:(CGFloat )size {
    UIFont *font = [UIFont fontWithName:@"Cousine" size:size];
    return font;
}

+ (void)logAllFonts {
    NSArray *familyNames = [[NSArray alloc] initWithArray:[UIFont familyNames]];
    NSArray *fontNames;
    NSInteger indFamily, indFont;
    for (indFamily=0; indFamily<[familyNames count]; ++indFamily)
    {
        NSLog(@"Family name: %@", [familyNames objectAtIndex:indFamily]);
        fontNames = [[NSArray alloc] initWithArray:
                     [UIFont fontNamesForFamilyName:
                      [familyNames objectAtIndex:indFamily]]];
        for (indFont=0; indFont<[fontNames count]; ++indFont)
        {
            NSLog(@"    Font name: %@", [fontNames objectAtIndex:indFont]);
        }
    }
}
@end
