//
//  IKFieldCell.m
//  IKeeper
//
//  Created by Chebulaev Oleg on 13.01.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import "IKFieldCell.h"
#import <SAMGradientView/SAMGradientView.h>

@implementation IKFieldCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib {
    // Set the gradient colors
    _separatorView.gradientColors = @[[UIColor whiteColor], UIColorFromRGB(0xdddddd),[UIColor lightGrayColor]];
    
    // Optionally set some locations
    _separatorView.gradientLocations = @[@0.01, @0.1, @1.0];
    
    // Optionally change the direction. The default is vertical.
    _separatorView.gradientDirection = SAMGradientViewDirectionVertical;
    
    
    if (!self.backgroundView) {
        self.backgroundView = [UIView new];
    }
    
    if (![self.backgroundView viewWithTag:200]) {
        UIView *v = [[UIView alloc] initWithFrame:CGRectMake(44, 43.5, 320-44, 0.5)];
        v.tag = 200;
        v.backgroundColor = [UIColor lightGrayColor];
        [self.backgroundView addSubview:v];
    }
    
    
}



@end
