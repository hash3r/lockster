//
//  IKCardNoteCell.m
//  IKeeper
//
//  Created by Chebulaev Oleg on 23.01.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import "IKCardNoteCell.h"

@implementation IKCardNoteCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib {
    for (NSLayoutConstraint *c in self.contentView.constraints) {
        if (c.firstAttribute == NSLayoutAttributeLeading && c.firstItem == self.noteTextView) {
            _noteTextViewLeadingContraint = c;
        }
        
        if (c.firstAttribute == NSLayoutAttributeLeading && c.firstItem == self.noteLbl) {
            _noteLblLeadingContraint = c;
        }
    }
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
    [super setEditing:editing animated:animated];
    
    self.noteTextView.editable = editing;
    if (editing) {
        self.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
        [UIView animateWithDuration:0.3 animations:^{
//            _noteTextViewLeadingContraint.constant = 10;
//            _noteLblLeadingContraint.constant = 15;
            _noteTextViewLeadingContraint.constant = 40;
            _noteLblLeadingContraint.constant = 45;

            [self layoutIfNeeded];
        } completion:^(BOOL finished) {
            
        }];
    } else {
        self.separatorInset = UIEdgeInsetsMake(0, 320, 0, 0);
        [UIView animateWithDuration:0.3 animations:^{

            _noteTextViewLeadingContraint.constant = 30;
            _noteLblLeadingContraint.constant = 35;
            [self layoutIfNeeded];
        } completion:^(BOOL finished) {
            
        }];
    }

}

@end
