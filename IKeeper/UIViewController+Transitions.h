//
//  UIViewController+Transitions.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 16.02.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Transitions)
- (void) presentViewController:(UIViewController *)viewController withPushDirection: (NSString *) direction;
- (void) dismissViewControllerWithPushDirection:(NSString *) direction;
@end
