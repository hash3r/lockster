//
//  IKCardTemplateCell.m
//  IKeeper
//
//  Created by Chebulaev Oleg on 13.01.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import "IKCardTemplateCell.h"

@implementation IKCardTemplateCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    }
    return self;
}

- (void)awakeFromNib {
    self.roundView.layer.cornerRadius = self.roundView.frame.size.width / 2.0;
    self.roundView.layer.borderWidth = 1.0;
    self.roundView.layer.borderColor = [UIColorFromRGB(0x999999) CGColor];
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
    [super setEditing:editing animated:animated];
    self.descriptionField.userInteractionEnabled = editing;
    self.categoryBtn.enabled = editing;
    self.indicator.hidden = !editing;
    
    if (editing) {
        self.roundView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.1];
        [self.iconBtn setTitle:@"Edit" forState:UIControlStateNormal];
        self.iconBtn.userInteractionEnabled = editing;
    } else {
        self.roundView.backgroundColor = [UIColor whiteColor];
        [self.iconBtn setTitle:@"" forState:UIControlStateNormal];
        self.iconBtn.userInteractionEnabled = NO;
    }
}

@end
