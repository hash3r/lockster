//
//  IKSettings.m
//  IKeeper
//
//  Created by Chebulaev Oleg on 18.02.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import "IKSettings.h"
#import "IKAppDelegate.h"
#import "EncryptedStore.h"

@implementation IKSettings

static IKSettings *_sharedInstance = nil;

+ (IKSettings *)sharedInstance
{
    @synchronized(self)
    {
        if (_sharedInstance == nil)
        {
            _sharedInstance = [[IKSettings alloc] init];
        }
    }
    
    return _sharedInstance;
}

- (void)resetWallaperPath {
    NSString *defaultWallaperImageName = [self defaultWallaperPath];
    [self setWallaperPath:defaultWallaperImageName];
}

- (NSString *)wallaperPath {
    NSString *wallaper = [[NSUserDefaults standardUserDefaults] objectForKey:@"wallaper"];
    if (!wallaper) {
        return [self defaultWallaperPath];
    }
    return wallaper;
}

- (void)setWallaperPath:(NSString *)path {
    [[NSUserDefaults standardUserDefaults] setObject:path forKey:@"wallaper"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationWallaperChanged object:path];
}

- (NSString *)defaultWallaperPath {
    return [[NSBundle mainBundle] pathForResource:@"screen1" ofType:@"jpg"];
}

- (void)setTitleSearchOnly:(BOOL)titleSearchOnly {
    [[NSUserDefaults standardUserDefaults] setBool:titleSearchOnly forKey:@"titleSearchOnly"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationTitleSearchOnlyChanged object:[NSNumber numberWithBool:titleSearchOnly]];
}

- (BOOL)titleSearchOnly {
    BOOL value  = [[NSUserDefaults standardUserDefaults] boolForKey:@"titleSearchOnly"];
    return value;
}

- (void)makeBackup {
    NSString * databaseName = @"IKeeper.sqlite";
    NSString * databaseBackupName = @"IKeeper.backup";
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error = nil;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    
    NSString *backupPath = [documentsDir stringByAppendingPathComponent:databaseBackupName];
    
    NSArray *libraryPath = NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES);
    
    NSString *libraryDir = ([libraryPath count] > 0) ? [libraryPath objectAtIndex:0] : nil;
    NSString *dbPath = [libraryDir stringByAppendingPathComponent:databaseName];
    
    NSURL *toURL = [NSURL fileURLWithPath:backupPath];
    
    if([fileManager fileExistsAtPath:backupPath])
    {
        //get rid of the old copy, only one allowed
        [fileManager removeItemAtPath:backupPath error:&error];
    }
    
    if([fileManager fileExistsAtPath:dbPath])
    {
        if ([fileManager copyItemAtPath:dbPath toPath:backupPath error:&error]) {
            [toURL setResourceValue:[NSNumber numberWithBool:YES] forKey:NSURLIsExcludedFromBackupKey error:&error];
        } else {
            NSLog(@"Failed to create writable database file with message '%@'.", [error localizedDescription]);
        }
    }
}

- (BOOL)restoreFromBackupWithError:(NSError **)error {
    NSArray *stores = [[NSPersistentStoreCoordinator MR_defaultStoreCoordinator] persistentStores];
    NSPersistentStore *store = [stores objectAtIndex:0];
    NSURL *storeURL = store.URL;
    
    [[NSPersistentStoreCoordinator MR_defaultStoreCoordinator] removePersistentStore:store error:error];
    [[NSFileManager defaultManager] removeItemAtPath:storeURL.path error:error];
    
    NSString *databaseName = @"iKeeper.sqlite";
    NSString *databaseBackupName = @"iKeeper.backup";
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    
    NSArray *libraryPath = NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES);
    NSString *libraryDir = ([libraryPath count] > 0) ? [libraryPath objectAtIndex:0] : nil;
    NSString *dbPath = [libraryDir stringByAppendingPathComponent:databaseName];
    
    NSString *backupPath = [documentsDir stringByAppendingPathComponent:databaseBackupName];
    
    if([fileManager fileExistsAtPath:backupPath])
    {
        if (![fileManager copyItemAtPath:backupPath toPath:dbPath error:error])
        {
            NSLog(@"Failed to create writable database file with message '%@'.", [*error localizedDescription]);
            return NO;
        }
        else
        {
//            [MagicalRecord cleanUp];
//            
//            IKAppDelegate *appDelegate = (IKAppDelegate *)[[UIApplication sharedApplication] delegate];
//            
//            NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Store" withExtension:@"momd"];
//            NSManagedObjectModel *model = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
//            
//            
//            
//            NSPersistentStoreCoordinator *coordinator = [EncryptedStore makeStoreWithDatabaseURL:<#(NSURL *)#> managedObjectModel:<#(NSManagedObjectModel *)#> :<#(NSString *)#>];
//            [NSPersistentStoreCoordinator MR_setDefaultStoreCoordinator:coordinator];
//            [NSManagedObjectContext MR_initializeDefaultContextWithCoordinator:coordinator];
//            
//            NSUndoManager *undoManager = [[NSUndoManager alloc] init];
//            [[NSManagedObjectContext MR_defaultContext] setUndoManager:undoManager];
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
//                //Background Thread
//                dispatch_async(dispatch_get_main_queue(), ^(void){
//                   
//                });
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                     [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPersistentStoreChanged object:nil];
                });
            });

        }
    } else {
        if (error) {
            *error  = [NSError errorWithDomain:@"" code:10 userInfo:@{NSLocalizedDescriptionKey: @"Backup file not found. Please place IKeeper.backup file to Documents directory"}];
            
        }
        return NO;
    }
    return YES;
}

#ifdef DEBUG
#define kDefaultRequestAuthAfter INT_MAX
#else
#define kDefaultRequestAuthAfter 1
#endif

- (NSInteger)requestAuthAfter {
    NSNumber *requestAfter = [[NSUserDefaults standardUserDefaults] objectForKey:@"requestAuthAfter"];
    if (!requestAfter) {
        return kDefaultRequestAuthAfter;
    }
    return [requestAfter integerValue];
}

- (void)setRequestAuthAfter:(NSInteger)minutes {
    NSNumber *requestAfter = [NSNumber numberWithInt:minutes];
    [[NSUserDefaults standardUserDefaults] setObject:requestAfter forKey:@"requestAuthAfter"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationRequestAuthAfterChanged object:requestAfter];
}

#define kDefaultClearPasteBoardAfter 1
- (NSInteger)clearClipboardAfter {
    NSNumber *clearAfter = [[NSUserDefaults standardUserDefaults] objectForKey:@"clearClipboardAfter"];
    if (!clearAfter) {
        return kDefaultClearPasteBoardAfter;
    }
    return [clearAfter integerValue];
}

- (void)setClearClipboardAfter:(NSInteger)minutes {
    NSNumber *clearAfter = [NSNumber numberWithInt:minutes];
    [[NSUserDefaults standardUserDefaults] setObject:clearAfter forKey:@"clearClipboardAfter"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationClearClipboardAfterChanged object:clearAfter];
}

#ifdef DEBUG
#define kDefaultLockOnExit NO
#else
#define kDefaultLockOnExit YES
#endif

- (BOOL)lockOnExit {
    NSNumber *lock = [[NSUserDefaults standardUserDefaults] objectForKey:@"lockOnExit"];
    if (!lock) {
        return kDefaultLockOnExit;
    }
    return [lock boolValue];
}

- (void)setLockOnExit:(BOOL)lock {
    NSNumber *lockObject = [NSNumber numberWithInt:lock];
    [[NSUserDefaults standardUserDefaults] setObject:lockObject forKey:@"lockOnExit"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationLockOnExitChanged object:lockObject];
}

#define kDefaultPermanentSearchBar NO
- (BOOL)permanentSearchBar {
    NSNumber *permanent = [[NSUserDefaults standardUserDefaults] objectForKey:@"permanent"];
    if (!permanent) {
        return kDefaultPermanentSearchBar;
    }
    return [permanent boolValue];
}
- (void)setPermanentSearchBar:(BOOL)permanent {
    NSNumber *permanentObject = [NSNumber numberWithInt:permanent];
    [[NSUserDefaults standardUserDefaults] setObject:permanentObject forKey:@"permanent"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPermanentSeaarchBarChanged object:permanentObject];

}

@end
