//
//  IKTemplateView.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 09.04.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@class IKButton;

@protocol IKTemplateViewDelegate
- (void)onChangeFavorite:(BOOL)value;
@end

@interface IKTemplateView : UIView {
    UIImage *_iconImage;
}

@property (nonatomic, weak) id<IKTemplateViewDelegate> delegate;

@property (nonatomic, strong) UIView *roundView;
@property (nonatomic, strong) IKButton *iconBtn;
@property (nonatomic, strong) UIButton *favoriteButton;
@property (nonatomic, strong) UITextField *descriptionField;

@property (nonatomic, weak) UIButton *categoryBtn;
@property (nonatomic, weak) UIImageView *indicator;

- (void)setEditing:(BOOL)editing animated:(BOOL)animated;
- (void)setFavoriteValue:(BOOL)favorite;
@end
