//
//  IKWallaperChooseCell.m
//  IKeeper
//
//  Created by Chebulaev Oleg on 01.03.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import "IKWallaperChooseCell.h"

@implementation IKWallaperChooseCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
