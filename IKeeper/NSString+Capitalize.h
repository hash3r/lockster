//
//  NSString+Capitalize.h
//  TrainingDiary
//
//  Created by Chebulaev Oleg on 27.08.13.
//  Copyright (c) 2013 Alex Edunov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Capitalize)
- (NSString *)capitalizeFirstLettterString;
@end
