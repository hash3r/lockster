//
//  IKDrawerController.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 17.04.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import "MMDrawerController.h"

@interface IKDrawerController : MMDrawerController


@property (nonatomic, assign) BOOL isDragged;


@end
