//
//  IKClearClipboardAfterSettingsViewController.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 13.03.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IKClearClipboardAfterSettingsViewController : UITableViewController

@end
