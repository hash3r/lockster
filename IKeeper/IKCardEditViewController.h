//
//  IKCardEditViewController.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 06.01.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@class IKCardTemplate, IKCard;

@protocol IKCardEditViewControllerDelegate
- (void)onSaveCard:(IKCard *)card viewContoller:(UIViewController *)vc;
- (void)onCancelCard:(IKCard *)card viewContoller:(UIViewController *)vc;
@end

@interface IKCardEditViewController : UIViewController

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) id<IKCardEditViewControllerDelegate> delegate;
@property (nonatomic, strong) IKCardTemplate *cardTemplate;
@property (nonatomic, retain) IKCard *card;
@property (nonatomic, assign) BOOL isNewFav;

- (void)onEditTapped:(id)sender;
- (void)onBackTapped:(id)sender;
- (void)onCancelTapped:(id)sender;
- (void)onDoneTapped:(id)sender;
- (IBAction)onChangeFieldTypeTapped:(id)sender;

@end
