//
//  IKIconsListViewController.m
//  IKeeper
//
//  Created by Chebulaev Oleg on 13.01.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import "IKIconsListViewController.h"
#import "IKIconCell.h"
#import <UIImage-Resize/UIImage+Resize.h>
#import "UIImage+RTTint.h"
#import <IKIconColorViewController.h>

@interface IKIconsListViewController () <UICollectionViewDataSource, UICollectionViewDelegate>
{
    NSArray *_iconPaths;
}

@end

@implementation IKIconsListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSString *path = [[NSBundle mainBundle] pathForResource:@"icon_names" ofType:@"plist"];
    _iconPaths = [NSArray arrayWithContentsOfFile:path];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section {
    return [_iconPaths count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    IKIconCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"IconCell" forIndexPath:indexPath];
    
    NSString *iconName = _iconPaths[indexPath.row];
    UIImage *iconImage = [UIImage imageNamed:iconName];
//    UIImage *tintedImage = [iconImage rt_tintedImageWithColor:[UIColor blackColor]];
    
    cell.iconImageView.image = iconImage;
    return cell;
}

#pragma mark – UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGSize retval = CGSizeMake(30, 30);
    return retval;
}

//- (UIEdgeInsets)collectionView:
//(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
//    return UIEdgeInsetsMake(50, 20, 50, 20);
//}

#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView
didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *iconName = _iconPaths[indexPath.row];
    
    UIStoryboard *s = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    IKIconColorViewController *vc = [s instantiateViewControllerWithIdentifier:@"IconColor"];
    vc.delegate = self.delegate;
    vc.iconName = iconName;
    
    [self.navigationController pushViewController:vc animated:YES];
}




@end
