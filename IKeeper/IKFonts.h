//
//  TDFonts.h
//  TrainingDiary
//
//  Created by Chebulaev Oleg on 17.08.13.
//  Copyright (c) 2013 Alex Edunov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IKFonts : NSObject
+ (UIFont *)futuraNewDemiWithSize:(CGFloat)size;
+ (UIFont *)futuraNewBookWithSize:(CGFloat)size;
+ (UIFont *)futuraNewBoldWithSize:(CGFloat)size;
+ (UIFont *)futuraNewLightWithSize:(CGFloat)size;
+ (UIFont *)futuraNewMediumWithSize:(CGFloat)size;
+ (UIFont *)helveticaNewWithSize:(CGFloat)size;
+ (UIFont *)helveticaNewMediumWithSize:(CGFloat)size;
+ (UIFont *)helveticaNewUltraLightWithSize:(CGFloat)size;
+ (UIFont *)helveticaNewLightWithSize:(CGFloat)size;
+ (UIFont *)helveticaNewBoldWithSize:(CGFloat)size;
+ (UIFont *)openSansRegularWithSize:(CGFloat)size;
+ (UIFont *)openSansLightWithSize:(CGFloat)size;
+ (UIFont *)cousineRegularWithSize:(CGFloat )size;
    

@end
