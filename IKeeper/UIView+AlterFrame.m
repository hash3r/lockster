//
//  UIView+AlterFrame.m
//  IKeeper
//
//  Created by Chebulaev Oleg on 17.01.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import "UIView+AlterFrame.h"

@implementation UIView (AlterFrame)
- (void) setFrameWidth:(CGFloat)newWidth {
    CGRect f = self.frame;
    f.size.width = newWidth;
    self.frame = f;
}

- (void) setFrameHeight:(CGFloat)newHeight {
    CGRect f = self.frame;
    f.size.height = newHeight;
    self.frame = f;
}

- (void) setFrameOriginX:(CGFloat)newX {
    CGRect f = self.frame;
    f.origin.x = newX;
    self.frame = f;
}

- (void) setFrameOriginY:(CGFloat)newY {
    CGRect f = self.frame;
    f.origin.y = newY;
    self.frame = f;
}

@end
