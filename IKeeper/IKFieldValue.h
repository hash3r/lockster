//
//  IKFieldValue.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 06.01.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class IKCardField;

@interface IKFieldValue : NSManagedObject

@property (nonatomic, retain) NSString * value;
@property (nonatomic, retain) IKCardField *field;
@property (nonatomic, retain) NSString *uniqueIdentifier;

@end
