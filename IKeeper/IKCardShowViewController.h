//
//  IKCardShowViewController.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 26.01.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IKCard.h"

@interface IKCardShowViewController : UITableViewController


@property (nonatomic, weak) IKCard *card;

- (IBAction)onEditBtnTapped:(id)sender;
@end
