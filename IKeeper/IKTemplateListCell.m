//
//  IKTemplateListCell.m
//  IKeeper
//
//  Created by Chebulaev Oleg on 12.01.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import "IKTemplateListCell.h"

@implementation IKTemplateListCell

- (void)awakeFromNib {
    self.roundView.layer.cornerRadius = self.roundView.frame.size.width / 2.0;
    self.roundView.layer.borderWidth = 1.0;
    self.roundView.layer.borderColor = [UIColorFromRGB(0x999999) CGColor];
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [super setHighlighted:highlighted animated:animated];
}

@end
