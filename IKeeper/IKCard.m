//
//  IKCard.m
//  IKeeper
//
//  Created by Chebulaev Oleg on 06.01.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import "IKCard.h"
#import "IKCardField.h"
#import "IKCardTemplate.h"
#import "IKTag.h"
#import "IKTagCardLink.h"
#import "IKFieldValue.h"
#import "IKFieldType.h"
#import "IKTemplateField.h"
#import "IKBaseField.h"


@implementation IKCard

@dynamic name;
@dynamic fields;
@dynamic tagLinks;
@dynamic template;
@dynamic iconName;
@dynamic uniqueIdentifier;
@dynamic isFavorite;
@dynamic iconColor;

- (void)awakeFromInsert
{
    [super awakeFromInsert];
    if (!self.uniqueIdentifier) {
        self.uniqueIdentifier = [[NSProcessInfo processInfo] globallyUniqueString];
    }
}

// call before save!!
- (void)updateNameField {
    IKCardField *df = [self descriptionField];
    NSString *name = [[df value] value];
    if (name && ![self.name isEqualToString:name]) {
        self.name = name;
    }
}

- (BOOL)isValidModel:(NSError **)error {
    IKCardField *descriptionField = [self descriptionField];
    if (!descriptionField) {
        NSDictionary *userInfoDict = [NSDictionary
                                      dictionaryWithObject:@"Description is required field"
                                      forKey:NSLocalizedDescriptionKey];
        *error = [[NSError alloc] initWithDomain:
                  NSCocoaErrorDomain code:-1 userInfo:userInfoDict];

        return NO;
    }
    
    IKFieldValue *fieldValue = [descriptionField value];
    NSString *val = [fieldValue value];
    
    if (val == nil || [val length] == 0) {
        NSDictionary *userInfoDict = [NSDictionary
                                      dictionaryWithObject:@"Description is required field"
                                      forKey:NSLocalizedDescriptionKey];
        *error = [[NSError alloc] initWithDomain:
                  NSCocoaErrorDomain code:-1 userInfo:userInfoDict];
        
        
        return NO;
    }
    
    return YES;
}

- (IKCardField *)createPasswordField {
    IKCardField *f = [IKCardField MR_createEntity];
    f.type = [IKFieldType passwordType];
    f.name = @"Password";
    f.showInMainView = [NSNumber numberWithBool:NO];
    f.listOrder = [NSNumber numberWithInt:[self maxListOrder] + 1];
    [self addFieldsObject:f];
    
    return f;
}

- (IKCardField *)createField {
    IKCardField *f = [IKCardField MR_createEntity];
    f.type = [IKFieldType defaultType];
    
    if ([self canAddFavoriteField]) {
        f.showInMainView = [NSNumber numberWithBool:YES];
    } else {
        f.showInMainView = [NSNumber numberWithBool:NO];
    }
    f.listOrder = [NSNumber numberWithInt:[self maxListOrder] + 1];
    [self addFieldsObject:f];
    
    return f;
}

- (void)replaceFieldsFromTemplate:(IKCardTemplate *)cardTemplate {
    [self removeFields:self.fields];
    
    for (IKTemplateField *templateField in cardTemplate.fields) {
        IKCardField *f = [IKCardField MR_createEntity];
        f.name = templateField.name;
        f.type = templateField.type;
        if ([self canAddFavoriteField] && ![f isNoteField] && ![f isDescriptionField]) {
            f.showInMainView = [NSNumber numberWithBool:YES];
        } else {
            f.showInMainView = [NSNumber numberWithBool:NO];
        }
        f.listOrder = [NSNumber numberWithInt:[templateField.listOrder integerValue]];
        f.card = self;
        [self addFieldsObject:f];
    }
}

- (BOOL)canAddFavoriteField {
    NSPredicate *p = [NSPredicate predicateWithFormat:@"showInMainView == %@", [NSNumber numberWithBool:YES]];
    NSSet *favoriteFields = [self.fields filteredSetUsingPredicate:p];
    if ([favoriteFields count] < 3) {
        return YES;
    }
    return NO;
}

- (BOOL)hasFilledCustomFields {
    NSArray *fields = [self customFields];
    for (IKCardField *f in fields) {
        if ([f hasFilledValue]) {
            return YES;
        }
    }
    return NO;
}

- (NSInteger)tagsMaxListOrder {
    NSInteger maxListOrder = -1;
    for (IKTagCardLink *l in self.tagLinks) {
        if ([l.listOrder integerValue] > maxListOrder) {
            maxListOrder = [l.listOrder integerValue];
        }
    }
    return maxListOrder;
}

- (NSArray *)tags {
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"listOrder" ascending:YES];
    
    NSArray *sortedLinks = [self.tagLinks sortedArrayUsingDescriptors:@[sortDescriptor]];
    NSMutableArray *tags = [NSMutableArray array];
    for (IKTagCardLink *l in sortedLinks) {
        [tags addObject:l.tag];
    }
    return [NSArray arrayWithArray:tags];
}

- (IKTagCardLink *)addTagWithName:(NSString *)name {
    NSManagedObjectContext *localContext = [self managedObjectContext];
    IKTag *tag = [IKTag MR_findFirstByAttribute:@"name" withValue:name inContext:localContext];
    if (!tag) {
        tag = [IKTag MR_createInContext:localContext];
        tag.name = name;
    }
    
    IKTagCardLink *link = [IKTagCardLink MR_createInContext:localContext];
    link.tag = tag;
    
    NSInteger listOrder = [self tagsMaxListOrder] + 1;
    link.listOrder = @(listOrder);
    
    [self addTagLinksObject:link];
    return link;
}

- (void)removeTagWithName:(NSString *)name {
    NSMutableSet *linksToRemove = [NSMutableSet set];
    for (IKTagCardLink *link in self.tagLinks) {
        if ([[link.tag name] isEqualToString:name]) {
            [linksToRemove addObject:link];
        }
    }
    [self removeTagLinks:linksToRemove];
}


- (NSArray *)tagsNames {
    NSArray *tags = [self tags];
    
    NSMutableArray *tagNames = [NSMutableArray array];
    for (IKTag *tag in tags) {
        NSString *name = [tag name];
        [tagNames addObject:name];
    }
    return [NSArray arrayWithArray:tagNames];
}

- (NSArray *)favoriteFields {
    NSArray *customFields = [self orderedCustomFields];
    NSPredicate *fillPredicate = [NSPredicate predicateWithFormat:@"value != nil && value.value != %@", @""];
    NSPredicate *visibilityPredicate = [NSPredicate predicateWithFormat:@"showInMainView == %@", [NSNumber numberWithBool:YES]];
    NSPredicate *commonPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[fillPredicate, visibilityPredicate]];

    NSArray *fields = [customFields filteredArrayUsingPredicate:commonPredicate];
    return fields;
}

- (UIImage *)icon {
    NSString *iconName = [self iconName];
    UIImage *image = [UIImage imageNamed:iconName];
    return image;
}

- (IKCardField *)descriptionField {
    IKCardField *descriptionField = nil;
    for (IKCardField *f in self.fields) {
        if ([f isDescriptionField]) {
            descriptionField = f;
        }
    }
    return descriptionField;
}

- (IKCardField *)noteField {
    for (IKCardField *f in self.fields) {
        if ([f isNoteField]) {
            return f;
        }
    }
    return nil;
}

- (NSArray *)customFields {
    NSMutableArray * customFields = [NSMutableArray array];
    for (IKCardField *f in self.fields) {
        if (![f isDescriptionField] && ![f isNoteField]) {
            [customFields addObject:f];
        }
    }
    return customFields;
}

- (NSArray *)orderedCustomFields {
    NSMutableArray *customFields = [[self customFields] mutableCopy];
    NSSortDescriptor *listSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"listOrder" ascending:YES];
    [customFields sortUsingDescriptors:@[listSortDescriptor]];
    return [NSArray arrayWithArray:customFields];
}

- (NSArray *)orderedFilledCustomFileds {
    NSArray *customFields = [self orderedCustomFields];
    NSMutableArray *filledFields = [NSMutableArray array];
    for (IKCardField *f in customFields) {
        if ([f hasFilledValue]) {
            [filledFields addObject:f];
        }
    }
    return [NSArray arrayWithArray:filledFields];
}

- (NSInteger)maxListOrder {
    NSInteger maxValue = -1;
    for (IKCardField *f in self.fields) {
        if ([f.listOrder integerValue] > maxValue) {
            maxValue = [f.listOrder integerValue];
        }
    }
    return maxValue;
}

@end
