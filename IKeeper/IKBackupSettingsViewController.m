//
//  IKBackupSettingsViewController.m
//  IKeeper
//
//  Created by Chebulaev Oleg on 06.03.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import "IKBackupSettingsViewController.h"
#import "EncryptedStore.h"
#import "IKAppDelegate.h"

#import "IKSettings.h"
#import <NSPersistentStoreCoordinator+MagicalRecord.h>
#import <NSDate+MTDates.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <zipzap/zipzap.h>


@interface IKBackupSettingsViewController () <UIActionSheetDelegate, MFMailComposeViewControllerDelegate> {
    NSMutableArray *_paths;
    NSString *_backupsPath;
    NSString *_restorePath;
}

@end


@implementation IKBackupSettingsViewController

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        NSArray *allPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [allPaths objectAtIndex:0];
        _backupsPath = documentsDirectory;
//        _backupsPath = [documentsDirectory stringByAppendingPathComponent:@"backups"];
        [self updateDataStore];
    }
    return self;
}

- (void)updateDataStore {
    _paths = [[[NSFileManager defaultManager] contentsOfDirectoryAtPath:_backupsPath  error:nil] mutableCopy];
    
    NSSortDescriptor* sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:nil ascending:NO selector:@selector(localizedCaseInsensitiveCompare:)];
    [_paths sortUsingDescriptors:@[sortDescriptor]];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setEditBar];

}

- (void)setEditBar {
    UIBarButtonItem *btn = [[UIBarButtonItem alloc] initWithTitle:@"Edit" style:UIBarButtonItemStyleBordered target:self action:@selector(onEditTapped)];
    self.navigationItem.rightBarButtonItem = btn;
    
}

- (void)setDoneBar {
    UIBarButtonItem *btn = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(onDoneTapped)];
    self.navigationItem.rightBarButtonItem = btn;
}

- (void)onEditTapped {
    [self setDoneBar];
    [self.tableView setEditing:YES animated:YES];
}

- (void)onDoneTapped {
    [self setEditBar];
    [self.tableView setEditing:NO animated:YES];
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return UITableViewCellEditingStyleNone;
    } else {
        return UITableViewCellEditingStyleDelete;
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *selectedPath = _paths[indexPath.row];
    NSString *removePath = [_backupsPath stringByAppendingPathComponent:selectedPath];
    [[NSFileManager defaultManager] removeItemAtPath:removePath error:nil];
    [self updateDataStore];
    [self.tableView reloadData];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    } else {
        return [_paths count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if (indexPath.row == 0 && indexPath.section == 0) {
        cell.textLabel.text = @"Create New Backup";
    } else {
        NSString *backupPath = _paths[indexPath.row];
        NSString *timestamp = [backupPath lastPathComponent];
        double interval = [timestamp doubleValue];
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:interval];
        NSString *representiveValue = [date mt_stringFromDateWithFormat:@"dd/MM/yyyy HH:mm" localized:YES];
        cell.textLabel.text = representiveValue;
    }
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return @"Backup";
    } else if (section == 1) {
        return @"Backups";
    }
    return @"";
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 0) {
        NSString *timeStampValue = [NSString stringWithFormat:@"%ld", (long)[[NSDate date] timeIntervalSince1970]];
        
        NSString *backupFolderPath = [_backupsPath stringByAppendingPathComponent:timeStampValue];
        NSURL *backupFolderUrl = [NSURL fileURLWithPath:backupFolderPath isDirectory:YES];
        [[NSFileManager defaultManager] createDirectoryAtURL:backupFolderUrl withIntermediateDirectories:YES attributes:nil error:NULL];
        
        
        NSString *filePath = [backupFolderPath stringByAppendingPathComponent:@"iKeeper.backup"];
        NSURL *fileUrl = [[NSURL alloc] initFileURLWithPath:filePath];

        NSError *error = nil;
        
        NSPersistentStoreCoordinator *psc = [NSPersistentStoreCoordinator MR_defaultStoreCoordinator];
        
        NSPersistentStore *store = [psc persistentStores][0];
        
        NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Store" withExtension:@"momd"];
        NSManagedObjectModel *model = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
        
        NSURL *storeUrl  = [store URL];
        NSPersistentStoreCoordinator *tempPsc = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:model];
        
        NSPersistentStore *tmpStore = [tempPsc addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeUrl options:nil error:&error];
        
        [tempPsc migratePersistentStore:tmpStore toURL:fileUrl options:nil withType:NSSQLiteStoreType error:&error];
        NSLog(@"%@", [error localizedDescription]);
        [self updateDataStore];
        [self.tableView reloadData];
    } else {
        NSString *selectedPath = _paths[indexPath.row];
        _restorePath = [_backupsPath stringByAppendingPathComponent:selectedPath];
        
        UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:@"Restoring data will erase all current data from the app without the ability to recover. Are you sure?" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Erase All & Restore" otherButtonTitles:@"Email Backup", nil];
        [sheet showInView:self.view];
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        NSString *dbFileName = [_restorePath stringByAppendingPathComponent:@"iKeeper.backup"];
        NSURL *backupStoreUrl = [NSURL fileURLWithPath:dbFileName];
        
        NSManagedObjectModel *model = [[NSPersistentStoreCoordinator MR_defaultStoreCoordinator] managedObjectModel];
        NSPersistentStoreCoordinator *backupPsc = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:model];
        
        NSError *error = nil;
        NSDictionary *options = @{NSSQLitePragmasOption:@{@"journal_mode":@"DELETE"}};
        NSPersistentStore *backupStore = [backupPsc addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:backupStoreUrl options:options error:&error];
        NSLog(@"add backup store: :%@", [error localizedDescription]);
        
        NSPersistentStore *currentStore = [[NSPersistentStoreCoordinator MR_defaultStoreCoordinator] persistentStores][0];
        NSURL *currentStoreUrl = [currentStore URL];
        
        [MagicalRecord cleanUp];
        
        NSLog(@"remove store: :%@", [error localizedDescription]);
        [[NSFileManager defaultManager] removeItemAtPath:[currentStoreUrl path] error:&error];
        NSLog(@"remove file: :%@", [error localizedDescription]);
        
        
        [backupPsc migratePersistentStore:backupStore toURL:currentStoreUrl options:nil withType:NSSQLiteStoreType error:&error];
        NSLog(@"migrate backup store: :%@", [error localizedDescription]);
        
        [NSPersistentStoreCoordinator MR_setDefaultStoreCoordinator:backupPsc];
        
        /* init context */
        [NSManagedObjectContext MR_initializeDefaultContextWithCoordinator:backupPsc];
        
        NSUndoManager *undoManager = [[NSUndoManager alloc] init];
        [[NSManagedObjectContext MR_defaultContext] setUndoManager:undoManager];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPersistentStoreChanged object:nil];
        
    } else if (buttonIndex == 1) {
        MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
        [controller setSubject:@"Backup"];
        
        NSArray *allPaths = NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES);
        NSString *libDirectory = [allPaths objectAtIndex:0];
        NSString *zipName = [_restorePath lastPathComponent];
        NSString *archivesPath = libDirectory;
        NSString *archivePath = [archivesPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.zip", zipName]];
        
        NSURL *archiveUrl = [NSURL fileURLWithPath:archivePath];

        ZZMutableArchive* newArchive = [ZZMutableArchive archiveWithContentsOfURL:archiveUrl];
        
        NSMutableArray *entires = [NSMutableArray array];
        
        NSArray *files = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:_restorePath  error:nil];
        for (NSString *fileName in files) {
            NSString *filePath = [_restorePath stringByAppendingPathComponent:fileName];
            ZZArchiveEntry *ent = [ZZArchiveEntry archiveEntryWithFileName:[NSString stringWithFormat:@"%@", fileName] compress:YES dataBlock:^NSData *(NSError *__autoreleasing *error) {
                NSData *data = [NSData dataWithContentsOfFile:filePath];
                return data;
            }];
            [entires addObject:ent];
        }
        
        [newArchive updateEntries:entires error:nil];
        
        
//
        NSString *message = [NSString stringWithFormat:@"My backup\n\n\n----------------\n"];
        message = [message stringByAppendingString:@"Lockster version: 1.0\n"];
//
        NSString *m = [[UIDevice currentDevice] model];
        message = [message stringByAppendingString:[NSString stringWithFormat:@"\n\n Sent from my %@", m]];
//
//        
        [controller setMessageBody:message isHTML:NO];
        [controller addAttachmentData:[newArchive contents] mimeType:@"application/zip" fileName:[NSString stringWithFormat:@"%@.zip", zipName]];
        [self presentViewController:controller animated:YES completion:nil];
    }
}

- (void)actionSheetCancel:(UIActionSheet *)actionSheet {
    
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    [self becomeFirstResponder];
	[self dismissViewControllerAnimated:YES completion:nil];
}


@end
