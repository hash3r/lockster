//
//  UIColor+RGB.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 17.01.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#ifndef IKeeper_UIColor_RGB_h
#define IKeeper_UIColor_RGB_h

static inline UIColor *UIColorFromRGBA(int rgb, float a) {
    return [UIColor colorWithRed:((float)((rgb & 0xFF0000) >> 16))/255.0
                           green:((float)((rgb & 0xFF00) >> 8))/255.0
                            blue:((float)(rgb & 0xFF))/255.0 alpha:a];
}

static inline UIColor *UIColorFromRGB(int rgb) {
    return UIColorFromRGBA(rgb, 1.f);
}


#endif
