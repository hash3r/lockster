//
//  IKNavigationController.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 16.04.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IKNavigationController : UINavigationController

@end
