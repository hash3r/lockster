//
//  IKAddFieldCell.m
//  IKeeper
//
//  Created by Chebulaev Oleg on 24.04.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import "IKAddFieldCell.h"

@implementation IKAddFieldCell

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        _separator = [[UIView alloc] initWithFrame:CGRectMake(0, 43.5, 320, 0.5)];
        _separator.backgroundColor = [UIColor lightGrayColor];
        [self addSubview:_separator];
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
