//
//  IKSettingsViewController.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 23.12.13.
//  Copyright (c) 2013 Chebulaev Oleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@class IKSettingsViewController;

@protocol IKSettingsViewControllerDelegate
- (void)onDismissViewController:(IKSettingsViewController *)vc;
@end

@interface IKSettingsViewController : UITableViewController

@property (nonatomic, weak) id<IKSettingsViewControllerDelegate> delegate;

- (IBAction)onComplete;
- (IBAction)onChangeTitleSearchOnly:(id)sender;

@end
