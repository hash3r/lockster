//
//  IKFieldCell.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 13.01.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SAMGradientView;

@interface IKFieldCell : UITableViewCell

@property (nonatomic, weak) IBOutlet SAMGradientView *separatorView;
@property (nonatomic, weak) IBOutlet UITextField *fieldField;
@property (nonatomic, weak) IBOutlet UIButton *fieldButton;

@end
