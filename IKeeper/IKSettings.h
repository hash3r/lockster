//
//  IKSettings.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 18.02.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kNotificationWallaperChanged @"WallperChanged"
#define kNotificationTitleSearchOnlyChanged @"TitleSeatchOnlyChanged"
#define kNotificationPersistentStoreChanged @"PersistentStoreChanged"
#define kNotificationRequestAuthAfterChanged @"RequestAuthAfterChanged"
#define kNotificationClearClipboardAfterChanged @"ClearClipboardAfterChanged"
#define kNotificationLockOnExitChanged @"LockOnExitChanged"
#define kNotificationPermanentSeaarchBarChanged @"PermanentSearchBarChanged"

@interface IKSettings : NSObject

+ (IKSettings *)sharedInstance;

- (NSInteger)requestAuthAfter;
- (void)setRequestAuthAfter:(NSInteger)minutes;

- (NSInteger)clearClipboardAfter;
- (void)setClearClipboardAfter:(NSInteger)minutes;

- (BOOL)lockOnExit;
- (void)setLockOnExit:(BOOL)lock;

- (void)resetWallaperPath;
- (NSString *)wallaperPath;
- (void)setWallaperPath:(NSString *)path;

- (BOOL)permanentSearchBar;
- (void)setPermanentSearchBar:(BOOL)permanent;

@property (nonatomic, assign) BOOL titleSearchOnly;

- (void)makeBackup;
- (BOOL)restoreFromBackupWithError:(NSError **)error;

@end
