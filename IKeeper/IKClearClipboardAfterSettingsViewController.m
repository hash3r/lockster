//
//  IKClearClipboardAfterSettingsViewController.m
//  IKeeper
//
//  Created by Chebulaev Oleg on 13.03.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import "IKClearClipboardAfterSettingsViewController.h"
#import <IKSettings.h>

@interface IKClearClipboardAfterSettingsViewController () {
    NSArray *_values;
    NSArray *_labels;
    NSIndexPath* _checkedIndexPath;

}

@end

@implementation IKClearClipboardAfterSettingsViewController

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        _values = @[@(1), @(5), @(10), @(INT_MAX)];
        _labels = @[@"1 min", @"5 min", @"10 min", @"Never"];
        NSInteger requestAuthAfter = [[IKSettings sharedInstance] clearClipboardAfter];
        [_values enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            NSNumber *value = (NSNumber *)obj;
            if ([value integerValue] == requestAuthAfter) {
#ifdef DEBUG
                _checkedIndexPath = [NSIndexPath indexPathForRow:idx inSection:3];
#else
                _checkedIndexPath = [NSIndexPath indexPathForRow:idx inSection:0];
#endif
                *stop = YES;
                return;
            }
        }];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_values count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.textLabel.text = _labels[indexPath.row];
    if([_checkedIndexPath isEqual:indexPath]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(_checkedIndexPath)
    {
        UITableViewCell* uncheckCell = [tableView
                                        cellForRowAtIndexPath:_checkedIndexPath];
        uncheckCell.accessoryType = UITableViewCellAccessoryNone;
    }
    UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
    _checkedIndexPath = indexPath;
    
    NSInteger clearClipboardAfter = [_values[indexPath.row] integerValue];
    [[IKSettings sharedInstance] setClearClipboardAfter:clearClipboardAfter];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
