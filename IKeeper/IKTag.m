//
//  IKTag.m
//  IKeeper
//
//  Created by Chebulaev Oleg on 14.01.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import "IKTag.h"
#import "IKCard.h"


@implementation IKTag

@dynamic name;
@dynamic cardLinks;
@dynamic uniqueIdentifier;
@dynamic listOrder;

- (void)awakeFromInsert
{
    [super awakeFromInsert];
    if (!self.uniqueIdentifier) {
        self.uniqueIdentifier = [[NSProcessInfo processInfo] globallyUniqueString];
    }
    
    IKTag *lastTag = [IKTag MR_findFirstOrderedByAttribute:@"listOrder" ascending:NO inContext:self.managedObjectContext];
    NSInteger max = [lastTag.listOrder integerValue];
    NSInteger order = max + 1;
    self.listOrder = [NSNumber numberWithInteger:order];
}



+ (IKTag *)tagByName:(NSString *)name {
    IKTag *tag = [IKTag MR_findFirstByAttribute:@"name" withValue:name];
    return tag;
}


+ (NSArray *)allTagTitles {
    NSArray *allTags = [IKTag MR_findAllSortedBy:@"name" ascending:YES];
    NSMutableArray *tagTitles = [NSMutableArray array];
    for (IKTag *t in allTags) {
        NSString *name = [t name];
        if (name) {
            [tagTitles addObject:name];
        }
    }
    return [NSArray arrayWithArray:tagTitles];
}

@end
