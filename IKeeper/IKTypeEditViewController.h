//
//  IKTypeEditViewController.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 15.01.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@class IKBaseField, IKCard, IKCardTemplate;

@protocol IKTypeEditViewControllerDelegate
- (void)onCancelEditField:(IKBaseField *)field viewController:(UIViewController *)vc;
- (void)onSaveEditField:(IKBaseField *)field viewController:(UIViewController *)vc;
@end

@interface IKTypeEditViewController : UITableViewController

#pragma mark - Properties
@property (nonatomic, weak) id<IKTypeEditViewControllerDelegate> delegate;
@property (nonatomic, strong) IKBaseField *field;
@property (nonatomic, strong) IKCard *card;
@property (nonatomic, strong) IKCardTemplate *cardTemplate;

//@property (nonatomic, assign) NSInteger maxFieldsListOrder;

//@property (nonatomic, strong) IBOutlet UITextField *fieldNameTextField;
//@property (nonatomic, strong) IBOutlet UISwitch *showInMainViewSwitch;
//@property (nonatomic, strong) IBOutlet UIButton *typeBtn;
//@property (nonatomic, strong) IBOutlet UILabel *showMainViewLabel;
//@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;

//@property (nonatomic, assign) BOOL templateFieldType;

#pragma mark - Event Handlers
- (void)onCancelTypeBtnTapped:(id)sender;
- (void)onSaveTypeBtnTapped:(id)sender;
- (IBAction)onChageShowInMainViewValue:(id)sender;
@end
