//
//  IKIconColorViewController.m
//  IKeeper
//
//  Created by Chebulaev Oleg on 13.05.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import "IKIconColorViewController.h"
#import <IKColorCell.h>
#import <UIImage+RTTint.h>

@interface IKIconColorViewController ()

@end

@implementation IKIconColorViewController

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        _colors = [NSArray arrayWithObjects:UIColorFromRGB(0x272727),
                   UIColorFromRGB(0x3f3f3f), UIColorFromRGB(0x797979), UIColorFromRGB(0x999999), UIColorFromRGB(0xff0000),UIColorFromRGB(0xff3300),UIColorFromRGB(0xff6600),UIColorFromRGB(0xff9900),UIColorFromRGB(0xffcc00),UIColorFromRGB(0xffff00),UIColorFromRGB(0xcccc33),UIColorFromRGB(0x99cc33),UIColorFromRGB(0x66cc33),UIColorFromRGB(0x00cc33),UIColorFromRGB(0x33cc99),UIColorFromRGB(0x33cccc),UIColorFromRGB(0x66ccff),UIColorFromRGB(0x3399ff),UIColorFromRGB(0x3366ff),UIColorFromRGB(0x0033ff),UIColorFromRGB(0x3333cc),UIColorFromRGB(0x6600ff),UIColorFromRGB(0x9900ff),UIColorFromRGB(0xcc00ff),UIColorFromRGB(0xff00cc),UIColorFromRGB(0xcc3399),UIColorFromRGB(0xff0066),UIColorFromRGB(0xcc0033),nil];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section {
    return [_colors count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    IKColorCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ColorCell" forIndexPath:indexPath];
    
    UIImage *iconImage = [UIImage imageNamed:_iconName];
    UIColor *color = _colors[indexPath.row];
    UIImage *tintedImage = [iconImage rt_tintedImageWithColor:color];
    
    cell.iconImageView.image = tintedImage;
    return cell;
}

#pragma mark – UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGSize retval = CGSizeMake(30, 30);
    return retval;
}

//- (UIEdgeInsets)collectionView:
//(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
//    return UIEdgeInsetsMake(50, 20, 50, 20);
//}

#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView
didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    UIImage *iconImage = [UIImage imageNamed:_iconName];
    UIColor *color = _colors[indexPath.row];
    [_delegate onSelectIcon:iconImage iconName:_iconName color:color inViewcontroller:self];
}


@end
