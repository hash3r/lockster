//
//  IKUIApplication.m
//  IKeeper
//
//  Created by Chebulaev Oleg on 13.03.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import "IKUIApplication.h"
#import "IKSettings.h"

@implementation IKUIApplication

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self addObservers];
    }
    return self;
}

- (void)addObservers {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resetIdleTimer) name:kNotificationRequestAuthAfterChanged object:nil];
}

- (void)removeObservers {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationRequestAuthAfterChanged object:nil];
}

- (void)sendEvent:(UIEvent *)event {
	[super sendEvent:event];
    
	if(!_lockTimer) {
		[self resetIdleTimer];
	}
    
    NSSet *allTouches = [event allTouches];
    if ([allTouches count] > 0) {
        UITouchPhase phase = ((UITouch *)[allTouches anyObject]).phase;
        if (phase == UITouchPhaseBegan) {
            [self resetIdleTimer];
		}
    }
}

- (void)resetIdleTimer
{
    if (_lockTimer) {
        [_lockTimer invalidate];
        _lockTimer = nil;
    }
    
    NSInteger requestAfter = [[IKSettings sharedInstance] requestAuthAfter];
    if (requestAfter == INT_MAX) {
        return;
    }
	int timeout = requestAfter * 60;
    _lockTimer = [NSTimer scheduledTimerWithTimeInterval:timeout
                                                   target:self
                                                 selector:@selector(idleTimerExceeded)
                                                 userInfo:nil
                                                  repeats:NO];
}

- (void)idleTimerExceeded {
	/* Post a notification so anyone who subscribes to it can be notified when
	 * the application times out */
	[[NSNotificationCenter defaultCenter]
	 postNotificationName:kApplicationDidIdleNotification object:nil];
}

- (void) dealloc {
    [self removeObservers];
	[_lockTimer invalidate];
    _lockTimer = nil;
}
@end
