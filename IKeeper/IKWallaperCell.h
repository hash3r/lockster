//
//  IKWallaperCell.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 18.02.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IKWallaperCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet UIImageView *wallaperImageView;
@property (nonatomic, weak) IBOutlet UIImageView *checkImageView;

@end
