//
//  IKWallaperSettingsViewController.m
//  IKeeper
//
//  Created by Chebulaev Oleg on 18.02.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import "IKWallaperSettingsViewController.h"
#import "IKSettings.h"
#import "IKWallapersListViewController.h"
#import "IKWallaperChooseCell.h"
#import <BlocksKit/UIView+BlocksKit.h>
#import "IKWallaperShowViewController.h"

@interface IKWallaperSettingsViewController () <IKWallaperShowViewControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@end

@implementation IKWallaperSettingsViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewWillAppear:(BOOL)animated {
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    if (indexPath.row == 0) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        cell.textLabel.text = @"Choose preinstalled";
        cell.textLabel.font = [UIFont systemFontOfSize:17];
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    } else if (indexPath.row == 1) {
        IKWallaperChooseCell *cell = [tableView dequeueReusableCellWithIdentifier:@"WallapersCell"];
        NSString *screnPath1 = [[NSBundle mainBundle] pathForResource:@"screen1" ofType:@"jpg"];
        NSString *currentPath = [[IKSettings sharedInstance] wallaperPath];
        
        [cell.screen1 setImage:[UIImage imageWithContentsOfFile:screnPath1] forState:UIControlStateNormal];

        if ([screnPath1 isEqualToString:currentPath]) {
            cell.chek1.hidden = NO;
        } else {
            cell.chek1.hidden = YES;
        }
        
        NSString *screnPath2 = [[NSBundle mainBundle] pathForResource:@"screen2" ofType:@"jpg"];
        [cell.screen2 setImage:[UIImage imageWithContentsOfFile:screnPath2] forState:UIControlStateNormal];
        if ([screnPath2 isEqualToString:currentPath]) {
            cell.chek2.hidden = NO;
        } else {
            cell.chek2.hidden = YES;
        }
        
        NSString *screnPath3 = [[NSBundle mainBundle] pathForResource:@"screen3" ofType:@"jpg"];
        [cell.screen3 setImage:[UIImage imageWithContentsOfFile:screnPath3] forState:UIControlStateNormal];
        if ([screnPath3 isEqualToString:currentPath]) {
            cell.chek3.hidden = NO;
        } else {
            cell.chek3.hidden = YES;
        }
        return cell;
    } else if (indexPath.row == 2) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        cell.textLabel.text = @"Photo Gallery";
        cell.textLabel.font = [UIFont systemFontOfSize:17];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        return cell;
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 0) {
//        [[IKSettings sharedInstance] resetWallaperPath];
//        [tableView reloadSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, 1)] withRowAnimation:UITableViewRowAnimationAutomatic];
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success reset wallaper to default" message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//        [alert show];
    } else if (indexPath.row == 1) {

//        IKWallapersListViewController *vc = [[UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil] instantiateViewControllerWithIdentifier:@"WallapersList"];
//        [self.navigationController pushViewController:vc animated:YES];
    } else if (indexPath.row == 2) {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:picker animated:YES completion:NULL];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [picker setNavigationBarHidden:NO];
    [picker.navigationBar setTranslucent:NO];
    
    UIStoryboard *s = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    IKWallaperShowViewController *vc = [s instantiateViewControllerWithIdentifier:@"BackShow"];
    vc.previewImage = chosenImage;
    vc.delegate = self;
    [picker pushViewController:vc animated:YES];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 1){
        return 192;
    }
    return 44.0;
}

- (void)onDissmissViewController:(IKWallaperShowViewController *)vc {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)onScreen1Tapped:(id)sender {
    UIStoryboard *s = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    IKWallaperShowViewController *vc = [s instantiateViewControllerWithIdentifier:@"BackShow"];
    NSString *screnPath1 = [[NSBundle mainBundle] pathForResource:@"screen1" ofType:@"jpg"];
    vc.previewImagePath = screnPath1;
    vc.delegate = self;
    [self presentViewController:vc animated:YES completion:nil];
}

- (IBAction)onScreen2Tapped:(id)sender {
    UIStoryboard *s = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    IKWallaperShowViewController *vc = [s instantiateViewControllerWithIdentifier:@"BackShow"];
    NSString *screnPath1 = [[NSBundle mainBundle] pathForResource:@"screen2" ofType:@"jpg"];
    vc.previewImagePath = screnPath1;
    vc.delegate = self;
    [self presentViewController:vc animated:YES completion:nil];
}

- (IBAction)onScreen3Tapped:(id)sender {
    UIStoryboard *s = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    IKWallaperShowViewController *vc = [s instantiateViewControllerWithIdentifier:@"BackShow"];
    NSString *screnPath1 = [[NSBundle mainBundle] pathForResource:@"screen3" ofType:@"jpg"];
    vc.previewImagePath = screnPath1;
    vc.delegate = self;
    [self presentViewController:vc animated:YES completion:nil];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

@end
