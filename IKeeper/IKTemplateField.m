//
//  IKTemplateField.m
//  IKeeper
//
//  Created by Chebulaev Oleg on 06.01.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import "IKTemplateField.h"
#import "IKCardTemplate.h"


@implementation IKTemplateField

@dynamic cardTemplate;

@end
