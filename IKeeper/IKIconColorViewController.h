//
//  IKIconColorViewController.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 13.05.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <IKIconsListViewController.h>

@interface IKIconColorViewController : UICollectionViewController {
    NSString *_iconName;
    NSArray *_colors;
    __weak id<IKIconsListViewControllerDelegate>_delegate;
}

@property (nonatomic, weak) id <IKIconsListViewControllerDelegate> delegate;
@property (nonatomic, strong) NSString *iconName;



@end
