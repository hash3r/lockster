//
//  IKServiceCell.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 03.04.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IKServiceCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *syncServiceName;
@property (nonatomic, weak) IBOutlet UISwitch *syncSwitch;
@property (nonatomic, weak) IBOutlet UILabel *syncLabel;

@end
