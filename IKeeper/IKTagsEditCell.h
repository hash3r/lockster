//
//  IKTagsEditCell.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 06.05.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IKTagsEditCell : UITableViewCell


@property (nonatomic, weak) IBOutlet UITextField *textField;

@end
