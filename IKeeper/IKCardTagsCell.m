//
//  IKCardTagsCell.m
//  IKeeper
//
//  Created by Chebulaev Oleg on 24.01.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import "IKCardTagsCell.h"

@implementation IKCardTagsCell

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        _v = [[UIView alloc] initWithFrame:CGRectMake(35, 0, 320, 0.5)];
        _v.backgroundColor = [UIColor lightGrayColor];
        [self addSubview:_v];
    }
    return self;
}

- (void)awakeFromNib {
//    _tagView.allowToUseSingleSpace = YES;
//    [_tagView setBackgroundColor:UIColorFromRGB(0x16457d)];
    [[HKKTagWriteView appearance] setTagBackgroundColor:[UIColor colorWithRed:0.36 green:0.55 blue:0.77 alpha:1.0]];
    [[HKKTagWriteView appearance] setFont:[UIFont systemFontOfSize:14.0]];
    for (NSLayoutConstraint *c in self.contentView.constraints) {
        if (c.firstAttribute == NSLayoutAttributeLeading) {
            _leadingContraint = c;
        }
        if (c.firstAttribute == NSLayoutAttributeTrailing) {
            _trailingContraint = c;
        }
    }
}


- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
    [super setEditing:editing animated:animated];
    
    
//    self.tokenFieldView.tokenField.editable = editing;
    
    
    if (editing) {
        _v.hidden = YES;
        [self.tagView setStartOffset:43.0 animated:animated];
        self.separatorInset = UIEdgeInsetsMake(0, 43, 0, 0);
        [UIView animateWithDuration:0.3 animations:^{
            _leadingContraint.constant = 0;
            _trailingContraint.constant = 0;
//            [self layoutIfNeeded];
        } completion:^(BOOL finished) {
//            [self.tokenFieldView.tokenField layoutTokensAnimated:YES];
        }];
    } else {
        _v.hidden = NO;
        [self.tagView setStartOffset:35.0 animated:animated];
        self.separatorInset = UIEdgeInsetsMake(0, 35, 0, 0);
        [UIView animateWithDuration:0.3 animations:^{
            _leadingContraint.constant = 0;
            _trailingContraint.constant = 0;
//            [self layoutIfNeeded];
        } completion:^(BOOL finished) {
//            [self.tokenFieldView.tokenField layoutTokensAnimated:YES];
        }];
    }
    [self.tagView setEditing:editing];

}
@end
