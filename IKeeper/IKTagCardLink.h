//
//  IKTagCardLink.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 24.01.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class IKCard, IKTag;

@interface IKTagCardLink : NSManagedObject

@property (nonatomic, retain) IKTag *tag;
@property (nonatomic, retain) IKCard *card;
@property (nonatomic, retain) NSNumber *listOrder;
@property (nonatomic, retain) NSString *uniqueIdentifier;

@end
