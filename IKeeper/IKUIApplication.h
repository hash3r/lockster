//
//  IKUIApplication.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 13.03.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kApplicationDidIdleNotification @"kApplicationDidIdleNotification"

@interface IKUIApplication : UIApplication {
    NSTimer *_lockTimer;
}
- (void)resetIdleTimer;

@end
