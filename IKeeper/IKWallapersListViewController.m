//
//  IKWallapersListViewController.m
//  IKeeper
//
//  Created by Chebulaev Oleg on 18.02.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import "IKWallapersListViewController.h"
#import "IKSettings.h"
#import "IKWallaperCell.h"
#import <UIImage+Additions/UIImage+Additions.h>
#import <BDBAttributedButton.h>
#import "IKWallaperShowViewController.h"

@interface IKWallapersListViewController () <IKWallaperShowViewControllerDelegate,
                                             UINavigationControllerDelegate, UIImagePickerControllerDelegate>
{
    NSMutableArray *_wallapersPaths;
}

@end

@implementation IKWallapersListViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    _wallapersPaths = [NSMutableArray array];
    
    for (int i = 1; i < 5; i++) {
        NSString *name = [NSString stringWithFormat:@"screen%d", i];
        NSString *path = [[NSBundle mainBundle] pathForResource:name ofType:@"jpg"];
        [_wallapersPaths addObject:path];
    }
}

- (IBAction)onChoosePhoto:(id)sender {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [picker setNavigationBarHidden:NO];
    [picker.navigationBar setTranslucent:NO];
    
    UIStoryboard *s = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    IKWallaperShowViewController *vc = [s instantiateViewControllerWithIdentifier:@"BackShow"];
    vc.previewImage = chosenImage;
    vc.delegate = self;
    [picker pushViewController:vc animated:YES];
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section {
    return [_wallapersPaths count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    IKWallaperCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    NSString *iconPath = _wallapersPaths[indexPath.row];
    
    NSString *current = [[IKSettings sharedInstance] wallaperPath];
    if ([current isEqualToString:iconPath]) {
        cell.checkImageView.hidden = NO;
    } else {
        cell.checkImageView.hidden = YES;
    }
    
    UIImage *iconImage = [UIImage imageWithContentsOfFile:iconPath];
    cell.wallaperImageView.image = iconImage;
    return cell;
}

#pragma mark – UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGSize retval = CGSizeMake(91, 162);
    return retval;
}

#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView
didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *imageName = _wallapersPaths[indexPath.row];
    UIStoryboard *s = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    IKWallaperShowViewController *vc = [s instantiateViewControllerWithIdentifier:@"BackShow"];
    vc.previewImagePath = imageName;
    vc.delegate = self;
    [self presentViewController:vc animated:YES completion:nil];
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    UICollectionReusableView *v = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"View" forIndexPath:indexPath];
    return v;
}

- (void)onDissmissViewController:(IKWallaperShowViewController *)vc {
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.collectionView reloadData];
}


@end
