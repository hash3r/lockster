//
//  IKTagCardLink.m
//  IKeeper
//
//  Created by Chebulaev Oleg on 24.01.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import "IKTagCardLink.h"
#import "IKCard.h"
#import "IKTag.h"


@implementation IKTagCardLink

@dynamic tag;
@dynamic card;
@dynamic listOrder;
@dynamic uniqueIdentifier;

- (void)awakeFromInsert
{
    [super awakeFromInsert];
    if (!self.uniqueIdentifier) {
        self.uniqueIdentifier = [[NSProcessInfo processInfo] globallyUniqueString];
    }
}

@end
