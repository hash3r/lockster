//
//  IKCardTemplate.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 06.01.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class IKCard, IKTemplateField;

@interface IKCardTemplate : NSManagedObject

@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *imageName;
@property (nonatomic, retain) id imageColor;
@property (nonatomic, retain) NSNumber *listOrder;
@property (nonatomic, retain) NSSet *fields;
@property (nonatomic, retain) NSSet *cards;
@property (nonatomic, retain) NSString *uniqueIdentifier;

- (UIImage *)image;


#pragma mark - Static Methods
+ (void)seed;
+ (IKCardTemplate *)buildEmptyTemplate;

#pragma mark - Methods
- (BOOL)isValidModel:(NSError **)error;
- (NSArray *)orderedCustomFields;
- (IKTemplateField *)noteField;
- (IKTemplateField *)descriptionField;
- (NSInteger)maxListOrder;
- (IKTemplateField *)createField;

@end

@interface IKCardTemplate (CoreDataGeneratedAccessors)
- (void)addFieldsObject:(IKTemplateField *)value;
- (void)removeFieldsObject:(IKTemplateField *)value;
- (void)addFields:(NSSet *)values;
- (void)removeFields:(NSSet *)values;

- (void)addCardsObject:(IKCard *)value;
- (void)removeCardsObject:(IKCard *)value;
- (void)addCards:(NSSet *)values;
- (void)removeCards:(NSSet *)values;

@end
