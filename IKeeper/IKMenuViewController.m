//
//  IKMenuViewController.m
//  IKeeper
//
//  Created by Chebulaev Oleg on 06.01.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import "IKMenuViewController.h"
#import "IKMenuCell.h"
#import "IKCardTemplate.h"
#import <UIImage+RTTint/UIImage+RTTint.h>
#import <UIImage+BlurredFrame.h>
#import <MMDrawerController/UIViewController+MMDrawerController.h>
#import "IKMainViewController.h"
#import "IKSettingsViewController.h"
#import "IKSettings.h"
#import <LKbadgeView/LKBadgeView.h>
#import "IKCard.h"
#import <IDMSyncManager.h>
#import <MTDates/NSDate+MTDates.h>
#import <UIImage+Resize.h>

@interface IKMenuViewController () <NSFetchedResultsControllerDelegate, IKSettingsViewControllerDelegate, UITableViewDelegate> {
    NSFetchedResultsController *_fetchController;
    id syncDidEndNotif, userDefaultsUpdateNotif, wallaperChangeNotif, storeChangeNotif;
    NSIndexPath *_selectedIndexPath;
    BOOL _selectTop;
}

@end

@implementation IKMenuViewController

- (id)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self addSettingsObservers];
        [self addSyncObservers];
    }
    return self;
}

- (void)dealloc {
    [self removeObservers];
}

- (void)removeObservers {
    [[NSNotificationCenter defaultCenter] removeObserver:syncDidEndNotif];
    [[NSNotificationCenter defaultCenter] removeObserver:userDefaultsUpdateNotif];
    [[NSNotificationCenter defaultCenter] removeObserver:wallaperChangeNotif];
    [[NSNotificationCenter defaultCenter] removeObserver:storeChangeNotif];
}

- (void)addSettingsObservers {
    wallaperChangeNotif = [[NSNotificationCenter defaultCenter] addObserverForName:kNotificationWallaperChanged object:nil queue:nil usingBlock:^(NSNotification *note) {
        [self onWallaperChange:note];
    }];
    
    storeChangeNotif = [[NSNotificationCenter defaultCenter] addObserverForName:kNotificationPersistentStoreChanged object:nil queue:nil usingBlock:^(NSNotification *note) {
        [self onChangeStore:note];
    }];
}

- (void)addSyncObservers {
    __weak typeof(self) weakSelf = self;
    syncDidEndNotif = [[NSNotificationCenter defaultCenter] addObserverForName:IDMSyncActivityDidEndNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        __strong typeof(self) strongSelf = weakSelf;
        if (!strongSelf) return;
        [strongSelf updateSyncLabel];
    }];
    userDefaultsUpdateNotif = [[NSNotificationCenter defaultCenter] addObserverForName:NSUserDefaultsDidChangeNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        __strong typeof(self) strongSelf = weakSelf;
        if (!strongSelf) return;
        [strongSelf updateSyncLabel];
    }];
    
}

- (void)updateSyncLabel {
    NSDate *syncDate = [[IDMSyncManager sharedSyncManager] lastSyncDate];
    if (syncDate) {
        NSString *value = [syncDate mt_stringFromDateWithFormat:@"HH:mm:ss | YYYY.MM.dd" localized:NO];
        self.syncDateLabel.text = [NSString stringWithFormat:@"Last Sync %@", value];
    } else {
        self.syncDateLabel.text = @"No Sync";
        
    }
    [self.syncDateLabel layoutIfNeeded];
    [self.icloudImage layoutIfNeeded];
}

- (void)onWallaperChange:(NSNotification *)notofication {
    [self setBackgroundWallaperFromSettings];
}

- (void)onChangeStore:(NSNotification *)notofication {
    [self reloadData];
}

- (void)setBackgroundWallaperFromSettings {
    NSString *wallaperPath = [[IKSettings sharedInstance] wallaperPath];
    UIImage *image = [UIImage imageWithContentsOfFile:wallaperPath];
    UIColor *tintColor = [UIColor colorWithWhite:0.0 alpha:0.1];

    image = [image applyBlurWithRadius:10.0 tintColor:tintColor saturationDeltaFactor:1.8 maskImage:nil atFrame:CGRectMake(0, 0, image.size.width, image.size.height)];

    self.backgroundImageView.image = image;
}


- (void)viewDidLoad
{
    [super viewDidLoad];

//    CGSize offset = CGSizeMake(-1, 0.6);
//    self.appNameLabel.shadowOffset = offset;
//    self.versionLabel.shadowOffset = offset;
//    
//    
//    self.infoButton.layer.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.75].CGColor;
//    self.infoButton.layer.shadowOpacity = 1.0;
//    self.infoButton.layer.shadowRadius = 1;
//    self.infoButton.layer.shadowOffset = offset;
//    
//    
//    self.appImageView.layer.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.75].CGColor;
//    self.appImageView.layer.shadowOpacity = 1.0;
//    self.appImageView.layer.shadowRadius = 1;
//    self.appImageView.layer.shadowOffset = offset;
    
    UIView *br1 = [[UIView alloc] initWithFrame:CGRectMake(0, 143.0, 320, 1.0)];
    br1.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.2];
    [self.view addSubview:br1];
    
    UIView *br2 = [[UIView alloc] initWithFrame:CGRectMake(0, 144.0, 320, 1.0)];
    br2.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.2];
    [self.view addSubview:br2];
    
    UIView *br3 = [[UIView alloc] initWithFrame:CGRectMake(0, 244.0, 320, 0.5)];
    br3.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.45];
    [self.view addSubview:br3];
    
    UIView *br4 = [[UIView alloc] initWithFrame:CGRectMake(0, 244.5, 320, 0.5)];
    br4.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.3];
    [self.view addSubview:br4];
    
    [self setBackgroundWallaperFromSettings];
    
    self.categoriesTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [self updateSyncLabel];
    
    self.categoriesTableView.scrollsToTop = NO;
    self.topTableView.scrollsToTop = NO;
    self.categoriesTableView.allowsSelection = YES;
    self.topTableView.allowsSelection = YES;
    
    NSMutableAttributedString *s = [[NSMutableAttributedString alloc] initWithString:@"LOCKSTER" attributes:@{}];
    [s addAttribute:NSFontAttributeName value:[IKFonts openSansRegularWithSize:25.0] range:NSMakeRange(0, 4)];
        [s addAttribute:NSFontAttributeName value:[IKFonts openSansLightWithSize:25.0] range:NSMakeRange(4, 4)];
    self.appNameLabel.attributedText = s;
}

- (void)viewWillAppear:(BOOL)animated {
    [self reloadData];
}

- (void)setStatusBarHidden:(BOOL)statusBarHidden {
    _statusBarHidden = statusBarHidden;
}


- (void)reloadData {
    _fetchController = [IKCardTemplate MR_fetchAllGroupedBy:nil withPredicate:nil sortedBy:@"listOrder" ascending:YES delegate:self inContext:[NSManagedObjectContext MR_defaultContext]];
    [self.categoriesTableView reloadData];
    [self.topTableView reloadData];
}

#pragma mark - EventHandlers
- (IBAction)onSettingsBtnTapped:(id)sender {
    [[IDMSyncManager sharedSyncManager] synchronizeWithCompletion:NULL];
    UIStoryboard *s = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    UINavigationController *navVc = [s instantiateViewControllerWithIdentifier:@"NavSettings"];
    IKSettingsViewController *vc = (IKSettingsViewController *)[navVc topViewController];
    vc.delegate = self;
    self.lastStyle = [[UIApplication sharedApplication] statusBarStyle];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    [self presentViewController:navVc animated:YES completion:nil];
}

- (void)onAllCardsBtnTapped:(id)sender {
    [[IDMSyncManager sharedSyncManager] synchronizeWithCompletion:NULL];
    UINavigationController *navVc = (UINavigationController *)[self.mm_drawerController centerViewController];
    IKMainViewController *mainVc = (IKMainViewController *)[navVc topViewController];
    mainVc.mode = CardsFilterModeAll;
    [self.mm_drawerController closeDrawerAnimated:YES completion:nil];
}

- (void)onFavoritesCardsBtnTapped:(id)sender {
    [[IDMSyncManager sharedSyncManager] synchronizeWithCompletion:NULL];
    UINavigationController *navVc = (UINavigationController *)[self.mm_drawerController centerViewController];
    IKMainViewController *mainVc = (IKMainViewController *)[navVc topViewController];
    mainVc.mode = CardsFilterModeFavorites;
    [self.mm_drawerController closeDrawerAnimated:YES completion:nil];
}

- (void)onDismissViewController:(IKSettingsViewController *)vc {
    [[UIApplication sharedApplication] setStatusBarStyle:self.lastStyle];
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Table view data source

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (tableView == self.topTableView) {
        return 1;
    } else {
        return [[_fetchController sections] count];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.topTableView) {
        return 2;
    } else {
        if ([[_fetchController sections] count] > 0) {
            id <NSFetchedResultsSectionInfo> sectionInfo = [[_fetchController sections] objectAtIndex:section];
            return [sectionInfo numberOfObjects];
        } else
            return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    IKMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];

    cell.countLabel.horizontalAlignment = LKBadgeViewHorizontalAlignmentCenter;
    cell.countLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:15.0];
    cell.countLabel.badgeColor = UIColorFromRGBA(0x150e0e, 0.5);
    cell.countLabel.textColor = UIColorFromRGB(0xffffff);
    cell.countLabel.backgroundColor = [UIColor clearColor];
    cell.countLabel.layer.cornerRadius = 10.5;
    cell.countLabel.textOffset = CGSizeMake(0.5, -0.5);
    
    cell.iconImageView.layer.opacity = 0.75;
    cell.titleLabel.layer.opacity  = 0.75;
    cell.countLabel.layer.opacity = 0.75;
    
    if (tableView ==  self.topTableView) {
        [self configureTopCell:cell atIndexPath:indexPath];
    } else {
        [self configureCell:cell atIndexPath:indexPath];
    }
    
    if (tableView == self.topTableView && indexPath.row == _selectedIndexPath.row && _selectedIndexPath.section == 0) {
        [self.topTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:_selectedIndexPath.row inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
    }
    
    if (tableView == self.categoriesTableView && indexPath.row == _selectedIndexPath.row && _selectedIndexPath.section == 1) {
        [self.categoriesTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:_selectedIndexPath.row inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
    }
    
    return cell;
}

- (void)configureTopCell:(IKMenuCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        cell.titleLabel.text = @"All Cards";
        cell.iconImageView.image = [[UIImage imageNamed:@"all_cards"] rt_tintedImageWithColor:[UIColor whiteColor]];
        NSInteger count = [[IKCard MR_findAll] count];
        cell.countLabel.text = [NSString stringWithFormat:@"%d", count];

    } else {
        cell.titleLabel.text = @"Favorites";
        cell.iconImageView.image = [[UIImage imageNamed:@"favorite_cards"] rt_tintedImageWithColor:[UIColor whiteColor]];
        NSPredicate *p = [NSPredicate predicateWithFormat:@"isFavorite == %@", [NSNumber numberWithBool:YES]];
        NSInteger countFav = [IKCard MR_countOfEntitiesWithPredicate:p];
        cell.countLabel.text = [NSString stringWithFormat:@"%d", countFav];
    }
}

- (void)configureCell:(IKMenuCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    IKCardTemplate *t = [_fetchController objectAtIndexPath:indexPath];
    cell.titleLabel.text = t.name;

    UIImage *image = [t image];
    image = [image rt_tintedImageWithColor:[UIColor whiteColor]];
    image = [image resizedImageToSize:CGSizeMake(27, 27)];
    cell.iconImageView.image = image;
    
    NSInteger cardsCount = [[t cards] count];
    cell.countLabel.text = [NSString stringWithFormat:@"%d", cardsCount];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 49;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [[IDMSyncManager sharedSyncManager] synchronizeWithCompletion:NULL];
    NSInteger newSection = 0;
    if (tableView == self.topTableView) {
        newSection = 0;
    } else {
        newSection = 1;
    }
    NSInteger newRow = indexPath.row;
    
    if (newSection != _selectedIndexPath.row) {
        [self deselectAtIndexPath:_selectedIndexPath];
    }
    
    _selectedIndexPath = [NSIndexPath indexPathForRow:newRow inSection:newSection];
    
    
    if (tableView == self.topTableView) {
        if (indexPath.row == 0) {
            [self onAllCardsBtnTapped:nil];
        }
        if (indexPath.row == 1) {
            [self onFavoritesCardsBtnTapped:nil];
        }
        return;
    }
    UINavigationController *navVc = (UINavigationController *)[self.mm_drawerController centerViewController];
    IKMainViewController *mainVc = (IKMainViewController *)[navVc topViewController];
    IKCardTemplate *t = [_fetchController objectAtIndexPath:indexPath];
    mainVc.cardTemplate = t;
    mainVc.mode = CardsFilterModeCustom;
    [self.mm_drawerController closeDrawerAnimated:YES completion:nil];
}

- (void)deselectAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        [self.topTableView deselectRowAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row inSection:0] animated:YES];
    } else {
        [self.categoriesTableView deselectRowAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row inSection:0] animated:YES];
    }
}

#pragma mark - NSFetchedResultsControllerDelegate
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.categoriesTableView beginUpdates];
}


- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.categoriesTableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.categoriesTableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    UITableView *tableView = self.categoriesTableView;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:(IKMenuCell *)[tableView cellForRowAtIndexPath:indexPath]
                    atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.categoriesTableView endUpdates];
}

@end
