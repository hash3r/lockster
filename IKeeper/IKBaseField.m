//
//  IKBaseField.m
//  IKeeper
//
//  Created by Chebulaev Oleg on 06.01.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import "IKBaseField.h"
#import "IKFieldType.h"


@implementation IKBaseField

@dynamic name;
@dynamic type;
@dynamic listOrder;
@dynamic uniqueIdentifier;

- (void)awakeFromInsert
{
    [super awakeFromInsert];
    if (!self.uniqueIdentifier) {
        self.uniqueIdentifier = [[NSProcessInfo processInfo] globallyUniqueString];
    }
}

- (BOOL)isDescriptionField
{
    return [self.name isEqualToString:@"Description"];
}

- (BOOL)isNoteField
{
    return [self.name isEqualToString:@"Note"];
}

-(BOOL)isValidModel:(NSError **)error
{
	
    NSString *val = [self name];

    if (val == nil || [val length] == 0) {
        NSDictionary *userInfoDict = [NSDictionary
                                      dictionaryWithObject:@"Name is required field"
                                      forKey:NSLocalizedDescriptionKey];
        *error = [[NSError alloc] initWithDomain:
                   NSCocoaErrorDomain code:-1 userInfo:userInfoDict];

        
        return NO;
    }
    
    IKFieldType *val2 = [self type];
    
    if (val2 == nil) {
        NSDictionary *userInfoDict = [NSDictionary
                                      dictionaryWithObject:@"Field type is required field"
                                      forKey:NSLocalizedDescriptionKey];
        *error = [[NSError alloc] initWithDomain:
                  NSCocoaErrorDomain code:-1 userInfo:userInfoDict];
        
        
        return NO;
    }
    
    return YES;
}

@end
