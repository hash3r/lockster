//
//  IKTemplateEditViewController.m
//  IKeeper
//
//  Created by Chebulaev Oleg on 01.02.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import "IKTemplateEditViewController.h"
#import <MMDrawerController/UIViewController+MMDrawerController.h>
#import "IKCardTemplate.h"
#import <BlocksKit/UIActionSheet+BlocksKit.h>
#import "IKTemplateFieldCell.h"
#import "IKTemplateDescriptionCell.h"
#import <UIImage+Resize.h>
#import <UIImage+RTTint.h>
#import "IKTemplateField.h"
#import "IKTypeEditViewController.h"
#import <BlocksKit/UIAlertView+BlocksKit.h>
#import "IKIconsListViewController.h"

@interface IKTemplateEditViewController () <IKTypeEditViewControllerDelegate, IKIconsListViewControllerDelegate, UITextFieldDelegate>{
    NSUndoManager *_undoManager;
    NSMutableArray *_customFields;
}

@property (nonatomic, strong) NSIndexPath *editingIndexPath;

@end

@implementation IKTemplateEditViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIView *topview = [[UIView alloc] initWithFrame:CGRectMake(0, -520, 320, 520)];
    topview.backgroundColor = [UIColor whiteColor];
    [self.tableView addSubview:topview];
    
    [self.tableView  setEditing:YES animated:YES];

//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(keyboardWillShow:)
//                                                 name:UIKeyboardWillShowNotification
//                                               object:nil];
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(keyboardWillHide:)
//                                                 name:UIKeyboardWillHideNotification
//                                               object:nil];

    
    _undoManager = [[NSManagedObjectContext MR_defaultContext] undoManager];
    [_undoManager beginUndoGrouping];
    
    if (!self.cardTemplate) {
        self.cardTemplate = [IKCardTemplate buildEmptyTemplate];
        [self setEmptyViewToFooter];
        self.title = @"New Card Template";
    } else {
        [self setRemoveButtonToFooter];
        self.title = @"Edit Card Template";
    }
    
    _customFields = [[self.cardTemplate orderedCustomFields] mutableCopy];
    
    [self.tableView reloadData];
}

- (void)setRemoveButtonToFooter {
    UIButton *removeCardButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [removeCardButton setFrame:CGRectMake(-1, 36, 322, 41)];
    removeCardButton.layer.borderWidth = 0.5;
    removeCardButton.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    [removeCardButton addTarget:self action:@selector(onRemoveTemplateTapped:) forControlEvents:UIControlEventTouchUpInside];
    [removeCardButton setTitle:@"Delete Card Template" forState:UIControlStateNormal];
    [removeCardButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [removeCardButton setBackgroundColor:[UIColor whiteColor]];
    removeCardButton.titleLabel.font = [UIFont systemFontOfSize:17.0];
    
    UIView *buttonsContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 160)];
    [buttonsContainer addSubview:removeCardButton];
    buttonsContainer.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.tableView.tableFooterView = buttonsContainer;
    self.tableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
}

- (void)setEmptyViewToFooter {
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 1)];
    self.tableView.tableFooterView = v;
    self.tableView.backgroundColor = [UIColor whiteColor];
}

- (void)onRemoveTemplateTapped:(id)sender {
    UIActionSheet *actionShet = [UIActionSheet bk_actionSheetWithTitle:nil];
    [actionShet bk_setDestructiveButtonWithTitle:@"Delete Tepmlate" handler:^{
        [[NSManagedObjectContext MR_defaultContext] deleteObject:self.cardTemplate];
        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
        [self.delegate onDismissViewController:self];
    }];
    
    [actionShet bk_setCancelButtonWithTitle:@"Cancel" handler:^{
        
    }];
    
    [actionShet showInView:self.view];
}

//- (void)keyboardWillShow:(NSNotification *)notification
//{
//    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
//    
//    UIEdgeInsets contentInsets;
//    if (UIInterfaceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation])) {
//        contentInsets = UIEdgeInsetsMake(64.0, 0.0, (keyboardSize.height), 0.0);
//    } else {
//        contentInsets = UIEdgeInsetsMake(64.0, 0.0, (keyboardSize.width), 0.0);
//    }
//    
//    
//    self.tableView.contentInset = contentInsets;
//    self.tableView.scrollIndicatorInsets = contentInsets;
//    
//    if (self.editingIndexPath) {
//        [self.tableView scrollToRowAtIndexPath:self.editingIndexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
//    }
//}
//
//- (void)keyboardWillHide:(NSNotification *)notification
//{
//    CGPoint oldOffset = self.tableView.contentOffset;
//    
//    self.tableView.contentInset = UIEdgeInsetsMake(64.0, 0.0, 0.0, 0.0);;
//    self.tableView.scrollIndicatorInsets = UIEdgeInsetsMake(64.0, 0.0, 0.0, 0.0);;
//    
//    self.tableView.contentOffset = oldOffset;
//    
//    [self.tableView scrollToRowAtIndexPath:self.editingIndexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
//}

- (IBAction)onChangeFieldTypeTapped:(id)sender {
    UIStoryboard *s = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    
    IKTypeEditViewController *vc = [s instantiateViewControllerWithIdentifier:@"TypeEdit"];
    
    UITableViewCell *cell = (UITableViewCell *)[[[sender superview] superview] superview];
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    
    if (indexPath.section == 1) {
        IKBaseField *field = [_customFields objectAtIndex:indexPath.row];
        vc.delegate = self;
        vc.field = field;
        vc.cardTemplate = self.cardTemplate;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (IBAction)onSaveTapped:(id)sender {
    if (self.editingIndexPath) {
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:self.editingIndexPath];
        if ([cell isKindOfClass:[IKTemplateDescriptionCell class]]) {
            IKTemplateDescriptionCell *editingCell = (IKTemplateDescriptionCell *)cell;
            if ([editingCell.descriptionField isFirstResponder]) {
                [editingCell.descriptionField resignFirstResponder];
            }
        }
    }
    
    NSError *error = nil;
    if ([self.cardTemplate isValidModel:&error]) {
        __weak NSUndoManager *undoMgr =_undoManager;
        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
            if (error) {
                NSString *errorMessage = [error localizedDescription];
                [UIAlertView bk_showAlertViewWithTitle:errorMessage message:nil cancelButtonTitle:@"Ok" otherButtonTitles:@[] handler:nil];
            } else {
                [undoMgr endUndoGrouping];
                [self.delegate onDismissViewController:self];
            }
        }];
    } else {
        NSString *errorMessage = [error localizedDescription];
        [UIAlertView bk_showAlertViewWithTitle:errorMessage
                                       message:nil
                             cancelButtonTitle:@"Ok"
                             otherButtonTitles:@[]
                                       handler:nil];
    }
}

- (IBAction)onCancelTapped:(id)sender {
    [_undoManager endUndoGrouping];
    [_undoManager undo];
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
    
    [self.delegate onDismissViewController:self];
}

- (IBAction)onChangeIconTapped:(id)sender {
    UIStoryboard *s = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    IKIconsListViewController *vc = [s instantiateViewControllerWithIdentifier:@"IconList"];
    vc.delegate = self;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)onSelectIcon:(UIImage *)iconImage iconName:(NSString *)iconName color:(UIColor *)color inViewcontroller:(UIViewController *)vc
{
    [self.navigationController popToViewController:self animated:YES];
    
    self.cardTemplate.imageName = iconName;
    [self.cardTemplate setValue:color forKey:@"imageColor"];
    
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.mm_drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModePanningNavigationBar];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.mm_drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Keyboard

#pragma mark - Table view data source
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1 && indexPath.row == [_customFields count]) {
        [self onAddFiledTapped:nil];
    }
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    } else {
        return [_customFields count] + 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *DescriptionCellIdentifier = @"DescriptionCell";
    static NSString *FieldCellIdentifier = @"FieldCell";
    static NSString *DefaultCellIdentifier = @"DefaultCell";
    
    if (indexPath.section == 0 && indexPath.row == 0) {
        IKTemplateDescriptionCell *cell = [tableView dequeueReusableCellWithIdentifier:DescriptionCellIdentifier forIndexPath:indexPath];
        UIImage *icon = [self.cardTemplate image];
        UIColor *tintColor = self.cardTemplate.imageColor;
        if (!tintColor) {
            tintColor = [UIColor blackColor];
        }
        icon = [icon rt_tintedImageWithColor:tintColor];
        
        [cell.iconBtn setImage:icon forState:UIControlStateNormal];
        
        NSString *templateName = self.cardTemplate.name;
        cell.descriptionField.text = templateName;
        cell.descriptionField.clearButtonMode = UITextFieldViewModeWhileEditing;
        cell.descriptionField.autocapitalizationType = UITextAutocapitalizationTypeWords;
        
        return cell;
    } else if (indexPath.section == 1){
        if (indexPath.row == [_customFields count]) {
            UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:DefaultCellIdentifier forIndexPath:indexPath];
            return cell;
        }
        
        IKTemplateFieldCell *cell = [tableView dequeueReusableCellWithIdentifier:FieldCellIdentifier forIndexPath:indexPath];
        IKTemplateField *field = _customFields[indexPath.row];
        NSString *fieldName = field.name;
        [cell.fieldButton setTitle:fieldName forState:UIControlStateNormal];
        return cell;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 90;
    }
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1) {
        if (indexPath.row == [_customFields count]) {
            return UITableViewCellEditingStyleInsert;
        } else {
            return UITableViewCellEditingStyleDelete;
        }
    } else {
        return UITableViewCellEditingStyleNone;
    }
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    NSMutableArray *things = _customFields;
    NSManagedObject *thing = [_customFields objectAtIndex:fromIndexPath.row];
    
    [things removeObject:thing];
    [things insertObject:thing atIndex:[toIndexPath row]];
    
    int i = 0;
    for (NSManagedObject *mo in things)
    {
        [mo setValue:[NSNumber numberWithInt:i++] forKey:@"listOrder"];
    }
}

- (NSIndexPath *)tableView:(UITableView *)tableView targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath *)sourceIndexPath toProposedIndexPath:(NSIndexPath *)proposedDestinationIndexPath
{
    
    if (proposedDestinationIndexPath.section > 1) {
        NSInteger row = [tableView numberOfRowsInSection:1] - 2;
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:1];
        return indexPath;
    } else if (proposedDestinationIndexPath.section == 1) {
        NSInteger row = MIN([tableView numberOfRowsInSection:sourceIndexPath.section] - 2, proposedDestinationIndexPath.row);
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:1];
        return indexPath;
    } else {
        return [NSIndexPath indexPathForRow:0 inSection:1];
    }
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1 && indexPath.row != [_customFields count]) {
        return YES;
    }
    return NO;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        IKTemplateField *f = [_customFields objectAtIndex:indexPath.row];
        [_customFields removeObjectAtIndex:indexPath.row];
        [self.cardTemplate removeFieldsObject:f];
        
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        [self onAddFiledTapped:nil];
    }
}

- (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return NO;
    }
    return YES;
}

- (void)onAddFiledTapped:(id)sender {
    UIStoryboard *s = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    
    IKTypeEditViewController *vc = [s instantiateViewControllerWithIdentifier:@"TypeEdit"];
    vc.delegate = self;
    vc.cardTemplate = self.cardTemplate;
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)onSaveEditField:(IKBaseField *)field viewController:(UIViewController *)vc {
    NSInteger index = -1;
    for (int i = 0; i < [_customFields count]; i++) {
        if (_customFields[i] == field) {
            index = i;
        }
    }
    
    [self.tableView beginUpdates];
    
    if (index == -1) {
        [_customFields addObject:field];
        index = [_customFields count] - 1;
        NSIndexPath *insertedIndexPath = [NSIndexPath indexPathForRow:index inSection:1];
        [self.tableView insertRowsAtIndexPaths:@[insertedIndexPath] withRowAnimation:UITableViewRowAnimationNone];
    } else {
        NSIndexPath *updatedIndexPath = [NSIndexPath indexPathForRow:index inSection:1];
        [self.tableView reloadRowsAtIndexPaths:@[updatedIndexPath] withRowAnimation:UITableViewRowAnimationNone];
    }
    
    [self.tableView endUpdates];
}


- (void)onCancelEditField:(IKBaseField *)field viewController:(UIViewController *)vc {
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    self.editingIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    NSString *value  = [textField text];
    self.cardTemplate.name = value;
    self.editingIndexPath = nil;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}




@end
