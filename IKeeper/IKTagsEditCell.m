//
//  IKTagsEditCell.m
//  IKeeper
//
//  Created by Chebulaev Oleg on 06.05.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import "IKTagsEditCell.h"

@implementation IKTagsEditCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
