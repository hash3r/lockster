//
//  IKMenuCell.m
//  IKeeper
//
//  Created by Chebulaev Oleg on 17.02.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import "IKMenuCell.h"
#import <YIInnerShadowView/YIInnerShadowView.h>
#import <LKBadgeView.h>


@implementation IKMenuCell

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        YIInnerShadowView *sv = [[YIInnerShadowView alloc] initWithFrame:CGRectMake(0, 0, 320, 49)];
        sv.shadowMask = YIInnerShadowMaskTop;
        sv.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.26];
        sv.shadowOffset = CGSizeMake(0, 1);
        sv.shadowRadius = 1.0;
        sv.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.1];
        
        _selectedView = sv;
        
        UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 48, 320, 1)];
        v.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.2];
        [sv addSubview:v];
        
        [self.contentView addSubview:_selectedView];
        [self.contentView sendSubviewToBack:_selectedView];
        _selectedView.hidden = YES;
    }
    return self;
}

- (void)awakeFromNib {
    if (!self.backgroundView) {
        YIInnerShadowView *sv = [[YIInnerShadowView alloc] initWithFrame:CGRectMake(0, 0, 320, 49)];
        sv.shadowMask = YIInnerShadowMaskTop;
        sv.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.26];
        sv.shadowOffset = CGSizeMake(0, 1);
        sv.shadowRadius = 1.0;
        self.backgroundView = sv;
        self.backgroundView.backgroundColor = [UIColor blackColor];
        
        self.backgroundView.layer.shadowColor = [[UIColor redColor]CGColor];
        self.backgroundView.layer.shadowOpacity = 0.0;
        self.backgroundView.layer.shadowRadius = 10;
        self.backgroundView.layer.shadowOffset = CGSizeMake(-10, 2);
    }
    
//    if (!self.selectedBackgroundView) {
//        UIView *selectedView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
//        selectedView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:1.0];
//        self.selectedBackgroundView =  selectedView;
//    }
    

    
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [super setHighlighted:highlighted animated:animated];
    if (highlighted) {
        self.backgroundView.layer.opacity = 0.2;
//        self.titleLabel.layer.opacity = 0.4;
//        self.countLabel.layer.opacity = 0.4;
//        self.roundView.layer.opacity = 0.4;
//        self.iconImageView.layer.opacity = 0.4;
    } else {
        self.backgroundView.layer.opacity = 0.00;
//        self.titleLabel.layer.opacity = 1.0;
//        self.countLabel.layer.opacity = 1.0;
//        self.roundView.layer.opacity = 1.0;
//        self.iconImageView.layer.opacity = 1.0;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    _selectedView.hidden = !selected;
}



@end
