//
//  IKFieldType.m
//  IKeeper
//
//  Created by Chebulaev Oleg on 06.01.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import "IKFieldType.h"
#import "IKBaseField.h"


@implementation IKFieldType

@dynamic name;
@dynamic fields;
@dynamic listOrder;
@dynamic internalIdentifier;
@dynamic uniqueIdentifier;

- (void)awakeFromInsert
{
    [super awakeFromInsert];
    if (!self.uniqueIdentifier) {
        self.uniqueIdentifier = [[NSProcessInfo processInfo] globallyUniqueString];
    }
}

#pragma mark - Static Methods
+ (void)seed {
    [IKFieldType MR_truncateAll];
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
    
    IKFieldType *textFieldType = [IKFieldType MR_createEntity];
    textFieldType.name = @"Text";
    textFieldType.internalIdentifier = @"text";
    textFieldType.listOrder = [NSNumber numberWithInt:0];
    textFieldType.uniqueIdentifier = @"IKFieldType0";
    
    IKFieldType *noCorectTextFieldType = [IKFieldType MR_createEntity];
    noCorectTextFieldType.name = @"Text (no auto-correct)";
    noCorectTextFieldType.internalIdentifier = @"noCorectText";
    noCorectTextFieldType.listOrder = [NSNumber numberWithInt:1];
    noCorectTextFieldType.uniqueIdentifier = @"IKFieldType1";
    
    IKFieldType *alphaNumericField = [IKFieldType MR_createEntity];
    alphaNumericField.name = @"Alpha-Numeric";
    alphaNumericField.internalIdentifier = @"alphaNumeric";
    alphaNumericField.listOrder = [NSNumber numberWithInt:2];
    alphaNumericField.uniqueIdentifier = @"IKFieldType2";
    
    IKFieldType *userameField = [IKFieldType MR_createEntity];
    userameField.internalIdentifier = @"username";
    userameField.listOrder = [NSNumber numberWithInt:3];
    userameField.name = @"Username";
    userameField.uniqueIdentifier = @"IKFieldType3";
    
    IKFieldType *passwordField = [IKFieldType MR_createEntity];
    passwordField.internalIdentifier = @"password";
    passwordField.listOrder = [NSNumber numberWithInt:4];
    passwordField.name = @"Password";
    passwordField.uniqueIdentifier = @"IKFieldType4";
    
    IKFieldType *numberField = [IKFieldType MR_createEntity];
    numberField.internalIdentifier = @"number";
    numberField.listOrder = [NSNumber numberWithInt:5];
    numberField.name = @"Number";
    numberField.uniqueIdentifier = @"IKFieldType5";
    
    IKFieldType *emailField = [IKFieldType MR_createEntity];
    emailField.internalIdentifier = @"email";
    emailField.listOrder = [NSNumber numberWithInt:6];
    emailField.name = @"Email";
    emailField.uniqueIdentifier = @"IKFieldType6";
    
    IKFieldType *urlFieldType = [IKFieldType MR_createEntity];
    urlFieldType.internalIdentifier = @"url";
    urlFieldType.listOrder = [NSNumber numberWithInt:7];
    urlFieldType.name = @"URL";
    urlFieldType.uniqueIdentifier = @"IKFieldType7";
    
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
}

+ (IKFieldType *)defaultType {
    IKFieldType *type = [IKFieldType MR_findFirstByAttribute:@"internalIdentifier" withValue:@"text"];
    return type;
}

+ (IKFieldType *)passwordType {
    IKFieldType *type = [IKFieldType MR_findFirstByAttribute:@"internalIdentifier" withValue:@"password"];
    return type;
}

#pragma mark - Methods
- (BOOL)isPasswordType {
    return [self.internalIdentifier isEqualToString:@"password"];
}

- (void)customizeTextField:(UITextField *)textField {
    
    if ([self.internalIdentifier isEqualToString:@"text"]) {
        textField.autocorrectionType = UITextAutocorrectionTypeDefault;
    } else {
        textField.autocorrectionType = UITextAutocorrectionTypeNo;
    }
    
    if ([self.internalIdentifier isEqualToString:@"text"]) {
        textField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    } else {
        textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    }
    
    
    if ([self.internalIdentifier isEqualToString:@"url"]) {
        textField.keyboardType = UIKeyboardTypeURL;
    } else if ([self.internalIdentifier isEqualToString:@"email"] || [self.internalIdentifier isEqualToString:@"username"]) {
        textField.keyboardType = UIKeyboardTypeEmailAddress;
    } else if ([self.internalIdentifier isEqualToString:@"number"]) {
        textField.keyboardType = UIKeyboardTypeNumberPad;
    } else if ([self.internalIdentifier isEqualToString:@"alphaNumeric"]) {
        textField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    } else {
        textField.keyboardType = UIKeyboardTypeDefault;
    }
}

@end
