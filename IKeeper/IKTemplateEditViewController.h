//
//  IKTemplateEditViewController.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 01.02.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IKCardTemplate.h"


@protocol IKTemplateEditViewControllerDelegate
- (void)onDismissViewController:(UIViewController *)vc;
@end

@interface IKTemplateEditViewController : UITableViewController

@property (nonatomic, weak) id<IKTemplateEditViewControllerDelegate> delegate;
@property (nonatomic, strong) IKCardTemplate *cardTemplate;


#pragma mark - Methods
- (IBAction)onCancelTapped:(id)sender;
- (IBAction)onChangeIconTapped:(id)sender;
- (IBAction)onSaveTapped:(id)sender;
- (IBAction)onChangeFieldTypeTapped:(id)sender;

@end
