//
//  IKFieldTypeListViewController.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 15.01.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import <UIKit/UIKit.h>
@class IKFieldType;

@protocol IKFieldTypeListViewControllerDelegate
- (void)onTypeSelect:(IKFieldType *)type viewController:(UIViewController *)vc;
@end

@interface IKFieldTypeListViewController : UITableViewController

@property (nonatomic, weak) id<IKFieldTypeListViewControllerDelegate> delegate;
@property (nonatomic, strong) IKFieldType *currentType;

@end