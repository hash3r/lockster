//
//  IKMarginLabel.m
//  IKeeper
//
//  Created by Chebulaev Oleg on 27.02.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import "IKMarginLabel.h"

@implementation IKMarginLabel

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)sizeToFit {
    [super sizeToFit];
}

//- (void)sizeToFit
//{
//	[super sizeToFit];
////    CGFloat border = self.layer.borderWidth;
//    CGRect newFrame = self.frame;
//    newFrame.size.height += 5;
//    newFrame.size.width += 5;
//    self.frame = newFrame;
//    CGFloat w = MAX(minWidth, self.bounds.size.height);
//    if (self.bounds.size.width < w) {
//        newFrame.size.width = w;
//        self.frame = newFrame;
//    }
//    gloss.cornerRadius = self.layer.cornerRadius = self.bounds.size.height / 2;
//	gloss.frame = self.layer.bounds;
    
//}

- (void)drawTextInRect:(CGRect)rect {
    UIEdgeInsets insets = {0, 10, 0, 10};
    [super drawTextInRect:UIEdgeInsetsInsetRect(rect, insets)];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
