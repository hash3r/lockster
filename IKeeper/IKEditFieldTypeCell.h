//
//  IKEditFieldTypeCell.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 08.02.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IKEditFieldTypeCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel  *fieldTypeLbl;

@end
