//
//  IKTemplateDescriptionCell.m
//  IKeeper
//
//  Created by Chebulaev Oleg on 01.02.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import "IKTemplateDescriptionCell.h"

@implementation IKTemplateDescriptionCell


- (void)awakeFromNib {
    self.roundView.layer.cornerRadius = self.roundView.frame.size.width / 2.0;
    self.roundView.layer.borderWidth = 1.0;
    self.roundView.layer.borderColor = [UIColorFromRGB(0x999999) CGColor];
}
@end
