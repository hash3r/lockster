//
//  IKTagViewCell.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 04.04.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <LKBadgeView.h>

@interface IKTagViewCell : UITableViewCell


@property (nonatomic, strong) UIImageView *checkImageView;
@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, assign) BOOL isChecked;

@end
