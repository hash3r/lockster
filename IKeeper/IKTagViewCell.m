//
//  IKTagViewCell.m
//  IKeeper
//
//  Created by Chebulaev Oleg on 04.04.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import "IKTagViewCell.h"
#import <LKBadgeView/LKBadgeView.h>

@implementation IKTagViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleGray;
        self.separatorInset = UIEdgeInsetsMake(0, 39, 0, 0);
        
        self.checkImageView = [[UIImageView alloc] initWithFrame:CGRectMake(15, 18, 14, 11)];
        self.checkImageView.image = [UIImage imageNamed:@"check.png"];
        [self.contentView addSubview:self.checkImageView];
        
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(39, 12, 200, 21)];
        self.titleLabel.font = [IKFonts helveticaNewWithSize:18.0];
        [self.contentView addSubview:self.titleLabel];
    }
    return self;
}

- (void)awakeFromNib {
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setIsChecked:(BOOL)isChecked {
    _isChecked = isChecked;
    self.checkImageView.hidden = !isChecked;
}

@end
