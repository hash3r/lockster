//
//  IKWallapersListViewController.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 18.02.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IKWallapersListViewController : UICollectionViewController

- (IBAction)onChoosePhoto:(id)sender;
@end
