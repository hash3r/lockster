//
//  IKCardShowDescriptionCell.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 26.01.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IKCardShowDescriptionCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIView *roundView;
@property (nonatomic, weak) IBOutlet UIImageView *iconImageView;
@property (nonatomic, weak) IBOutlet UILabel *descriptionLabel;
@property (nonatomic, weak) IBOutlet UILabel *templateLabel;

@end
