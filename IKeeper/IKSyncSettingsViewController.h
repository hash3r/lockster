//
//  IKSyncSettingsViewController.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 03.04.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IKSyncSettingsViewController : UITableViewController

@end
