//
//  IKCardTemplate.m
//  IKeeper
//
//  Created by Chebulaev Oleg on 06.01.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import "IKCardTemplate.h"
#import "IKCard.h"
#import "IKTemplateField.h"
#import "IKFieldType.h"


@implementation IKCardTemplate

@dynamic name;
@dynamic imageName;
@dynamic imageColor;
@dynamic listOrder;
@dynamic fields;
@dynamic cards;
@dynamic uniqueIdentifier;

- (void)awakeFromInsert
{
    [super awakeFromInsert];
    if (!self.uniqueIdentifier) {
        self.uniqueIdentifier = [[NSProcessInfo processInfo] globallyUniqueString];
    }
}

#pragma mark - Static Methods
+ (void)seed {
    [IKCardTemplate MR_truncateAll];
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
    
    NSString *pathOfTemplates = [[NSBundle mainBundle] pathForResource:@"templates" ofType:@"plist"];
    NSArray *rawTemplates = [NSArray arrayWithContentsOfFile:pathOfTemplates];
    
    NSInteger uniqIdentiferT = 0;
    for (NSDictionary *rawTemplate in rawTemplates) {
        IKCardTemplate *template = [IKCardTemplate MR_createEntity];
        
        NSString *uniqIdentiferString = [NSString stringWithFormat:@"IKCardTemplate%d", uniqIdentiferT];
        uniqIdentiferT++;
        template.uniqueIdentifier = uniqIdentiferString;
        
        NSArray *rawFields = [rawTemplate objectForKey:@"Fields"];
        template.name = [rawTemplate objectForKey:@"Name"];
        
        NSInteger uniqIdentiferF = 0;
        for (NSDictionary *rawField in rawFields) {
            NSString *fieldName = [rawField objectForKey:@"Name"];
            NSNumber *listOrder = [rawField objectForKey:@"ListOrder"];
            NSString *fieldType = [rawField objectForKey:@"Type"];
            
            IKTemplateField *templateField = [IKTemplateField MR_createEntity];
            
            NSString *uniqIdentiferFString = [NSString stringWithFormat:@"IKTemplateField%d", uniqIdentiferF];
            uniqIdentiferF++;
            templateField.uniqueIdentifier = uniqIdentiferFString;
            
            templateField.name = fieldName;
            IKFieldType *type = [IKFieldType MR_findFirstByAttribute:@"internalIdentifier" withValue:fieldType];
            
            
            templateField.type = type;
            templateField.listOrder = listOrder;
            [template addFieldsObject:templateField];
        }
        template.imageName = [rawTemplate objectForKey:@"ImageName"];
        template.listOrder = [rawTemplate objectForKey:@"ListOrder"];
    }

    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
}

+ (IKCardTemplate *)buildEmptyTemplate {
    IKCardTemplate *template = [IKCardTemplate MR_createEntity];
    IKCardTemplate *lastTemplate = [IKCardTemplate MR_findFirstOrderedByAttribute:@"listOrder" ascending:NO];
    template.listOrder = [NSNumber numberWithInt:[lastTemplate.listOrder integerValue] + 1];
    template.imageName = @"lock1";
    

    IKFieldType *defaultType = [IKFieldType defaultType];
    
    IKTemplateField *descriptionField = [IKTemplateField MR_createEntity];
    descriptionField.name = @"Description";
    descriptionField.listOrder = [NSNumber numberWithInt:0];

    descriptionField.type = defaultType;
    [template addFieldsObject:descriptionField];
    
    IKTemplateField *noteField = [IKTemplateField MR_createEntity];
    noteField.name = @"Note";
    noteField.listOrder = [NSNumber numberWithInt:1];
    noteField.type = defaultType;
    [template addFieldsObject:noteField];

    return template;
}

- (BOOL)isValidModel:(NSError **)error {
    NSString *val = [self name];
    if (val == nil || [val length] == 0) {
        NSDictionary *userInfoDict = [NSDictionary
                                      dictionaryWithObject:@"Description is required field"
                                      forKey:NSLocalizedDescriptionKey];
        *error = [[NSError alloc] initWithDomain:
                  NSCocoaErrorDomain code:-1 userInfo:userInfoDict];
        
        
        return NO;
    }
    
    for (IKTemplateField *f in self.fields) {
        if (![f isValidModel:error]) {
            return NO;
        }
    }
    
    return YES;
}

- (IKTemplateField *)createField {
    IKTemplateField *f = [IKTemplateField MR_createEntity];
    f.type = [IKFieldType defaultType];
    f.listOrder = [NSNumber numberWithInt:[self maxListOrder] + 1];
    [self addFieldsObject:f];
    return f;
}

- (UIImage *)image {
    NSString *imageName = [self imageName];
    NSString *fileName = [imageName stringByAppendingString:@".png"];
    UIImage *image = [UIImage imageNamed:fileName];
    return image;
}

- (IKTemplateField *)descriptionField {
    IKTemplateField *descriptionField = nil;
    for (IKTemplateField *f in self.fields) {
        if ([f isDescriptionField]) {
            descriptionField = f;
        }
    }
    return descriptionField;
}

- (IKTemplateField *)noteField {
    for (IKTemplateField *f in self.fields) {
        if ([f isNoteField]) {
            return f;
        }
    }
    return nil;
}

- (NSArray *)customFields {
    NSMutableArray * customFields = [NSMutableArray array];
    for (IKTemplateField *f in self.fields) {
        if (![f isDescriptionField] && ![f isNoteField]) {
            [customFields addObject:f];
        }
    }
    return customFields;
}

- (NSArray *)orderedCustomFields {
    NSMutableArray *customFields = [[self customFields] mutableCopy];
    NSSortDescriptor *listSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"listOrder" ascending:YES];
    [customFields sortUsingDescriptors:@[listSortDescriptor]];
    return [NSArray arrayWithArray:customFields];
}

- (NSInteger)maxListOrder {
    NSInteger maxValue = -1;
    for (IKTemplateField *f in self.fields) {
        if ([f.listOrder integerValue] > maxValue) {
            maxValue = [f.listOrder integerValue];
        }
    }
    return maxValue;
}

#pragma mark - Methods

@end
