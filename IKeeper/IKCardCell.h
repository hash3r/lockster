//
//  IKCardCell.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 19.01.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IKCardCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIView *roundView;
@property (nonatomic, weak) IBOutlet UIView *lineView;
@property (nonatomic, weak) IBOutlet UIImageView *iconImageView;
@property (nonatomic, weak) IBOutlet UILabel *descriptionLabel;
@property (nonatomic, weak) IBOutlet UIView *fieldsContainerView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *topSpace;

- (void)setIsFavorite:(BOOL)favorite;

@end
