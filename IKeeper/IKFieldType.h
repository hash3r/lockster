//
//  IKFieldType.h
//  IKeeper
//
//  Created by Chebulaev Oleg on 06.01.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class IKBaseField;

@interface IKFieldType : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * internalIdentifier;
@property (nonatomic, retain) NSNumber * listOrder;
@property (nonatomic, retain) NSSet *fields;
@property (nonatomic, retain) NSString *uniqueIdentifier;

#pragma mark - Static Methods
+ (void)seed;
+ (IKFieldType *)defaultType;
+ (IKFieldType *)passwordType;
- (BOOL)isPasswordType;

#pragma mark - Methods
- (void)customizeTextField:(UITextField *)textField;
@end

@interface IKFieldType (CoreDataGeneratedAccessors)

- (void)addFieldsObject:(IKBaseField *)value;
- (void)removeFieldsObject:(IKBaseField *)value;
- (void)addFields:(NSSet *)values;
- (void)removeFields:(NSSet *)values;

@end
