//
//  IKPasteboardHelper.m
//  IKeeper
//
//  Created by Chebulaev Oleg on 13.03.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import "IKPasteboard.h"
#import "IKSettings.h"

@implementation IKPasteboard

+ (instancetype)sharedInstance
{
    static dispatch_once_t pred;
    static IKPasteboard *shared = nil;
    
    dispatch_once(&pred, ^{
        shared = [[IKPasteboard alloc] init];
    });
    return shared;
}

- (void)addObservers {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resetIdleTimer) name:kNotificationClearClipboardAfterChanged object:nil];
}

- (void)removeObservers {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationClearClipboardAfterChanged object:nil];
}

- (void)resetIdleTimer
{
    if (_clipboardTimer) {
        [_clipboardTimer invalidate];
        _clipboardTimer = nil;
    }
    
    NSInteger cleanAfter = [[IKSettings sharedInstance] clearClipboardAfter];
    if (cleanAfter == INT_MAX) {
        return;
    }
	int timeout = cleanAfter * 60;
    _clipboardTimer = [NSTimer scheduledTimerWithTimeInterval:timeout
                                                  target:self
                                                selector:@selector(cleanTimerExceeded)
                                                userInfo:nil
                                                 repeats:NO];
}

- (void)cleanTimerExceeded {
    [UIPasteboard generalPasteboard].string = @"";
}

- (void)setToPasteboard:(NSString *)text {
    [self resetIdleTimer];
    [UIPasteboard generalPasteboard].string = text;
}

- (void) dealloc {
    [self removeObservers];
	[_clipboardTimer invalidate];
    _clipboardTimer = nil;
}
@end
