//
//  IKHintCreateView.m
//  IKeeper
//
//  Created by Chebulaev Oleg on 25.03.14.
//  Copyright (c) 2014 Chebulaev Oleg. All rights reserved.
//

#import "IKHintCreateView.h"
#import "IKFonts.h"

@implementation IKHintCreateView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        UIView *hintView = [[UIView alloc] initWithFrame:CGRectMake(18, 135, 284, 185)];
        
        UILabel *l = [[UILabel alloc] initWithFrame:CGRectMake(15, 18, 254, 50)];
        l.backgroundColor = [UIColor clearColor];
        l.font = [IKFonts helveticaNewLightWithSize:15.0];
        l.textColor = [UIColor whiteColor];
        l.backgroundColor = [UIColor clearColor];
        l.textAlignment = NSTextAlignmentCenter;
        l.numberOfLines = 2;
        l.lineBreakMode = NSLineBreakByWordWrapping;
        l.text = @"Please, describe a hint here\n in case you’ll forget your passcode";
        [hintView addSubview:l];
        
        UITextField *f = [[UITextField alloc] initWithFrame:CGRectMake(15, CGRectGetMaxY(l.frame) + 10, 254, 40)];
        f.tag = 101;
        f.delegate = self;
        f.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.2];
        f.font = [IKFonts helveticaNewLightWithSize:15.0];
        f.textAlignment = NSTextAlignmentCenter;
        f.tintColor = [UIColor yellowColor];
        f.textColor = [UIColor yellowColor];
        [hintView addSubview:f];
        self.textField = f;
        
        UIButton *useEmpty = [UIButton buttonWithType:UIButtonTypeSystem];
        [useEmpty setTitle:@"Use empty" forState:UIControlStateNormal];
        useEmpty.frame = CGRectMake(15, CGRectGetMaxY(f.frame) + 10, 120, 50);
        [hintView addSubview:useEmpty];
        self.useEmptyButton = useEmpty;
        
        UIButton *done = [UIButton buttonWithType:UIButtonTypeSystem];
        [done setTitle:@"Done" forState:UIControlStateNormal];
        done.frame = CGRectMake(150, CGRectGetMaxY(f.frame) + 10, 120, 50);
        [hintView addSubview:done];
        self.doneButton = done;
        
        hintView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.9];
        [self addSubview:hintView];
        _hintView = hintView;
        _hintView.alpha = 0.0;
    }
    return self;
}



- (void)show {
    [UIView animateWithDuration:0.15 animations:^{
        _hintView.alpha = 1.0;
    }];
}

- (void)hideAndRemove {
    [UIView animateWithDuration:0.15 animations:^{
        _hintView.alpha = 0.0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}


@end
